package quicksort;

/**
 * Created by phanindra on 3/28/16.
 */
import java.util.Arrays;

public class Partition {
    public static int[] partition(int[] a) {
        int pivot = a[0];
        int startIndex = 0;
        int endIndex = a.length - 1;
        int[] b = new int[a.length];
        for(int i = 1; i < a.length; i++) {
            if(a[i] > pivot) {
                b[endIndex] = a[i];
                endIndex--;
            } else if(a[i] < pivot) {
                b[startIndex] = a[i];
                startIndex++;
            }
        }
        b[startIndex] = pivot;
        return b;
    }

    public static int partitionInPlace(int[] a, int left, int right) {
       return -1;
    }

    public static void main(String[] args) {
        int[] a = new int[] {2, 8, 3, 5, 1, 4, 7, 6};
        System.out.println(Partition.partitionInPlace(a, 0, a.length - 1));
    }
}
