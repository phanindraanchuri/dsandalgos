package ll;

public class Solution {
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		// Works assuming lists are in reverse order that is the number 98 is
		// represented as 8->9
		// The reason it is important that the lists be in reversed order is so
		// that the units place etc match up during adding.
		// We use two pointers in addition to result, currNode is a reference to
		// the node being
		// constructed from the sum during any step, prev keeps track of the end
		// of the result list constructed so far.
		ListNode result = null, prev = null;
		ListNode currNode = null;
		// Initially there is no carry
		int carry = 0;
		while (l1 != null || l2 != null) {
			int sum = 0;
			if (l1 != null) {
				sum = sum + l1.val;
			}
			if (l2 != null) {
				sum = sum + l2.val;
			}
			// sum at any step is the sum of the val of the node in the first
			// list, the val of the node in the second list and any carry.
			sum = sum + carry;
			if (sum > 9) {
				// Sum is > 9 that is there will be a carry, used in further
				// steps.
				carry = 1;
				// Update sum to leave the carry out. For instance let sum = 12,
				// carry = 1, sum =2;
				sum = sum % 10;
			}
			// Make a new node with the sum
			currNode = new ListNode(sum);
			if (result == null) {
				// This will be true the first time around, that is the result
				// list has not yet been made.
				result = currNode;
			} else {
				// This will be true on all subsequent steps, since the result
				// has been partially constructed the first time around and prev
				// as stated previously is the last node in the result. Making
				// prev.next = currNode will add currNode to result
				prev.next = currNode;
			}
			// Update prev for the next step, to point to the newly added result
			// node currNode
			prev = currNode;
			// Move onto the next node in the lists, assuming they exist.
			if (l1 != null) {
				l1 = l1.next;
			}
			if (l2 != null) {
				l2 = l2.next;
			}
		}
		// After all the steps, if there is a carry, we need to add an
		// additional node to the result.
		if (carry > 0) {
			// currNode is the last node added in the result, so we need to
			// construct a new node with the value carry and set it as the
			// currNode's next, so that the carry node becomes the last node in
			// the result.
			currNode.next = new ListNode(carry);
		}
		return result;
	}

	public static void main(String[] args) {
		ListNode l11 = new ListNode(9);
		ListNode l12 = new ListNode(9);
		ListNode l13 = new ListNode(9);
		l11.next = l12;
		l12.next = l13;

		ListNode l21 = new ListNode(8);
		ListNode l22 = new ListNode(9);
		l21.next = l22;

		Solution s = new Solution();
		s.addTwoNumbers(l11, l21);
	}
}
