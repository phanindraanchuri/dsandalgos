package circle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Solution {
	
	static class Candidate {
		int index;
		int radius;
		int cost;

		Candidate(int i, int r, int c) {
			index = i;
			radius = r;
			cost = c;
		}
	}
	
	static int[] Circles(int distance, int[] radius, int[] cost) {
		int noOfGears = radius.length;
		Map<Integer, List<Candidate>> candidateMap = new HashMap<>();

		for (int i = 0; i < noOfGears; i++) {
			List<Candidate> innerList = new ArrayList<Candidate>();
			for (int j = 0; j < noOfGears; j++) {
				if (radius[i] + radius[j] >= distance) {
					Candidate p = new Candidate(j, radius[j], cost[j]);
					innerList.add(p);
				}
			}
			candidateMap.put(i, innerList);
		}
		
		for(Entry<Integer, List<Candidate>> e: candidateMap.entrySet()) {
			int currentMin = Integer.MAX_VALUE;
			for(Candidate c : e.getValue()) {
			}
		}
		return null;
	}

}
