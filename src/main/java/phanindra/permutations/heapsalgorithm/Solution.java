package phanindra.permutations.heapsalgorithm;

import java.util.Arrays;

public class Solution {
  public void permute(int[] input) {
    permute(input, input.length);
  }

  public void permute(int[] nums, int n) {
    if (n == 1) {
      System.out.println(Arrays.toString(nums));
    }
    else {
      for (int i = 0; i < n - 1; i++) {
        permute(nums, n - 1);
        if (i % 2 == 0) {
          swap(nums, i, n - 1);
        }
        else {
          swap(nums, 0, n - 1);
        }
      }
      permute(nums, n - 1);
    }
  }

  private void swap(int[] nums, int i, int j) {
    int temp = nums[i];
    nums[i] = nums[j];
    nums[j] = temp;
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    s.permute(new int[] { 1, 2, 3 });
  }
}
