package phanindra.countpalindromes;

public class Solution {
	static int countPalindromes(String S) {
		int count = 0;
		for (int i = 0; i < S.length(); i++) {
			for (int j = i; j < S.length(); j++) {
				if (isPalindrome(S.substring(i, j + 1))) {
					count++;
				}
			}
		}
		return count;
	}

	static boolean isPalindrome(String s) {
		if (s == null || s.isEmpty() || s.length() == 1) {
			return true;
		}
		int start = 0;
		int end = s.length() - 1;
		while (start < end) {
			if (s.charAt(start) != s.charAt(end)) {
				return false;
			}
			start++;
			end--;
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(Solution.countPalindromes("wowpurerocks"));
	}
}
