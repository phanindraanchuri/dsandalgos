package phanindra.removevowels;

import java.util.Arrays;
import java.util.List;

public class Solution {

	private static final List<Character> VOWELS = Arrays
			.asList(new Character[] { 'a', 'e', 'i', 'o', 'u' });

	private static String removeVowels(String s) {
		char[] chars = s.toCharArray();
		int vowelCount = 0;
		int vowelStartIndex = -1;
		for (int i = 0; i < s.length(); i++) {
			if (VOWELS.contains(chars[i])) {
				vowelCount++;
				if (vowelStartIndex == -1) {
					vowelStartIndex = i;
				}
			}
			if (!VOWELS.contains(chars[i]) && vowelStartIndex != -1) {
				char temp = chars[i];
				chars[i] = chars[vowelStartIndex];
				chars[vowelStartIndex] = temp;
				vowelStartIndex++;
			}
		}
		char[] result = Arrays.copyOfRange(chars, 0, chars.length - vowelCount);
		return Arrays.toString(result);
	}

	public static void main(String[] args) {
		System.out.println(Solution.removeVowels("papers"));
	}
}
