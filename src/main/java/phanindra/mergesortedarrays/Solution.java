package phanindra.mergesortedarrays;

public class Solution {
	static int[] mergeArrays(int[] a, int[] b) {
		int resultSize = a.length + b.length;
		int[] result = new int[resultSize];
		int aIndex = 0;
		int bIndex = 0;
		int resultIndex = 0;
		if(a.length != b.length) {
			return null;
		}
		while(aIndex < a.length && bIndex < b.length) {
			if(a[aIndex] < b[bIndex]) {
				result[resultIndex] = a[aIndex];
				aIndex++;
			} else {
				result[resultIndex] = b[bIndex];
				bIndex++;
			}
			resultIndex++;
		}
		
		while(aIndex < a.length) {
			result[resultIndex] = a[aIndex];
			aIndex++;
			resultIndex++;
		}
		
		while(bIndex < b.length) {
			result[resultIndex] = b[bIndex];
			bIndex++;
			resultIndex++;
		}
		return result;
	}
}
