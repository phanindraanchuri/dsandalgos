package phanindra.arrayrotate;

public class ReversalRotationAlgorithm implements RotationAlgorithm {

	@Override
	public int[] rotate(int[] array, int amount, RotationDirection direction) {
		if (amount > array.length) {
			amount = amount % array.length;
		}
		if (RotationDirection.RIGHT.equals(direction)) {
			return applyReversalAlgorithm(array, amount);
		} else {
			return applyReversalAlgorithm(array, array.length - amount);
		}
	}

	private int[] applyReversalAlgorithm(int[] array, int amount) {
		int[] a = new int[array.length - amount];
		System.arraycopy(array, 0, a, 0, a.length);
		int[] revA = reverse(a, 0, a.length - 1);

		int[] b = new int[amount];
		System.arraycopy(array, array.length - amount, b, 0, b.length);
		int[] revB = reverse(b, 0, b.length - 1);

		int[] r = new int[revA.length + revB.length];
		System.arraycopy(revA, 0, r, 0, revA.length);
		System.arraycopy(revB, 0, r, revA.length, revB.length);
		return reverse(r, 0, r.length - 1);
	}

	private int[] reverse(int[] array, int startIndex, int endIndex) {
		int left = startIndex;
		int right = endIndex;
		while (left < right) {
			int temp = array[left];
			array[left] = array[right];
			array[right] = temp;
			left++;
			right--;
		}
		return array;
	}
}
