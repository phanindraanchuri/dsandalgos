package phanindra.arrayrotate;

public interface RotationAlgorithm {
	int[] rotate(int[] array, int amount, RotationDirection direction);
}
