package phanindra.arrayrotate;

public class ExtraSpaceRotationAlgorithm implements RotationAlgorithm {

	@Override
	public int[] rotate(int[] array, int amount, RotationDirection direction) {
		if (amount > array.length) {
			amount = amount % array.length;
		}
		if(RotationDirection.RIGHT.equals(direction)) {
			rotateHelper(array, amount);
		} else {
			rotateHelper(array, array.length - amount);
		}
		return array;
	}

	private void rotateHelper(int[] array, int amount) {
		int[] temp = new int[amount];
		int tempIndex = 0;
		for(int index = array.length - amount; index < array.length; index++) {
			temp[tempIndex] = array[index];
			tempIndex++;
		}
		for(int index = array.length - amount - 1; index >= 0; index--) {
			array[index+amount] = array[index];
		}
		for(int index = 0; index < tempIndex; index++) {
			array[index] = temp[index];
		}
	}
}
