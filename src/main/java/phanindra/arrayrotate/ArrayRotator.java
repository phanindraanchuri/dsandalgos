package phanindra.arrayrotate;

public class ArrayRotator {
	private RotationAlgorithm algorithm;

	public ArrayRotator(RotationAlgorithm algorithm) {
		this.algorithm = algorithm;
	}
	
	int[] rotate(int[] array, int amount, RotationDirection direction) {
		return algorithm.rotate(array, amount, direction);
	}
}
