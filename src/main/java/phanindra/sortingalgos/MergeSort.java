package phanindra.sortingalgos;

import java.util.Arrays;

public class MergeSort {
	public void sort(int[] a) {
		sortHelper(a, 0, a.length - 1);
	}

	public void sortHelper(int[] a, int low, int high) {
		if (low < high) {
			int mid = low + (high - low) / 2;
			sortHelper(a, low, mid);
			sortHelper(a, mid + 1, high);
			merge(a, low, mid, high);
		}
	}

	private void merge(int[] a, int low, int mid, int high) {
		// Having a size of high+1 ensures that we have sufficient space in b
		int[] b = new int[high + 1];
		int i = low;
		int k = low;
		int j = mid + 1;
		while (i <= mid && j <= high) {
			if (a[i] < a[j]) {
				b[k] = a[i];
				k++;
				i++;
			} else {
				b[k] = a[j];
				k++;
				j++;
			}
		}
		while (i <= mid) {
			b[k] = a[i];
			k++;
			i++;
		}
		while (j <= high) {
			b[k] = a[j];
			k++;
			j++;
		}
		for (int m = low; m <= high; m++) {
			a[m] = b[m];
		}
	}

	public static void main(String[] args) {
		MergeSort ms = new MergeSort();
		int[] a = new int[] { 12, 6, 3, 5, 4, 7, 9, 11 };
		ms.sort(a);
		System.out.println(Arrays.toString(a));
	}
}