package phanindra.arrayintersection;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

// http://stackoverflow.com/questions/13270491/best-way-to-find-an-intersection-between-two-arrays
// Dumb simplest approach
// Search for every element from array one in array two. Time complexity O(n^2).
//
// Sorting approach
// You need to sort only array one, then search for elements from array two using binary search. Time complexity:
// sorting O(nlogn), searching O(n * logn) = O(nlogn), total O(nlogn).
//
// Hash approach
// Create a hash table from array one elements. Search for elements form second table in the hash table. The time
// complexity depends on the hash function. You can achieve O(1) for searches in the optimal case (all elements will
// have different hash value), but O(n) in the worst case (all elements will have the same hash value). Total time
// complexity: O(n^x), where x is a factor of hash function efficiency (between 1 and 2).
//
// Some hash functions are guaranteed to build a table with no collisions. But the building no longer takes strictly
// O(1) time for every element. It will be O(1) in most cases, but if the table is full or a collision is encountered,
// then the table needs to be rehashed - taking O(n) time. This happens not so often, much less frequently than clean
// adds. So the AMORTISED time complexity is O(1). We don't care about some of the adds taking O(n) time, as long as
// the majority of adds takes O(1) time.
//
// But even so, in an extreme case, the table must be rehashed every single insertion, so the strict time complexity
// would be O(n^2)
public class ArrayIntersection {

  List<Integer> intersection(int[] a, int[] b) {
    List<Integer> result = new ArrayList<>();
    Arrays.sort(a);
    Arrays.sort(b);
    // To avoid adding duplicates to the result
    int lastAddedToResult = -1;
    for (int i = 0; i < a.length; i++) {
      if (binarySearch(b, a[i]) != -1 && a[i] != lastAddedToResult) {
        lastAddedToResult = a[i];
        result.add(a[i]);
      }
    }
    return result;
  }

  List<Integer> intersection2(int[] a, int[] b) {
    Map<Integer, Integer> aMap = new HashMap<>();
    for (int x : a) {
      if (aMap.containsKey(x)) {
        aMap.put(x, aMap.get(x) + 1);
      }
      else {
        aMap.put(x, 1);
      }
    }
    List<Integer> result = new ArrayList<>();
    for (int x : b) {
      if (aMap.containsKey(x) && aMap.get(x) >= 1) {
        result.add(x);
        // Once we find an intersection, we don't want to add it to the result again,
        // so remove from the map.
        aMap.remove(x);
      }
    }
    return result;
  }

  int binarySearch(int[] a, int x) {
    return binarySearchHelper(a, 0, a.length - 1, x);
  }

  int binarySearchHelper(int[] a, int low, int high, int x) {
    int result = -1;
    while (low <= high) {
      int mid = low + (high - low) / 2;
      if (a[mid] == x) {
        result = x;
        return result;
      }
      else if (x > a[mid]) {
        low = mid + 1;
      }
      else {
        high = mid - 1;
      }
    }
    return result;
  }

  public static void main(String[] args) {
    ArrayIntersection ai = new ArrayIntersection();
    int[] a = new int[] { 4, 1, 2, 2, 2, 1 };
    int[] b = new int[] { 2, 1, 2, 1 };
    System.out.println(ai.intersection(a, b));
    System.out.println(ai.intersection2(a, b));
  }
}
