package phanindra.selltickets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * Given an array a, that represents the number of tickets available for sale at
 * each ticket window and the number of tickets to sell m, find the maximum
 * amount of money that can be made given that tickets at a window sell for as
 * much as the number of left over tickets prior to the sale.
 *
 */
public class Solution {
	public static int maximumMoney(int[] a, int m) {
		List<Integer> l = new ArrayList<>();
		for (int x : a) {
			l.add(x);
		}
		Collections.sort(l, Collections.reverseOrder());
		List<Integer> l2 = new ArrayList<>();
		for (int e : l) {
			for (int i = e; i > 0; i--) {
				l2.add(i);
			}
		}
		Collections.sort(l2, Collections.reverseOrder());
		int result = 0;
		for (int i = 0; i < m; i++) {
			result += l2.get(i);
		}
		return result;
	}

	public static void main(String[] args) {
		int[] a = new int[] { 3, 3, 4 };
		System.out.println(Solution.maximumMoney(a, 5));
	}
}
