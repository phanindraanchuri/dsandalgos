package phanindra.selltickets;

import java.util.Collections;
import java.util.PriorityQueue;

/**
 * 
 * Given an array a, that represents the number of tickets available for sale at
 * each ticket window and the number of tickets to sell m, find the maximum
 * amount of money that can be made given that tickets at a window sell for as
 * much as the number of left over tickets prior to the sale.
 *
 */
public class HeapSolution {
	public static int maximumMoney(int[] a, int m) {
		PriorityQueue<Integer> pq = new PriorityQueue<>(
				Collections.reverseOrder());
		for (int num : a) {
			pq.add(num);
		}
		int money = 0;
		for (int i = m; i > 0; i--) {
			int max = pq.peek();
			System.out.println("max is " + max);
			money += pq.remove();
			pq.add(max - 1);
		}
		return money;
	}

	public static void main(String[] args) {
		int[] a = new int[] { 3, 3, 4 };
		System.out.println(HeapSolution.maximumMoney(a, 11));
	}
}
