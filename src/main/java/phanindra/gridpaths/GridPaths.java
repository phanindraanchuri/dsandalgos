package phanindra.gridpaths;

public class GridPaths {
  public int numPaths(int m, int n) {
    System.out.println("NP " + m + ":" + n);
    if (m == 0 || n == 0) {
      return 1;
    }
    else {
      return numPaths(m - 1, n) + numPaths(m, n - 1);
    }
  }

  public int numPathsDp(int m, int n) {
    int[][] np = new int[m + 1][n + 1];
    for (int i = 0; i <= m; i++) {
      np[i][0] = 1;
    }
    for (int i = 0; i <= n; i++) {
      np[0][i] = 1;
    }
    for (int i = 1; i <= m; i++) {
      for (int j = 1; j <= n; j++) {
        np[i][j] = np[i - 1][j] + np[i][j - 1];
      }
    }
    return np[m][n];
  }

  public static void main(String[] args) {
    int m = 2;
    int n = 2;
    GridPaths gp = new GridPaths();
    System.out.println(gp.numPathsDp(m, n));
  }
}
