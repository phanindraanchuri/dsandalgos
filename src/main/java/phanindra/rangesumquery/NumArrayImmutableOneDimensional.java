package phanindra.rangesumquery;

/**
 * Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.

 Example:
 Given nums = [-2, 0, 3, -5, 2, -1]

 sumRange(0, 2) -> 1
 sumRange(2, 5) -> -1
 sumRange(0, 5) -> -3
 Note:
 You may assume that the array does not change.
 There are many calls to sumRange function.
 */
public class NumArrayImmutableOneDimensional {
    private int[] cumulativeSumArray;

    public NumArrayImmutableOneDimensional(int[] nums) {
        if(nums.length != 0) {
            cumulativeSumArray = new int[nums.length+1];
            cumulativeSumArray[0] = nums[0];
            for(int i= 1; i < nums.length; i++) {
                cumulativeSumArray[i] = cumulativeSumArray[i-1] + nums[i];
            }
        }
    }

    public int sumRange(int i, int j) {
        if(i == 0) {
            return cumulativeSumArray[j];
        }
        return cumulativeSumArray[j] - cumulativeSumArray[i-1];
    }
}
