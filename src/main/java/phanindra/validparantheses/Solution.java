package phanindra.validparantheses;

import java.util.*;

public class Solution {

    private static final List<Character> OPEN = Arrays.asList('(','{','[');

    private static final Map<Character,Character> OPEN_CLOSE;

    static {
        OPEN_CLOSE = new HashMap<>();
        OPEN_CLOSE.put('{','}');
        OPEN_CLOSE.put('[',']');
        OPEN_CLOSE.put('(',')');
    }

    public boolean isValid(String s) {
        if(s == null || s.isEmpty()) {
            return false;
        }
        Stack<Character> stack = new Stack<>();
        for(char c : s.toCharArray()) {
            if(OPEN.contains(c)) {
                stack.push(c);
            } else {
                if(stack.isEmpty()) {
                    return false;
                }
                char popped = stack.pop();
                if(OPEN_CLOSE.get(popped) != c) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}