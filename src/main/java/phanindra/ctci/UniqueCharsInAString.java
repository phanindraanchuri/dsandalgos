package phanindra.ctci;

import java.util.HashMap;
import java.util.Map;

public class UniqueCharsInAString {
	public boolean hasUniqueChars(String input) {
		if(input.isEmpty()) {
			return false;
		}
		if(input.length() == 1) {
			return true;
		}
		Map<Character, Integer> m = new HashMap<Character, Integer>();
		for(char c : input.toCharArray()) {
			if(m.containsKey(c)) {
				return false;
			} else {
				m.put(c, 1);
			}
		}
		return false;
	}
}
