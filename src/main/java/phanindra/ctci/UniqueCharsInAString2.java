package phanindra.ctci;

import java.util.Arrays;

public class UniqueCharsInAString2 {

	public static final boolean[] flagArray = new boolean[26];

	public boolean hasUniqueChars(String input) {
		boolean result = true;
		if (input.isEmpty() || input.length() == 1) {
			return result;
		}

		for (char c : input.toCharArray()) {
			if (flagArray[c - 'a']) {
				result = false;
				break;
			} else {
				flagArray[c - 'a'] = true;
			}
		}
		System.out.println(Arrays.toString(flagArray));
		return result;
	}

	public static void main(String[] args) {
		UniqueCharsInAString2 algo = new UniqueCharsInAString2();
		System.out.println(algo.hasUniqueChars("hello"));
	}
}
