package phanindra.dsangalgos.dividetwointegers;

public class Solution {
	public int divide(int dividend, int divisor) {

		if (divisor == 0 || dividend == Integer.MAX_VALUE
				|| divisor == Integer.MAX_VALUE
				|| dividend == Integer.MIN_VALUE
				|| divisor == Integer.MIN_VALUE) {
			return Integer.MAX_VALUE;
		}

		boolean divisorNegative = divisor < 0;
		boolean dividendNegative = dividend < 0;

		boolean bothNegativeOperands = divisorNegative && dividendNegative;
		boolean oneNegativeOperand = divisorNegative ^ dividendNegative;

		if (dividendNegative && oneNegativeOperand) {
			dividend = -dividend;
		}
		if (divisorNegative && oneNegativeOperand) {
			divisor = -divisor;
		}

		if (bothNegativeOperands) {
			dividend = -dividend;
			divisor = -divisor;
		}

		if (dividend < divisor) {
			return 0;
		}

		int result = 0;
		int remaining = dividend;
		while (remaining >= divisor) {
			remaining -= divisor;
			result++;
		}
		return oneNegativeOperand ? -result : result;
	}
}
