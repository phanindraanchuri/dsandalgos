package phanindra.pascalstriangle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.math.BigInteger;

/**
 * [ [1], [1,1], [1,2,1], [1,3,3,1], [1,4,6,4,1] ]
 *
 */
public class Solution {
	public List<List<Integer>> generate(int numRows) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (numRows == 0) {
			return result;
		}
		List<Integer> currentList = Arrays.asList(1);
		result.add(currentList);
		for (int i = 1; i < numRows; i++) {
			List<Integer> nextList = Solution.generateNextList(currentList);
			result.add(nextList);
			currentList = nextList;
		}
		return result;
	}

	public static List<Integer> generateNextList(List<Integer> currentList) {
		List<Integer> nextList = new ArrayList<>();
		nextList.add(1);
		for (int i = 1; i < currentList.size(); i++) {
			nextList.add(currentList.get(i - 1) + currentList.get(i));
		}
		nextList.add(1);
		return nextList;
	}

	/**
	 * The kth row in a pascal triangle is k choose 0, k choose 1, k choose
	 * 2....k choose k, The formula for k choose r is k!/r! * (k-r)!
	 * 
	 * @return
	 */
	public static List<Integer> generateKthRow(int k) {
		List<Integer> result = new ArrayList<>();
		for (int r = 0; r <= k; r++) {
			result.add(choose(k, r).intValue());
		}
		return result;
	}

	private static BigInteger choose(int k, int r) {
		return factorial(k).divide((factorial(r).multiply(factorial(k - r))));
	}

	private static BigInteger factorial(int n) {
		if (n == 0 || n == 1) {
			return BigInteger.ONE;
		}
		BigInteger[] factorials = new BigInteger[n + 1];
		factorials[0] = BigInteger.ONE;
		factorials[1] = BigInteger.ONE;
		for (int i = 2; i <= n; i++) {
			factorials[i] = BigInteger.valueOf(i).multiply(factorials[i - 1]);
		}
		return factorials[n];
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.generate(13));
	}
}
