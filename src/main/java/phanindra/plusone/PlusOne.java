package phanindra.plusone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlusOne {
	public static List<Integer> plusOne(List<Integer> digits) {
		List<Integer> result = new ArrayList<Integer>(Collections.nCopies(
				digits.size() + 1, 0));
		int carry = 1;
		int sum = 0;
		for (int index = digits.size() - 1; index >= 0; index--) {
			sum = carry + digits.get(index);
			result.set(index + 1, sum % 10);
			carry = sum / 10;
		}
		result.set(0, carry);
		if (carry == 0) {
			return result.subList(1, result.size());
		}
		return result;
	}
}
