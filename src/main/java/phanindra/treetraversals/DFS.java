package phanindra.treetraversals;

import java.util.ArrayDeque;
import java.util.Deque;

public class DFS {

	public void dfs(TreeNode root) {
		if (root == null) {
			return;
		}
		Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
		stack.addFirst(root);

		while (!stack.isEmpty()) {
			TreeNode current = stack.removeFirst();
			System.out.println(current.getVal());
			if (current.getRight() != null) {
				stack.addFirst(current.getRight());
			}
			if (current.getLeft() != null) {
				stack.addFirst(current.getLeft());
			}
		}
	}

	public static void main(String[] args) {
		TreeNode one = new TreeNode(1);
		TreeNode two = new TreeNode(2);
		TreeNode three = new TreeNode(3);
		one.setLeft(two);
		one.setRight(three);
		TreeNode four = new TreeNode(4);
		TreeNode five = new TreeNode(5);
		two.setLeft(four);
		two.setRight(five);
		TreeNode six = new TreeNode(6);
		TreeNode seven = new TreeNode(7);
		three.setLeft(six);
		three.setRight(seven);
		DFS dfs = new DFS();
		dfs.dfs(one);
	}
}
