package phanindra.treetraversals;

import java.util.Deque;
import java.util.ArrayDeque;

public class InOrderIterative {
	public void printInOrderIterative(TreeNode root) {
		Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
		stack.push(root);
		while (!stack.isEmpty()) {
			TreeNode current = stack.pop();
			if(current.getLeft() != null) {
				stack.push(current.getLeft());
			}
		}
	}
}
