package phanindra.treetraversals;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class LevelOrderTraversal {
	public List<List<Integer>> levelOrder(TreeNode root) {
		if (root == null) {
			return null;
		}

		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);
		List<List<Integer>> allLevels = new ArrayList<List<Integer>>();
		while (!queue.isEmpty()) {
			List<Integer> thisLevel = new ArrayList<Integer>();
			int queueSize = queue.size();
			for (int i = 0; i < queueSize; i++) {
				if (queue.peek().getLeft() != null)
					queue.offer(queue.peek().getLeft());
				if (queue.peek().getRight() != null)
					queue.offer(queue.peek().getRight());
				thisLevel.add(queue.poll().getVal());
			}
			allLevels.add(thisLevel);
		}
		return allLevels;
	}

	public static void main(String[] args) {
		TreeNode six = new TreeNode(6);
		TreeNode four = new TreeNode(4);
		TreeNode ten = new TreeNode(10);
		TreeNode two = new TreeNode(2);
		TreeNode five = new TreeNode(5);
		TreeNode seven = new TreeNode(7);
		TreeNode eleven = new TreeNode(11);

		TreeNode one = new TreeNode(1);
		TreeNode three = new TreeNode(3);
		TreeNode nine = new TreeNode(9);
		TreeNode eight = new TreeNode(8);

		six.setLeft(four);
		six.setRight(ten);

		four.setLeft(two);
		four.setRight(five);
		ten.setLeft(seven);
		ten.setRight(eleven);

		two.setLeft(one);
		two.setRight(three);

		seven.setRight(nine);
		nine.setLeft(eight);

		LevelOrderTraversal lot = new LevelOrderTraversal();
		System.out.println(lot.levelOrder(six));
	}
}
