package phanindra.treetraversals;

import java.util.ArrayDeque;
import java.util.Deque;

public class BFS {
	public void bfs(TreeNode root) {
		if (root == null) {
			return;
		}
		Deque<TreeNode> queue = new ArrayDeque<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			TreeNode current = queue.remove();
			System.out.println(current.getVal());
			if (current.getLeft() != null) {
				queue.add(current.getLeft());
			}
			if (current.getRight() != null) {
				queue.add(current.getRight());
			}
		}
	}
	
	public static void main(String[] args) {
		TreeNode one = new TreeNode(1);
		TreeNode two = new TreeNode(2);
		TreeNode three = new TreeNode(3);
		one.setLeft(two);
		one.setRight(three);
		TreeNode five = new TreeNode(5);
		TreeNode six = new TreeNode(6);
		two.setLeft(five);
		two.setRight(six);
		TreeNode nine = new TreeNode(9);
		TreeNode ten = new TreeNode(10);
		five.setLeft(nine);
		five.setRight(ten);
		BFS bfs = new BFS();
		bfs.bfs(one);
	}
}
