package phanindra.leetcode.levelorder;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class Solution {
	public List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) {
			return null;
		} else {
			Queue<TreeNode> q = new ArrayDeque<TreeNode>();
			q.add(root);
			List<Integer> level = new ArrayList<Integer>();
			int len = q.size();
			while (!q.isEmpty()) {
				for (int i = 0; i < len; i++) {
					TreeNode t = q.peek();
					level.add(t.val);
					q.remove();
					if (t.left != null) {
						q.add(t.left);
					}
					if (t.right != null) {
						q.add(t.right);
					}
				}
				result.add(level);
			}
			return result;
		}
	}
	
	public static void main(String[] args) {
		
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		Solution s = new Solution();
		s.levelOrder(root);
	}
}