package phanindra.leetcode.bullsandcows;

import java.util.HashMap;
import java.util.Map;

public class Solution {
	public String getHint(String secret, String guess) {
		int bulls = 0;
		int cows = 0;
		Map<Character, Integer> secretMap = constructMap(secret);
		Map<Character, Integer> guessMap = constructMap(guess);
		for (Character c : secretMap.keySet()) {
			if (guessMap.containsKey(c)) {
				if (secretMap.get(c) == guessMap.get(c)) {
					bulls++;
				} else {
					cows++;
				}
			}
		}
		return bulls + "A" + cows + "B";
	}

	private Map<Character, Integer> constructMap(String input) {
		Map<Character, Integer> m = new HashMap<>();
		for (int index = 0; index < input.toCharArray().length; index++) {
			m.put(input.charAt(index), index);
		}
		return m;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.getHint("1", "0"));
	}
}
