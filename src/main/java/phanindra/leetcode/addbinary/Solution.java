package phanindra.leetcode.addbinary;

public class Solution {
	public String addBinary(String a, String b) {
		int aLength = a.length();
		int bLength = b.length();
		int diff = Math.abs(aLength - bLength);
		String pad = "";
		for (int i = 0; i < diff; i++) {
			pad = pad + "0";
		}
		if (aLength > bLength) {
			b = pad + b;
		} else if (aLength < bLength) {
			a = pad + a;
		}
		System.out.println(a);
		System.out.println(b);
		String result = "";
		int carry = 0;
		for (int x = a.length() - 1; x >= 0; x--) {
			int firstBit = a.charAt(x) - '0';
			int secondBit = b.charAt(x) - '0';

			// boolean expression for sum of 3 bits
			int sum = (firstBit ^ secondBit ^ carry) + '0';

			result = (char) sum + result;

			// boolean expression for 3-bit addition
			carry = (firstBit & secondBit) | (secondBit & carry)
					| (firstBit & carry);
		}
		if (carry > 0)
			result = '1' + result;
		return result;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.addBinary("11", "1"));
	}
}
