package phanindra.leetcode.addbinary;

/**
 * Created by phanindra on 8/23/16.
 */
public class Take2 {
    public static String addBinary(String a, String b) {
        if(a == null || b == null) {
            return "";
        }

        // No padding necessary, start from the right and look at the strings in full
        int first = a.length() - 1;
        int second = b.length() - 1;

        int carry = 0;

        StringBuilder sb = new StringBuilder();

        // Look at both strings in full, even if the other one runs out.
        while(first >=0 || second >= 0) {
            int sum = carry;
            if(first >= 0) {
                sum += a.charAt(first)-'0';
                first--;
            }
            if(second >= 0) {
                sum+=b.charAt(second) - '0';
                second--;
            }


            carry = sum >> 1;
            sum = sum & 1;
            sb.append(sum == 0 ? '0' : '1' );
        }
        if(carry > 0) {
            sb.append('1');
        }
        return String.valueOf(sb.reverse());
    }

    public static void main(String[] args) {
        //System.out.println(Take2.addBinary(null, "10"));
        //System.out.println(Take2.addBinary("10", null));
        System.out.println(Take2.addBinary("11", "11"));
    }
}
