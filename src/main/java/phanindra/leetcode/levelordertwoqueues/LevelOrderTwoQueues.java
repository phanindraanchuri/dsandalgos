package phanindra.leetcode.levelordertwoqueues;

import java.util.ArrayList;
import java.util.List;
import java.util.Deque;
import java.util.ArrayDeque;

public class LevelOrderTwoQueues {

  public static List<List<Integer>> levelOrder(TreeNode root) {
    List<List<Integer>> allLists = new ArrayList<List<Integer>>();
    if (root == null) {
      return allLists;
    }
    Deque<TreeNode> currentLevel = new ArrayDeque<TreeNode>();
    Deque<TreeNode> nextLevel = new ArrayDeque<TreeNode>();
    currentLevel.add(root);

    while (!currentLevel.isEmpty()) {
      List<Integer> thisLevel = new ArrayList<Integer>();
      while (!currentLevel.isEmpty()) {
        TreeNode t = currentLevel.pollFirst();
        thisLevel.add(t.val);
        if (t.left != null) {
          nextLevel.add(t.left);
        }
        if (t.right != null) {
          nextLevel.add(t.right);
        }
      }
      Deque<TreeNode> temp = currentLevel;
      currentLevel = nextLevel;
      nextLevel = temp;
      allLists.add(thisLevel);
    }
    return allLists;
  }

  public static void main(String[] args) {
    TreeNode root = new TreeNode(4);
    root.left = new TreeNode(2);
    root.right = new TreeNode(7);
    root.left.left = new TreeNode(1);
    root.left.right = new TreeNode(8);
    root.right.left = new TreeNode(6);
    root.right.right = new TreeNode(9);
    System.out.println(LevelOrderTwoQueues.levelOrder(root));
  }
}
