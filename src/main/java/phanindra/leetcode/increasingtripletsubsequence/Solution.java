package phanindra.leetcode.increasingtripletsubsequence;

import java.util.Arrays;

public class Solution {
  private static boolean increasingTriplet(int[] nums) {
    int len = nums.length;
    if (len < 3) {
      return false;
    }
    int[] l = new int[len];
    for (int i = 0; i < len; i++) {
      l[i] = 1;
    }
    for (int i = 0; i < len; i++) {
      int maxL = findMaxL(i, l, nums);
      l[i] = 1 + maxL;
      if(l[i] == 3) {
        return true;
      }
    }
    System.out.println(Arrays.toString(l));
    return false;
  }

  private static int findMaxL(int i, int[] l, int[] nums) {
    int max = Integer.MIN_VALUE;
    for (int j = i - 1; j >= 0; j--) {
      if (nums[j] < nums[i] && l[j] > max) {
        max = l[j];
      }
    }
    return max != Integer.MIN_VALUE
        ? max
        : 0;
  }

  public static void main(String[] args) {
    System.out.println(Solution.increasingTriplet(new int[] { 5, 1, 5, 5, 2, 5, 4 }));
  }
}
