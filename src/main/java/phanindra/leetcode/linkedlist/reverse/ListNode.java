package phanindra.leetcode.linkedlist.reverse;

public class ListNode {
	ListNode next;
	int val;

	ListNode(int v) {
		val = v;
	}

	@Override
	public String toString() {
		return " " + val;
	}
}
