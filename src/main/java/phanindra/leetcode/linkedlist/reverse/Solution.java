package phanindra.leetcode.linkedlist.reverse;

public class Solution {
  public ListNode reverseList(ListNode head) {
    // Initialization - The only node we know about is current.
    ListNode previous = null;
    ListNode next = null;
    ListNode current = head;
    // Is current still pointing at a valid node
    while (current != null) {
      // next is current.next
      next = current.next;
      // Make current's next point to the previous
      current.next = previous;
      // Setup previous for the next iteration, previous for the next iteration is current from this iteration
      previous = current;
      // Setup current for the next iteration, current for the next iteration is next from this iteration
      current = next;
    }
    // When the while is done, previous points to the last node, which is the head to the reversed list
    return previous;
  }
}
