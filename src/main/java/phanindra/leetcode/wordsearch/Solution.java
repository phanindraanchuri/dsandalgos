package phanindra.leetcode.wordsearch;

import java.util.List;
import java.util.ArrayList;

public class Solution {

	private char[][] board;

	private boolean[][] visited;

	private static Move[] validMoves = new Move[] { Move.TOP, Move.BOTTOM,
			Move.LEFT, Move.RIGHT };

	public Solution(char[][] board) {
		this.board = board;
		this.visited = new boolean[board.length][board[0].length];
	}

	public boolean exist(char[][] board, String word) {
		boolean exists = false;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (existsRecursive(word, i, j, 0, new ArrayList<Move>())) {
					System.out.println("Starting at i "+ i + " j "+ j + " move ");
					exists = true;
					break;
				}
			}
		}
		return exists;
	}

	private boolean existsRecursive(String word, int i, int j, int wordIndex, List<Move> moves) {
		if (!indexesInRange(i, j)) {
			return false;
		}
		char boardChar = board[i][j];
		char wordChar = word.charAt(wordIndex);
		if (boardChar == wordChar && !isVisited(i, j)) {
			visit(i, j);
			if (wordIndex == word.length() - 1) {
				System.out.println(moves);
				return true;
			} else {
				for (Move validMove : validMoves) {
					int nextX = i + validMove.getX();
					int nextY = j + validMove.getY();
					moves.add(validMove);
					if (existsRecursive(word, nextX, nextY, wordIndex + 1, moves)) {
						return true;
					}
					moves.remove(moves.size() - 1);
				}
			}
			unvisit(i, j);
		}
		return false;
	}

	private void unvisit(int i, int j) {
		visited[i][j] = false;
	}

	private void visit(int i, int j) {
		visited[i][j] = true;
	}

	private boolean isVisited(int i, int j) {
		return visited[i][j];
	}

	private boolean indexesInRange(int i, int j) {
		return (i >= 0 && i < board.length && j >= 0 && j < board[0].length);
	}

	enum Move {
		LEFT(0, -1), RIGHT(0, 1), TOP(-1, 0), BOTTOM(1, 0);

		private int x;
		private int y;

		private Move(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}

	}

	public static void main(String[] args) {
		char[][] board = { { 'a', 'b', 'c', 'e' }, { 's', 'f', 'c', 's' },
				{ 'a', 'd', 'e', 'e' } };
		Solution s = new Solution(board);
		System.out.println(s.exist(board, "eed"));
	}
}
