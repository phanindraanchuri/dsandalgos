package phanindra.leetcode.graycode;

import java.util.ArrayList;
import java.util.List;

public class GrayCode {
  public List<String> grayCode(int n) {
    if (n == 1) {
      List<String> res = new ArrayList<>();
      res.add("0");
      res.add("1");
      return res;
    }
    else {
      List<String> result = new ArrayList<>();
      List<String> rem = grayCode(n - 1);
      for (int i = 0; i < rem.size(); i++) {
        String s = "0" + rem.get(i);
        result.add(s);
      }
      for (int i = rem.size() - 1; i >= 0; i--) {
        String s = "1" + rem.get(i);
        result.add(s);
      }
      return result;
    }
  }

  public List<Integer> grayCodeInt(int n) {
    List<Integer> result = new ArrayList<>();
    if (n > 0) {
      graycodeIntHelper(n, result);
    }
    else {
      result.add(0);
    }
    return result;
  }

  private void graycodeIntHelper(int n, List<Integer> result) {
    if (n == 1) {
      result.add(0);
      result.add(1);
      return;
    }
    graycodeIntHelper(n - 1, result);
    int size = result.size() - 1;
    for (int i = size; i >= 0; i--) {
      int num = result.get(i) | (1 << (n - 1));
      result.add(num);
    }
  }

  public static void main(String[] args) {
    GrayCode gc = new GrayCode();
    System.out.println(gc.grayCode(2));
    System.out.println(gc.grayCodeInt(3));
  }
}
