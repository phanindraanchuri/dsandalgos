package phanindra.leetcode.validatebst;

import java.util.List;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;

import phanindra.treetraversals.TreeNode;

public class BSTValidator {
  boolean isValidBST(TreeNode root) {
    List<Integer> result = new ArrayList<>();

    // In order traversal using stack
    TreeNode current = root;
    Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
    boolean done = false;
    while (!done) {
      if (current != null) {
        stack.push(current);
        current = current.getLeft();
      }
      else {
        if (stack.isEmpty()) {
          done = true;
        }
        else {
          current = stack.peek();
          stack.pop();
          result.add(current.getVal());
          current = current.getRight();
        }
      }
    }
    // Check if the in order traversal is sorted
    for (int i = 1; i < result.size() - 1; i++) {
      if (result.get(i - 1) > result.get(i)) {
        return false;
      }
    }
    return true;
  }

  public static void main(String[] args) {
    // 10
    // / \
    // 5 15 -------- binary tree (1)
    // / \
    // 6 20
    TreeNode ten = new TreeNode(10);
    TreeNode five = new TreeNode(5);
    TreeNode fifteen = new TreeNode(15);
    TreeNode six = new TreeNode(6);
    TreeNode twenty = new TreeNode(20);
    ten.setLeft(five);
    ten.setRight(fifteen);
    fifteen.setLeft(six);
    fifteen.setRight(twenty);
    BSTValidator validator = new BSTValidator();
    System.out.println(validator.isValidBST(ten));
  }
}
