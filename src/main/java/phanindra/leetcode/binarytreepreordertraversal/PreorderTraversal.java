package phanindra.leetcode.binarytreepreordertraversal;

import java.util.List;
import java.util.ArrayList;
import java.util.Stack;

public class PreorderTraversal {
    public List<Integer> iterative(TreeNode root) {
        List<Integer> result = new ArrayList<>();

        if(root == null) {
            return result;
        }

        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);

        while(!stack.isEmpty()) {
            TreeNode node = stack.pop();
            result.add(node.getVal());

            if(node.getRight() != null) {
                stack.push(node.getRight());
            }

            if(node.getLeft() != null) {
                stack.push(node.getLeft());
            }
        }

        return result;
    }

    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        preorderRecursive(root, result);
        return result;
    }

    void preorderRecursive(TreeNode root, List<Integer> result) {
        if(root == null) {
            return;
        }
        result.add(root.getVal());
        preorderRecursive(root.getLeft(), result);
        preorderRecursive(root.getRight(), result);
    }

}
