package phanindra.leetcode.reversenumber;

public class Solution {
	public static int reverse(int x) {
		boolean isNegative = x < 0;
		int copy = x > 0 ? x : -x;
		int result = 0;
		while (copy > 0) {
			// Look here https://www.securecoding.cert.org/confluence/display/java/NUM00-J.+Detect+or+prevent+integer+overflow
			if(result > Integer.MAX_VALUE/10) {
				return 0;
			}
			result = result * 10 + copy % 10;
			System.out.println("Result " + result);
			copy = copy / 10;
		}
		return isNegative ? -result : result;
	}
}
