package phanindra.leetcode.wordbreak;

import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;

public class Solution {
	public String wordBreak(String input, Set<String> dictionary) {
		if (input == null || dictionary.contains(input)) {
			return input;
		}
		for (int i = 1; i < input.length(); i++) {
			String prefix = input.substring(0, i);
			if (dictionary.contains(prefix)) {
				String suffix = input.substring(i, input.length());
				String segmentSuffix = wordBreak(suffix, dictionary);
				if (segmentSuffix != null) {
					return prefix + " " + segmentSuffix;
				}
			}
		}
		return null;
	}

	public boolean wordBreakCheck(String s, Set<String> wordDict) {
		if (s == null) {
			return false;
		}
		if (wordDict.contains(s)) {
			return true;
		}
		for (int i = 1; i < s.length(); i++) {
			String prefix = s.substring(0, i);
			if (wordDict.contains(prefix)) {
				String suffix = s.substring(i, s.length());
				return wordBreakCheck(suffix, wordDict);
			}
		}
		return false;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		Set<String> dictionary = new HashSet<String>(Arrays.asList("iam", "a",
				"good", "guy"));
		String input = "iamagoodguy";
		System.out.println(s.wordBreakCheck(input, dictionary));
	}
}
