package phanindra.leetcode.titletonumber;

public class Solution {
	public static int titleToNumber(String s) {
		int result = 0;
		char[] input = s.toCharArray();
		int len = input.length;
		for (int k = 0; k < len; k++) {
			int m = (int) Math.pow(26, len - k - 1);
			int val = input[k] - 'A' + 1;
			result = result + (m * val);
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(Solution.titleToNumber("AZ"));
	}
}