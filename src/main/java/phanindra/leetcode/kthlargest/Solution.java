package phanindra.leetcode.kthlargest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class Solution {
	public static int findKthLargest(int[] nums, int k) {
		if(nums.length == 0) {
			return 0;
		}
		List<Integer> list = new ArrayList<>();
		for (int num : nums) {
			list.add(num);
		}
		PriorityQueue<Integer> pq = new PriorityQueue<>(
				Collections.reverseOrder());
		pq.addAll(list);
		for (int i = 0; i < k - 1; i++) {
			pq.remove();
		}
		return pq.remove();
	}

	public static void main(String[] args) {
		System.out.println(Solution.findKthLargest(new int[] {}, 1));
	}
}
