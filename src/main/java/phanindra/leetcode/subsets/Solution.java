package phanindra.leetcode.subsets;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Solution {
  public List<List<Integer>> subsets(int[] nums) {
    Arrays.sort(nums);
    List<List<Integer>> res = new ArrayList<>();
    List<Integer> each = new ArrayList<>();
    helper(res, each, 0, nums);
    System.out.println(res);
    return res;
  }

  public void helper(List<List<Integer>> res, List<Integer> each, int pos, int[] n) {
    if (pos <= n.length) {
      res.add(each);
    }
    for (int i = pos; i < n.length; i++) {
      each.add(n[i]);
      helper(res, new ArrayList<>(each), i + 1, n);
      each.remove(each.size() - 1);
    }
    return;
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    s.subsets(new int[] { 1, 2, 3 });
  }
}
