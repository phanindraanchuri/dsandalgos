package phanindra.leetcode.subsets;

import java.util.List;
import java.util.ArrayList;

/**
 * https://www.youtube.com/watch?v=3LqlTROMffg
 * https://www.youtube.com/watch?v=NdF1QDTRkck
 */
public class Subsets2 {

    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if (nums == null) {
            return res;
        }
        List<Integer> tmp = new ArrayList<>();
        subsetHelper(res, tmp, nums, 0);
        return res;
    }

    private void subsetHelper(List<List<Integer>> result, List<Integer> tmp, int[] nums, int start) {
        if (start == nums.length) {
            result.add(new ArrayList<>(tmp));
        } else {
            // Include nums[start] in the subset and recurse starting at the next position.
            tmp.add(nums[start]);
            subsetHelper(result, tmp, nums, start + 1);
            // Undo choice of including nums[start] in subset and recurse starting at the next position
            tmp.remove(tmp.size() - 1);
            subsetHelper(result, tmp, nums, start + 1);
        }
    }

    public static void main(String[] args) {
        Subsets2 sub2 = new Subsets2();
        System.out.println(sub2.subsets(new int[]{1, 2, 3}));
    }
}
