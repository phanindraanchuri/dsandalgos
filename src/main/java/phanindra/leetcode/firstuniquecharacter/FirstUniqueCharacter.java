package phanindra.leetcode.firstuniquecharacter;

public class FirstUniqueCharacter {
    public int firstUniqChar(String s) {
        int[] counts = new int[256];
        char[] chars = s.toCharArray();
        for(char c: chars) {
            counts[c-'a']++;
        }
        int index = -1;
        for(int i = 0; i < chars.length; i++) {
            if(counts[chars[i]-'a'] == 1) {
                return i;
            }
        }
        return index;
    }
}