package phanindra.leetcode.spiralmatrix;

import java.util.List;
import java.util.ArrayList;

/**
 * 1 2 3 4
 * 5 6 7 8
 * 9 10 11 12
 * 12 13 14 15 16
 * 
 * 1 2 3 
 * 4 5 6
 * 7 8 9
 * 
 * 1 2 3 4 5
 * 6 7 8 9 1
 * 1 1 1 1 1
 * 1 1 1 1 2
 * 2 2 2 2 2
 * @author phanindra
 *
 */
public class Solution {
	static List<Integer> spiral(int[][] matrix) {
		// 0,0 0,1 0,2 0,3
		// 1,3 2,3 3,3
		// 3,2 3,1 3,0
		// 2,0 1,0
		List<Integer> result = new ArrayList<>();
		int count = 0;
		int row = 0;
		int col = 0;
		int len = matrix[0].length;
		while(count <= 8) {
			for(int c = col; c<len; c++) {
				result.add(matrix[row][c]);
				count++;
			}
			for(int r = row; r<len; r++) {
				result.add(matrix[r][col]);
				count++;
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
        int[][] matrix = {{0,1,2},{3,4,5}, {6,7, 8}};
		System.out.println(Solution.spiral(matrix));
	}
}
