package phanindra.leetcode.lettercombinationsofphonenumber;

import java.util.ArrayList;
import java.util.List;

public class LetterCombinationsOfPhoneNumber {

  // Using a String[] as a map where the key is the index
  // The first two Strings in the array are empty because the keypad has no letters corresponding to 0 and 1
  private String[] digitMap = new String[] { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };

  public List<String> letterCombinations(String digits) {
    List<String> result = new ArrayList<String>();
    letterCombinationsHelper(digits, 0, new StringBuffer(), result);
    return result;
  }

  private void letterCombinationsHelper(String digits, int index, StringBuffer temp, List<String> result) {
    int length = digits.length();
    // If we've looked at all the digits, done so return (ArrayIndexOutOfBounds if you don't return)
    if (index == length) {
      result.add(temp.toString());
      return;
    }
    String s = digitMap[digits.charAt(index) - '0'];
    for (int i = 0; i < s.length(); i++) {
      temp.append(s.charAt(i));
      letterCombinationsHelper(digits, index + 1, temp, result);
      // Backtracking step
      temp.deleteCharAt(temp.length() - 1);
    }
  }

  public static void main(String[] args) {
    LetterCombinationsOfPhoneNumber lcp = new LetterCombinationsOfPhoneNumber();
    System.out.println(lcp.letterCombinations("23"));
  }
}
