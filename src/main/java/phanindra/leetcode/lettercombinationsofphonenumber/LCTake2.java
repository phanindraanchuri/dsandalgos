package phanindra.leetcode.lettercombinationsofphonenumber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by phanindra on 9/24/16.
 */
public class LCTake2 {
    private static final Map<Character, String> DIGIT_MAP;

    static {
        DIGIT_MAP = new HashMap<>();
        DIGIT_MAP.put('2', "abc");
        DIGIT_MAP.put('3', "def");
        DIGIT_MAP.put('4', "ghi");
        DIGIT_MAP.put('5', "jkl");
        DIGIT_MAP.put('6', "mno");
        DIGIT_MAP.put('7', "pqrs");
        DIGIT_MAP.put('8', "tuv");
        DIGIT_MAP.put('9', "wxyz");
    }

    public List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<>();
        lcHelper(0, digits, new StringBuffer(), result);
        return result;
    }

    void lcHelper(int i, String digits, StringBuffer current, List<String> result) {
        if(i == digits.length()) {
            result.add(current.toString());
            return;
        } else {
            char digitChar = digits.charAt(i);
            String digitString = DIGIT_MAP.get(digitChar);
            for(int ds = 0; ds < digitString.length(); ds++) {
                current.append(digitString.charAt(ds));
                lcHelper(i+1, digits, current, result);
                current.deleteCharAt(current.length() - 1);
            }
        }
    }

    public static void main(String[] args) {
        LCTake2 lcp = new LCTake2();
        System.out.println(lcp.letterCombinations("22"));
    }
}
