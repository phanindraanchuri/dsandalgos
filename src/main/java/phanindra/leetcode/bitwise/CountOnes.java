package phanindra.leetcode.bitwise;

public class CountOnes {
	public static int count(int n) {
		int count = 0;
		while(n > 0) {
			if(n%2 == 1) {
				count++;
			}
			n = n >> 1;
		}
		return count;
	}
	
	public static int count2(int n) {
		int count = 0;
		while(n != 0) {
			n = n & (n-1);
			count++;
		}
		return count;
	}
	
	public static void main(String[] args) {
		System.out.println(CountOnes.count(2147483647));
	}
}
