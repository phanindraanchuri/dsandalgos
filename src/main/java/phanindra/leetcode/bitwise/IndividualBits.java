package phanindra.leetcode.bitwise;

import java.util.Arrays;

public class IndividualBits {
	public static int[] reverseBits(int n) {
		int[] bitArray = new int[32];
		int bitArrayIndex = 0;
		while(n != 0) {
			int r = n%2;
			bitArray[bitArrayIndex] = r;
			n = n/2;
			bitArrayIndex++;
		}
		return bitArray;
	}
	
	public static int bitArrayToInt(int[] bits) {
		int k = bits.length - 1;
		int result = 0;
		for(int bit : bits) {
			result += Math.pow(2, k) * bit;
			k--;
		}
		return result;
	}
	
	public static void main(String[] args) {
		int n = 43261596;
		int[] reverseBits = IndividualBits.reverseBits(n);
		System.out.println(Arrays.toString(reverseBits));
		System.out.println(IndividualBits.bitArrayToInt(reverseBits));
	}
}
