package phanindra.leetcode.adddigits;

import java.util.Stack;

public class Solution {
	public int addDigits(int num) {
		int result = addDigitsNum(num);
		if (result > 9) {
			return new Solution().addDigits(result);
		}
		return result;
	}

	private int addDigitsNum(int num) {
		int result = 0;
		String numString = Integer.toString(num);
		for (char numChar : numString.toCharArray()) {
			result += numChar - '0';
		}
		return result;
	}

	public int addDigitsStack(int num) {
		Stack<Integer> s = new Stack<>();
		s.push(num);
		while (!s.isEmpty()) {
			int n = s.peek();
			if (n > 9) {
				s.pop();
				s.push(addDigitsNum(n));
			} else {
				break;
			}
		}
		return s.pop();
	}

	public int addDigitsModMethod(int num) {
		// dr(n) = 0 if n is 0
		//       = 9 if n mod 9 is 0
		//       = n mod 9 otherwise
		if (num == 0) {
			return num;
		} else if (num % 9 == 0) {
			return 9;
		} else {
			return num % 9;
		}
	}
	
	public int addDigitsModFormula(int num) {
		// dr(n) = 1 + ((n-1) mod 9)
		return 1 + (num-1) % 9;
	}
	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.addDigitsModFormula(19));
	}
}
