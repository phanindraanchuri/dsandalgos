package phanindra.leetcode.besttimetobuyandsellstock;

/**
 * Single buy and Single sell only allowed.
 */
public class SolutionOneTransaction {

    /**
     * Intuition: DP
     * Suppose we knew the answer for the first k elements,
     * how can we use this combined with the k+1 st element
     * to solve the problem for k+1 elements ?
     *
     * If you had 1 element, that is the best buy/sell pair with a profit of 0.
     *
     * Suppose we knew the best answer for the first k elements and
     * look at the k+1st element, the ONLY way this element can create
     * a better solution than what we had for the first k elements is
     * IF THE DIFFERENCE BETWEEN THE SMALLEST OF THE FIRST K ELEMENTS
     * AND THE NEW ELEMENT IS BIGGER THAN THE BIGGEST DIFFERENCE WE'VE
     * COMPUTED SO FAR.
     *
     * => We need to keep track of two values as we go through our elements.
     * a. Smalles element/ minimum seen so far
     * b. Maximum profit we can make with just the first k elements.
     *
     * Initially min value is first element, max profit = 0
     *
     * When we see a new element,
     *  first update the optimal profit by computing how much we would make
     *  by buying at the lowest price seen so far and selling at the current price.
     *  If this is better than the max profit we've computed so far,
     *  UPDATE THE OPTIMAL SOLUTION TO BE THIS NEW PROFIT.
     *  second, update hte minimum element seen so far to be the minimum of the
     *  smallest element so far and the new element.
     */
    public int maxProfit(int[] prices) {
        if(prices == null || prices.length < 2) {
            return 0;
        }
        int min = prices[0];
        int maxProfit = 0;
        for(int i = 1; i < prices.length; i++) {
            if(prices[i] - min > maxProfit) {
                maxProfit = prices[i] - min;
            }
            if(prices[i] < min) {
                min = prices[i];
            }
        }
        return maxProfit;
    }
}
