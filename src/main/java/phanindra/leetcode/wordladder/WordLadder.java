package phanindra.leetcode.wordladder;

import java.util.*;

public class WordLadder {

    class Entry {
        private String word;

        private int steps;

        public Entry(String word, int steps) {
            this.word = word;
            this.steps = steps;
        }

        public String getWord() {
            return word;
        }

        public int getSteps() {
            return steps;
        }
    }

    public int ladderLength(String beginWord, String endWord, Set<String> wordList) {
        Set<String> allWords = new HashSet<>();
        allWords.addAll(wordList);
        allWords.add(beginWord);
        allWords.add(endWord);
        Map<String, Set<String>> m = constructMap(allWords);
        return bfs(beginWord, endWord, m);
    }

    private int bfs(String beginWord, String endWord, Map<String, Set<String>> m) {
        Queue<Entry> q = new ArrayDeque<>();
        Set<String> visited = new HashSet<>();
        q.add(new Entry(beginWord, 1));
        while (!q.isEmpty()) {
            Entry e = q.poll();
            String word = e.getWord();
            if (!visited.contains(word)) {
                visited.add(word);
                if (word.equals(endWord)) {
                    return e.getSteps();
                }
                for (int i = 0; i < word.length(); i++) {
                    String key = word.substring(0, i) + "_" + word.substring(i + 1, word.length());
                    Set<String> neighboringWords = m.get(key);
                    for (String neighboringWord : neighboringWords) {
                        if (!visited.contains(neighboringWord)) {
                            int steps = e.getSteps();
                            q.add(new Entry(neighboringWord, steps + 1));
                        }
                    }
                }
            }
        }
        return -1;
    }

    private Map<String, Set<String>> constructMap(Set<String> allWords) {
        Map<String, Set<String>> m = new HashMap<>();
        for (String word : allWords) {
            for (int i = 0; i < word.length(); i++) {
                String key = word.substring(0, i) + "_" + word.substring(i + 1, word.length());
                if (m.containsKey(key)) {
                    Set<String> existingSet = m.get(key);
                    existingSet.add(word);
                    m.put(key, existingSet);
                } else {
                    Set<String> s = new HashSet<>();
                    s.add(word);
                    m.put(key, s);
                }
            }
        }
        return m;
    }

    public static void main(String[] args) {
        WordLadder ladder = new WordLadder();
        String[] words = new String[]{"hot", "dog"};
        Set<String> wordList = new HashSet<>();
        Collections.addAll(wordList, words);
        System.out.println(ladder.ladderLength("hot", "dog", wordList));
    }
}