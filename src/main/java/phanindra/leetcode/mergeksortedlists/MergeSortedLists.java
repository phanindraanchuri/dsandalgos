package phanindra.leetcode.mergeksortedlists;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class MergeSortedLists {
  int[] merge(int[][] a) {
    int k = a.length;
    int n = a[0].length;
    int[] indices = new int[k];
    int[] result = new int[n * k];
    PriorityQueue<ElementOrigin> heap = new PriorityQueue<>(new Comparator<ElementOrigin>() {
      @Override
      public int compare(ElementOrigin o1, ElementOrigin o2) {
        return Integer.compare(o1.element, o2.element);
      }
    });

    for (int i = 0; i < k; i++) {
      heap.add(new ElementOrigin(a[i][0], i));
    }

    for (int c = 0; c < result.length; c++) {
      ElementOrigin eo = heap.poll();
      result[c] = eo.element;
      indices[eo.origin] = indices[eo.origin] + 1;
      if (indices[eo.origin] < a[eo.origin].length) {
        heap.add(new ElementOrigin(a[eo.origin][indices[eo.origin]], eo.origin));
      }
    }
    return result;
  }

  class ElementOrigin {
    int element;

    int origin;

    public ElementOrigin(int element, int origin) {
      this.element = element;
      this.origin = origin;
    }
  }

  public static void main(String[] args) {
    MergeSortedLists msl = new MergeSortedLists();
    System.out.println(Arrays.toString(msl.merge(new int[][] { { 1, 3, 5, 7 }, { 2, 4, 6, 8 }, { 0, 9, 10, 11 } })));
  }
}
