package phanindra.leetcode.searchrotatedsorted;

public class Solution {
  public int search(int[] nums, int target) {
    if (nums == null || nums.length == 0) {
      return -1;
    }
    int low = 0;
    int high = nums.length - 1;

    while (low <= high) {
      int mid = low + (high - low) / 2;
      if (nums[mid] == target) {
        return mid;
      }
      // Check if the left half is sorted
      else if (nums[low] <= nums[mid]) {
        // Check if the element we're looking for is in the left half,
        // target < nums[mid] because we've already checked
        // for equality
        if (nums[low] <= target && target < nums[mid]) {
          // proceed with the left half, knowing that target != nums[mid]
          high = mid - 1;
        }
        else {
          low = mid + 1;
        }
      }
      // Check if the right half is sorted
      else if (nums[high] >= nums[mid]) {
        // Check if the element we're looking for is in the right half,
        // nums[mid] < target because we've already checked
        // for equality
        if (nums[mid] < target && target <= nums[high]) {
          // proceed with the right half, knowing that target != nums[mid]
          low = mid + 1;
        }
        else {
          high = mid - 1;
        }
      }
    }
    return -1;
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    System.out.println(s.search(new int[] { 4, 5, 6, 7, 0, 1, 2, 3 }, 0));
  }
}
