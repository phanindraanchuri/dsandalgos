package phanindra.leetcode.mergesort;

import java.util.Arrays;

class Solution {

    public static void main(String[] args) {
        int[] a = new int[]{5, 1, 4, 6, 3, 9};
        Solution s = new Solution();
        s.mergeSort(a);
        System.out.println(Arrays.toString(a));
    }

    void mergeSort(int[] a) {
        mergeSort(a, 0, a.length - 1);
    }

    void mergeSort(int[] a, int s, int e) {
        if (s < e) {
//            System.out.println("MS(" + s + "," + e + ")");
            int m = s + (e - s) / 2;
            mergeSort(a, s, m);
            mergeSort(a, m + 1, e);
            merge(a, s, m, e);
        }
    }

    void merge(int[] a, int s, int m, int e) {
//        System.out.println(" Merge called with s= " + s + " m=  " + m + " e= " + e);
        int[] b = new int[e - s + 1];

        int i = s;
        int j = m + 1;
        int bIndex = 0;

        while (i <= m && j <= e) {
            if (a[i] < a[j]) {
                b[bIndex] = a[i];
                i++;
            } else {
                b[bIndex] = a[j];
                j++;
            }
            bIndex++;
        }

//        System.out.println("b after first while " + Arrays.toString(b));
        while (i <= m) {
            b[bIndex] = a[i];
            i++;
            bIndex++;
        }

//        System.out.println("b after second while " + Arrays.toString(b));
        while (j <= e) {
            b[bIndex] = a[j];
            j++;
            bIndex++;
        }

//        System.out.println("b after third while " + Arrays.toString(b));

//        System.out.println("i = " + i + " j = " + j);

//        System.out.println("Going to copy back to a, " + Arrays.toString(b));

        int bb = 0;
        for (int k = s; k <= e; k++) {
            a[k] = b[bb];
            bb++;
        }
    }
}