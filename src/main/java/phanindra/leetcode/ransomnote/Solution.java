package phanindra.leetcode.ransomnote;

/**
 *  Given  an  arbitrary  ransom  note  string  and  another  string  containing  letters from  all  the  magazines,  write  a  function  that  will  return  true  if  the  ransom   note  can  be  constructed  from  the  magazines ;  otherwise,  it  will  return  false.   

 Each  letter  in  the  magazine  string  can  only  be  used  once  in  your  ransom  note.

 Note:
 You may assume that both strings contain only lowercase letters.

 canConstruct("a", "b") -> false
 canConstruct("aa", "ab") -> false
 canConstruct("aa", "aab") -> true
 */
public class Solution {
    public boolean canConstruct(String ransomNote, String magazine) {
        int[] noteArr = constructFreqArr(ransomNote);
        int[] magArr = constructFreqArr(magazine);
        for(int i = 0; i < 26; i++) {
            if(noteArr[i] != 0) {
                if(magArr[i] < noteArr[i]) {
                    return false;
                }
            }
        }
        return true;
    }

    private int[] constructFreqArr(String s) {
        int[] freqArr = new int[26];
        for(Character c : s.toCharArray()) {
            int index = c-'a';
            freqArr[index] = freqArr[index]+1;
        }
        return freqArr;
    }
}