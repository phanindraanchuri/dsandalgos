package phanindra.leetcode.uglynumber;

import java.util.ArrayList;
import java.util.List;

public class Solution {

	public boolean isUgly(int num) {
		if (num < 0 || num == Integer.MIN_VALUE) {
			num = (-1) * num;
		}
		if (num > Integer.MAX_VALUE || num < Integer.MIN_VALUE) {
			return false;
		}

		List<Integer> primes = findPrimes(num);
		// System.out.println(primes);
		for (int p = 2; p < primes.size() && num / primes.get(p) >= 2; p++) {
			System.out.println("Checking " + primes.get(p));
			if (num % primes.get(p) == 0) {
				System.out.println("Found " + primes.get(p));
				return false;
			}
		}
		return true;
	}

	private List<Integer> findPrimes(int num) {
		Double sqrt = Math.sqrt(num);
		System.out.println(sqrt);
		int size = sqrt.intValue();
		List<Integer> primes = new ArrayList<>();
		boolean[] sieve = new boolean[size];
		for (int j = 2; j < sieve.length; j++) {
			for (int i = j * 2; i < sieve.length; i = i + j) {
				if (!sieve[i]) {
					sieve[i] = true;
				}
			}
		}
		for (int x = 2; x < sieve.length; x++) {
			if (!sieve[x]) {
				primes.add(x);
			}
		}
		return primes;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.isUgly(1715933796));
	}
}
