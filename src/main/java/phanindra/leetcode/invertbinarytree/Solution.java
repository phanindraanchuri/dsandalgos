package phanindra.leetcode.invertbinarytree;

public class Solution {
	public TreeNode invert(TreeNode currentNode) {
		if(currentNode == null) {
			return null;
		}
		if (currentNode.isLeaf()) {
			return currentNode;
		} else {
			TreeNode leftInvert = invert(currentNode.left);
			TreeNode rightInvert = invert(currentNode.right);
			currentNode.left = rightInvert;
			currentNode.right = leftInvert;
			return currentNode;
		}
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(4);
		root.left = new TreeNode(2);
		root.right = new TreeNode(7);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(3);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(9);
		System.out.println(root.printTree());
		Solution s = new Solution();
		TreeNode invertedTree = s.invert(root);
		System.out.println(invertedTree.printTree());
	}
}
