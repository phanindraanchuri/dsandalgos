package phanindra.leetcode.invertbinarytree;

public class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}

	boolean isLeaf() {
		return left == null && right == null;
	}

	String printTree() {
		if (isLeaf()) {
			return "" + val;
		} else {
			String leftTree = left.printTree();
			String rightTree = right.printTree();
			return leftTree + val + rightTree;
		}
	}
}
