package phanindra.leetcode.sqrt;

public class Solution {
	public int sqrt(int x) {
		if(x < 2) {
			return x;
		}
		
		int l = 0, r = x, mid = 0;
		while(l <= r) {
			mid = l + (r-l)/2;
			if(mid <= x/mid) {
				l = mid+1;
			} else {
				r = mid-1;
			}
		}
		return r;
	}
	
	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.sqrt(625));
	}
}
