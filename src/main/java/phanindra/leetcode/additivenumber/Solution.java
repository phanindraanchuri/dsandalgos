package phanindra.leetcode.additivenumber;

public class Solution {
	public static boolean isAdditiveNumber(String num) {
		int len = num.length();
		if (len < 3) {
			return false;
		}
		int i = 0;
		int j = i + 1;
		while (i <= len - 3) {
			while (j <= len - 2) {
				int firstNum = Integer.parseInt(num.substring(i, i + 1));
				int secondNum = Integer.parseInt(num.substring(i + 1, j + 1));
				System.out.println("First num " + firstNum);
				System.out.println("Second num " + secondNum);
				int sum = firstNum + secondNum;
				System.out.println("Sum is " + sum);
				int inArraySum1 = Integer.parseInt(num.substring(j + 1, 2 * j
						- i + 1));
				int inArraySum2 = Integer.parseInt(num.substring(j + 1, 2 * j
						- i + 2));
				boolean match = sum == inArraySum1 || sum == inArraySum2;
				if (sum == inArraySum1) {
					i = j;
					j = j + 1;
					System.out.println("less digit match ");
				}
				if (sum == inArraySum2) {
					i = j;
					j = j + 2;
					System.out.println("more digit match ");
				}
				if (!match) {
					j++;
				}
			}
			i++;
		}
		return false;
	}

	public static void main(String[] args) {
		Solution.isAdditiveNumber("1235");
	}
}