package phanindra.leetcode.balancedbinarytree;

public class Solution {
	public boolean isBalanced(TreeNode root) {
		return root == null
				|| isBalanced(root.getLeft())
				&& isBalanced(root.getRight())
				&& Math.abs(height(root.getLeft()) - height(root.getRight())) <= 1;
	}

	public int height(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return Math.max(height(root.getLeft()), height(root.getRight())) + 1;
	}

	public static void main(String[] args) {
		TreeNode one = new TreeNode(1);
		TreeNode two = new TreeNode(2);
		TreeNode three = new TreeNode(3);
		one.setLeft(two);
		one.setRight(three);
		TreeNode four = new TreeNode(4);
		TreeNode five = new TreeNode(5);
		two.setLeft(four);
		two.setRight(five);
		TreeNode six = new TreeNode(6);
		four.setLeft(six);
		Solution s = new Solution();
		System.out.println(s.isBalanced(one));
	}
}
