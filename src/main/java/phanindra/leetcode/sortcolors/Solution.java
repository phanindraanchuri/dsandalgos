package phanindra.leetcode.sortcolors;

import java.util.Arrays;

/**
 * http://www.csse.monash.edu.au/~lloyd/tildeAlgDS/Sort/Flag/
 *
 * Invariant to maintain for two colors
 * nums[0..lo-1] zeroes
 * nums[lo...hi] - unknown
 * nums[hi+1..nums.length-1] - ones
 *
 * Shrink unknown
 *
 * A rather straight forward solution is a two-pass algorithm using counting sort.
 * First, iterate the array counting number of 0's, 1's, and 2's, then overwrite array
 * with total number of 0's, then 1's and followed by 2's.
 * Could you come up with an one-pass algorithm using only constant space?
 */
public class Solution {
    public static void sortTwoColors(int[] nums) {
        int low = 0;
        int high = nums.length-1;
        while(low <= high) {
            if(nums[low] == 0) {
                low++;
            } else {
                swap(nums, low, high);
                high--;
            }
        }
    }

    private static void swap(int[] nums, int low, int high) {
        int temp = nums[low];
        nums[low] = nums[high];
        nums[high] = temp;
    }

    public static void sortThreeColors(int[] nums) {
        int low = 0;
        int mid = 0;
        int high = nums.length - 1;
        while(mid <= high) {
            if(nums[mid] == 1) {
                mid++;
            } else if(nums[mid] == 0) {
                swap(nums, low, mid);
                low++;
                mid++;
            } else {
                swap(nums, mid, high);
                high--;
            }
        }

    }

    public static void main(String[] args) {
        int[] nums = {0, 1, 0, 1, 1, 1, 1, 1, 0, 0};
        Solution.sortTwoColors(nums);
        System.out.println(Arrays.toString(nums));
        int[] nums2 = { 0, 0, 2, 1, 2, 0, 2, 1, 2, 1};
        Solution.sortThreeColors(nums2);
        System.out.println(Arrays.toString(nums2));
    }

}
