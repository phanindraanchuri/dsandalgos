package phanindra.leetcode.numberpalindrome;

public class Solution {
	public static boolean isPalindrome(int x) {
		return x == reverse(x);
	}

	private static int reverse(int x) {
		int result = 0;
		while (x > 0) {
			if (result > Integer.MAX_VALUE / 10) {
				return 0;
			}
			result *= 10;
			result = (result + x % 10);
			x = x / 10;
		}
		return result;
	}

	public static void main(String[] args) {
		int x = Integer.MAX_VALUE - 1;
		System.out.println(x);
		System.out.println(Solution.reverse(x));
	}
}
