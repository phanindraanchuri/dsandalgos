package phanindra.leetcode.powxn;

public class Solution {
    public double myPow(double x, int n) {
        if(n == 0 || x == 1) {
            return 1;
        }

        // This check needs to occur before the next one because if we don't then
        // doing -n will cause an Integer overflow.
        if(n == Integer.MIN_VALUE) {
            return (double)1/(x*myPow(x,Integer.MAX_VALUE));
        }

        if(n < 0) {
            return 1/myPow(x, -n);
        }

        // n == 1, is taken care of by the else condition here.
        double half = myPow(x, n/2);
        if(n% 2 == 0) {
            return half * half;
        } else {
            return half*half*x;
        }
    }
}