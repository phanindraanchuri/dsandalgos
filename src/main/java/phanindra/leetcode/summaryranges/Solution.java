package phanindra.leetcode.summaryranges;

import java.util.List;
import java.util.ArrayList;

public class Solution {

  public List<String> summaryRanges(int[] nums) {
    List<String> ranges = new ArrayList<>();
    if (nums.length == 0) {
      return ranges;
    }
    int i = 0;
    while (i < nums.length) {
      String range = "";
      int rangeStart = i;
      range += nums[i];
      while (i+1 < nums.length && nums[i + 1] - nums[i] == 1) {
        i++;
      }
      if (i != rangeStart) {
        range += "->" + nums[i];
      }
      i++;
      ranges.add(range);
    }
    return ranges;
  }

  public static void main(String[] args) {
    Solution s = new Solution();
    System.out.println(s.summaryRanges(new int[] { 0, 1 }));
  }
}
