package phanindra.leetcode.istreebst;

public class TreeNode {
  int val;

  TreeNode left;

  TreeNode right;

  public TreeNode(int x) {
    val = x;
  }

  boolean isLeaf() {
    return left == null && right == null;
  }

  @Override
  public String toString() {
    return "[" + val + "]";
  }
}
