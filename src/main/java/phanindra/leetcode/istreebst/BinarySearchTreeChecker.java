package phanindra.leetcode.istreebst;

import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class BinarySearchTreeChecker {

  public static boolean isBst(TreeNode root) {
    List<Integer> values = new ArrayList<>();
    Stack<TreeNode> stack = new Stack<TreeNode>();
    TreeNode current = root;
    boolean done = false;
    while (!done) {
      if (current != null) {
        stack.push(current);
        current = current.left;
      }
      else {
        if (stack.isEmpty()) {
          done = true;
        }
        else {
          current = stack.peek();
          values.add(stack.pop().val);
          current = current.right;
        }
      }
    }
    System.out.println(values);
    for(int i = 1; i < values.size(); i++) {
      if(values.get(i) < values.get(i-1)) {
        return false;
      }
    }
    return true;
  }

  public static void main(String[] args) {
    TreeNode root = new TreeNode(4);
    root.left = new TreeNode(2);
    root.right = new TreeNode(7);
    root.left.left = new TreeNode(1);
    root.left.right = new TreeNode(8);
    root.right.left = new TreeNode(6);
    root.right.right = new TreeNode(9);
    System.out.println(BinarySearchTreeChecker.isBst(root));
  }
}
