package phanindra.leetcode.mergetwolinkedlists;

public class Solution {

  public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
    if (l1 == null) {
      return l2;
    }
    if (l2 == null) {
      return l1;
    }
    ListNode result = null;
    ListNode head = null;
    ListNode c1 = l1;
    ListNode c2 = l2;
    while (c1 != null && c2 != null) {
      int v1 = c1.val;
      int v2 = c2.val;
      if (v1 < v2) {
        if (result == null) {
          result = new ListNode(v1);
          head = result;
        }
        else {
          result.next = new ListNode(v1);
          result = result.next;
        }
        c1 = c1.next;
      }
      else {
        if (result == null) {
          result = new ListNode(v2);
          head = result;
        }
        else {
          result.next = new ListNode(v2);
          result = result.next;
        }
        c2 = c2.next;
      }
    }
    if (c1 == null) {
      while (c2 != null) {
        result.next = new ListNode(c2.val);
        result = result.next;
        c2 = c2.next;
      }
    }
    if (c2 == null) {
      while (c1 != null) {
        result.next = new ListNode(c1.val);
        result = result.next;
        c1 = c1.next;
      }
    }
    return head;
  }

  class ListNode {
    int val;

    ListNode next;

    ListNode(int x) {
      val = x;
    }
  }
}
