package phanindra.leetcode.nextpermutation;

import java.util.Arrays;

/*
 * // Algorithm // Step 1: Find the longest tail that is ordered in the decreasing order // Step 2: Swap the number just
 * before the tail with the smallest number bigger than it in the tail // Step 3: Sort the tail in increasing order i.e
 * reverse the tail
 */
public class Solution {
  public void nextPermutation(int[] nums) {
    int swapIndex = findTailIndex(nums);
    if (swapIndex == -1) {
      reverse(nums, 0, nums.length - 1);
    }
    else {
      nums = swap(nums, swapIndex);
      reverse(nums, swapIndex + 1, nums.length - 1);
    }
  }

  private int findTailIndex(int[] nums) {
    int currIndex = nums.length - 1;
    int prevIndex = currIndex - 1;
    while (prevIndex >= 0) {
      if (nums[currIndex] <= nums[prevIndex]) {
        currIndex = prevIndex;
        prevIndex = prevIndex - 1;
      }
      else {
        break;
      }
    }
    return prevIndex;
  }

  private int[] swap(int[] nums, int swapIndex) {
    int swapWith = swapIndex;
    for (int s = swapIndex + 1; s < nums.length; s++) {
      if (nums[s] > nums[swapIndex]) {
        swapWith = s;
      }
    }
    int temp = nums[swapIndex];
    nums[swapIndex] = nums[swapWith];
    nums[swapWith] = temp;
    return nums;
  }

  private void reverse(int[] arr, int s, int e) {
    int i = s;
    int j = e;
    while (i < j) {
      int temp = arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
      i++;
      j--;
    }
  }

  public static void main(String[] args) {
    int[] nums = new int[] { 5, 1, 7, 6, 3, 9, 8, 4, 2 };
    // int[] nums = new int[] { 5, 1, 1 };
    // int[] nums = new int[] { 3, 2, 1 };
    new Solution().nextPermutation(nums);
    System.out.println(Arrays.toString(nums));
    // [5, 1, 7, 6, 4, 9, 8, 3, 2]
    // [5, 1, 7, 6, 4, 2, 3, 8, 9]
  }
}
