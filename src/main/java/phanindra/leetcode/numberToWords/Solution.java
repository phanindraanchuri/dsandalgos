package phanindra.leetcode.numberToWords;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;

public class Solution {

	static String[] unitWords = new String[] { "Billion", "Million",
			"Thousand", "" };
	
	private static final Map<Character, String> TENS_MAP = createTensMap();

	private static Map<Character, String> createTensMap() {
		Map<Character, String> m = new HashMap<>();
		m.put('2', "Twenty");
		m.put('3', "Thirty");
		return Collections.unmodifiableMap(m);
	}

	private static final Map<Character, String> UNITS_MAP = createUnitsMap();

	private static Map<Character, String> createUnitsMap() {
		Map<Character, String> m = new HashMap<>();
		m.put('1', "One");
		m.put('2', "Two");
		m.put('3', "Three");
		m.put('4', "Four");
		m.put('5', "Five");
		m.put('6', "Six");
		m.put('7', "Seven");
		m.put('8', "Eight");
		m.put('9', "Nine");
		return Collections.unmodifiableMap(m);
	}

	private static final Map<String, String> TWO_DIGIT_MAP = createTwoDigitMap();

	private static Map<String, String> createTwoDigitMap() {
		Map<String, String> m = new HashMap<>();
		m.put("11", "Eleven");
		m.put("12", "Twelve");
		m.put("13", "Thirteen");
		m.put("14", "Fourteen");
		m.put("15", "Fifteen");
		m.put("16", "Sixteen");
		m.put("17", "Seventeen");
		m.put("18", "Eighteen");
		m.put("19", "Nineteen");
		return Collections.unmodifiableMap(m);
	}
	
	static final int CHUNK_LENGTH = 3;

	public String numberToWords(int num) {
		StringBuffer result = new StringBuffer();
		String numString = Integer.toString(num);
		String[] chunks = chunks(numString);
		// System.out.println(Arrays.toString(chunks));
		int unitWordIndex = unitWords.length - chunks.length;
		int chunkIndex = 0;
		while (unitWordIndex < unitWords.length) {
			result.append(chunks[chunkIndex]).append(" ")
					.append(unitWords[unitWordIndex]).append(" ");
			unitWordIndex++;
			chunkIndex++;
		}
		for (String chunk : chunks) {
			System.out.println(chunk);
			System.out.println(chunkToWord(chunk));
		}
		return result.toString();
	}

	private String chunkToWord(String chunk) {
		StringBuffer result = new StringBuffer();
		boolean[] exists = new boolean[]{false, false, false};
		for(int i = chunk.length()-1; i>=0; i--) {
			exists[i] = true;
		}
		for(int i = 0; i < chunk.length(); i++) {
			if(exists[i] && chunk.charAt(i) != '0') {
			}
		}
		return null;
	}

	public String[] chunks(String numString) {
		int inputLength = numString.length();
		int add = inputLength % CHUNK_LENGTH > 0 ? 1 : 0;
		String[] chunks = new String[inputLength / CHUNK_LENGTH + add];
		int endIndex = inputLength;
		int startIndex = endIndex - CHUNK_LENGTH;
		int chunkIndex = chunks.length - 1;
		while (chunkIndex > 0 && startIndex > 0) {
			chunks[chunkIndex] = numString.substring(startIndex, endIndex);
			chunkIndex--;
			endIndex = endIndex - CHUNK_LENGTH;
			startIndex = startIndex - CHUNK_LENGTH;
		}
		chunks[0] = numString.substring(0, endIndex);
		return chunks;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.numberToWords(10));
	}
}
