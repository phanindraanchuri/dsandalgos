package phanindra.leetcode.binarytreeinordertraversal;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by phanindra on 9/21/16.
 */
public class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if(root == null) {
            return result;
        }
        Stack<TreeNode> s = new Stack<>();
        while(true) {
            if(root != null) {
                s.push(root);
                root = root.getLeft();
            } else {
                if(s.isEmpty()) {
                    break;
                }
                root = s.pop();
                result.add(root.getVal());
                root = root.getRight();
            }
        }
        return result;
    }
}