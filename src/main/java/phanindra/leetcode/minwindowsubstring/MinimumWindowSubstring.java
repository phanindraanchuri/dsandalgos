package phanindra.leetcode.minwindowsubstring;

import java.util.Map;
import java.util.HashMap;

public class MinimumWindowSubstring {
  public static String minWindow(String s, String t) {
    int sLength = s.length();
    int tLength = t.length();
    Map<Character, Integer> tMap = tMap(t);

    Map<Character, Integer> sMap = new HashMap<>();
    int minWindowLen = Integer.MAX_VALUE;
    int count = 0;

    for (int begin = 0, end = 0; end < sLength; end++) {
      char endChar = s.charAt(end);
      if (tMap.containsKey(endChar)) {
        if (sMap.containsKey(endChar)) {
          sMap.put(endChar, sMap.get(endChar + 1));
        }
        else {
          sMap.put(endChar, 1);
        }
        if (sMap.get(endChar) <= tMap.get(endChar)) {
          count++;
        }
      }

      if (count == tLength) {
        char beginChar = s.charAt(begin);
        while (sMap.get(beginChar) == 0 || sMap.get(beginChar) > tMap.get(beginChar)) {
          if (sMap.get(beginChar) > tMap.get(beginChar))
            sMap.put(beginChar, sMap.get(beginChar) - 1);
          begin++;
        }

        // update minWindow if a minimum length is met
        int windowLen = end - begin + 1;
        if (windowLen < minWindowLen) {
          minWindowLen = windowLen;
        } // end if
      }
    }
    return "";
  }

  private static Map<Character, Integer> tMap(String t) {
    Map<Character, Integer> tMap = new HashMap<>();
    for (char tc : t.toCharArray()) {
      if (tMap.containsKey(tc)) {
        tMap.put(tc, tMap.get(tc) + 1);
      }
      else {
        tMap.put(tc, 1);
      }
    }
    return tMap;
  }

  public static void main(String[] args) {
    MinimumWindowSubstring.minWindow("adobecodebanc", "abc");
  }
}
