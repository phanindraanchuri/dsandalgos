package phanindra.leetcode.integerbreak;

/**
 * Given a positive integer n,
 * break it into the sum of at least two positive integers and maximize the product of those integers.
 * Return the maximum product you can get.
 * Note: You may assume that n is not less than 2 and not larger than 58.
 */
/**
 * Explanation : The optimal product cannot contain a factor greater than or equal to 4
 * because if it did, we can break the factor f >= 4 into 2 and f-2 which contributes a product
 * of 2*(f-2) = 2f -4  which is greater than f for all f > 4.
 * So we never need a factor greater than or equal to 4.
 *
 * Of the remaining factors, using 1 is wasteful.
 *
 * Of 2 and 3, 3*3 is better than 2*2*2, so never use more than 2 two's
 *
 */
public class IntegerBreak {
    public static int integerBreak(int n) {
        if(n == 2) {
            return 1;
        }
        if(n == 3) {
            return 2;
        }
        int maxProduct = 1;
        while(n > 4) {
            maxProduct *= 3; //Make as many 3s as possible until you get to 4.
            n -= 3;
        }
        maxProduct *= n; // Multiply left over
        return maxProduct;
    }

    public static void main(String[] args) {
        for(int i = 2; i < 20; i++) {
            System.out.println("n is "+ i +" Max product is " +IntegerBreak.integerBreak(i));
        }
    }
}
