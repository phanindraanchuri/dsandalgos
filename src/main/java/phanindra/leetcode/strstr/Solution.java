package phanindra.leetcode.strstr;

public class Solution {
	public static int strStr(String haystack, String needle) {
		if (haystack.equals(needle)) {
			return 0;
		}
		if (!haystack.isEmpty() && needle.isEmpty()) {
			return 0;
		}
		if (haystack.isEmpty() || needle.isEmpty()
				|| needle.length() > haystack.length()) {
			return -1;
		}
		for (int hssIndex = 0; hssIndex <= haystack.length() - needle.length(); hssIndex++) {
			if (haystack.substring(hssIndex, (hssIndex + needle.length()))
					.equals(needle)) {
				return hssIndex;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		System.out.println(Solution.strStr("gogodworldgod", "god"));
	}
}
