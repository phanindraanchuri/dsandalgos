package phanindra.leetcode.largestnumber;

import java.util.*;

/**
 * Given a list of non negative integers, arrange them such that they form the largest number.

 For example, given [3, 30, 34, 5, 9], the largest formed number is 9534330.

 Note: The result may be very large, so you need to return a string instead of an integer.

 Idea : Given two numbers, we compare which ordering gives us the higher value, use this to sort
 Eg: [3, 30, 34, 5, 9]
 3 vs 30 -> XY is 330, YX is 303, 3 > 30
 3 vs 34 -> XY is 334, YX is 343 34 > 3
 and so on
 */
public class LargestNumber {
    public String largestNumber(int[] nums) {
        List<Integer> numList = new ArrayList<>();
        for(int num: nums) {
            numList.add(num);
        }
        Collections.sort(numList, new LexComparator());
        StringBuffer resultBuffer = new StringBuffer();
        for(int i = numList.size() - 1; i >= 0; i--) {
            resultBuffer.append(numList.get(i));
        }
        String result = resultBuffer.toString();
        if(result.charAt(0) == '0') {
            return "0";
        }
        return resultBuffer.toString();
    }

    class LexComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            String s1 = o1.toString();
            String s2 = o2.toString();
            String a = s1+s2;
            String b = s2+s1;
            return a.compareTo(b);
        }
    }

    public static void main(String[] args) {
        LargestNumber ln = new LargestNumber();
        String result = ln.largestNumber(new int[]{3, 30, 34, 5, 9});
        System.out.println(result);
    }
}
