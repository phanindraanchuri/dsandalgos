package phanindra.leetcode.longestpalindrome;

public class Solution {
	public String longestPalindrome(String s) {
		if (s == null || s.length() <= 1)
			return s;
		int len = s.length();
		boolean[][] dp = new boolean[len][len];
		char[] w = s.toCharArray();
		for (int i = 0; i < len; i++) {
			dp[i][i] = true;
		}

		/**
		 * substrings of length k
		 */
		for (int k = 2; k <= len; ++k) {
			/* i is the start index */
			for (int i = 0; i < len - k + 1; ++i) {
				/* j is endIndex, starting at i of length k */
				int j = i + k - 1;
				if (dp[i + 1][j - 1] && w[i] == w[j]) {
					dp[i][j] = true;
				} else {
					dp[i][j] = false;
				}
			}
		}
		return "";
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.longestPalindrome("abcdcbg"));
	}
}
