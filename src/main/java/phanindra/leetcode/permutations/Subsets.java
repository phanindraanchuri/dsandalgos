package phanindra.leetcode.permutations;

import java.util.ArrayList;
import java.util.List;

public class Subsets {
	public static List<List<Integer>> subsets(int[] nums) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (nums.length == 0) {
			return result;
		}
		subsetsRecursive(nums, 0, new ArrayList<Integer>(), result);
		return result;
	}

	public static void subsetsRecursive(int[] s, int index,
			List<Integer> subset, List<List<Integer>> result) {
		result.add(new ArrayList<Integer>(subset));

		for (int i = index; i < s.length; i++) {
			subset.add(s[i]);
			subsetsRecursive(s, i + 1, subset, result);
			subset.remove(subset.size() - 1);
		}
	}

	public static void main(String[] args) {
		System.out.println(Subsets.subsets(new int[] { 1, 2, 3 }));
	}
}
