package phanindra.leetcode.permutations;

import java.util.ArrayList;
import java.util.List;

public class StringPermutations {
	public static List<String> permutations(String s) {
		List<String> result = new ArrayList<String>();
		permuteRecursive("", s, result, 0);
		return result;
	}

	public static void permuteRecursive(String stringSoFar, String remaining,
			List<String> result, int level) {
		String s = "";
		for (int l = 0; l < level; l++) {
			s += "--";
		}
		System.out.println(" " + s + "String so far " + stringSoFar
				+ " String remaining " + remaining);
		int remainingLength = remaining.length();
		if (remainingLength == 0) {
			result.add(stringSoFar);
		} else {
			for (int i = 0; i < remainingLength; i++) {
				String nextSoFar = stringSoFar + remaining.charAt(i);
				String nextRemaining = remaining.substring(0, i)
						+ remaining.substring(i + 1, remainingLength);
				permuteRecursive(nextSoFar, nextRemaining, result, level + 1);
			}
		}
	}

	public static void main(String[] args) {
		System.out.println(StringPermutations.permutations("abc"));
	}
}
