package phanindra.leetcode.permutations;

import java.util.List;
import java.util.ArrayList;

// writing for practice
public class StringPermutations2 {
  private List<String> permute(String s) {
    return permute("", s, new ArrayList<>());
  }

  private List<String> permute(String prefix, String rest, List<String> result) {
    int len = rest.length();
    if (len == 0) {
      result.add(prefix);
    }
    else {
      for (int i = 0; i < len; i++) {
        // We do not want to return here, returning here means we do not run the loop for values of i other than 0
        permute(prefix + rest.charAt(i), rest.substring(0, i) + rest.substring(i + 1, len), result);
      }
    }
    return result;
  }

  public static void main(String[] args) {
    StringPermutations2 s = new StringPermutations2();
    List<String> result = s.permute("abc");
    System.out.println(result);
  }
}
