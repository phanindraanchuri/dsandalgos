package phanindra.leetcode.permutations;

import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<List<Integer>> permute(int[] nums) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (nums.length == 0) {
			return result;
		}
		permuteRecursive(new ArrayList<Integer>(), result, nums);
		return result;
	}

	public void permuteRecursive(List<Integer> current,
			List<List<Integer>> result, int[] nums) {
		if (current.size() == nums.length) {
			result.add(new ArrayList<Integer>(current));
		}

		for (int i = 0; i < nums.length; i++) {
			if (!current.contains(nums[i])) {
				current.add(nums[i]);
				permuteRecursive(current, result, nums);
				current.remove(current.size() - 1);
			}
		}
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		s.permute(new int[]{1,2,3});
	}
}
