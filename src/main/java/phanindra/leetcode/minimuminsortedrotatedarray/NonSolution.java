package phanindra.leetcode.minimuminsortedrotatedarray;

public class NonSolution {
  public static int findMin(int[] nums) {
    if (nums == null || nums.length == 0) {
      return -1;
    }
    if (nums.length == 1) {
      return nums[0];
    }
    if (nums.length == 2) {
      return Math.min(nums[0], nums[1]);
    }
    int low = 0;
    int high = nums.length - 1;
    while (low < high) {
      int mid = low + (high - low) / 2;
      // Is mid the minimum element ?,
      // minimum element is one that has elements greater than it on both sides
      if (mid > 0 && nums[mid - 1] > nums[mid] && nums[mid + 1] > nums[mid]) {
        return nums[mid];
      }
      // Check to see which half is sorted, the min is in the other half
      // Is the left half sorted ?
      if (nums[low] < nums[mid]) {
        low = mid + 1;
      }
      else {
        high = mid - 1;
      }
    }
    return nums[0];
  }

  public static void main(String[] args) {
    System.out.println(NonSolution.findMin(new int[] { 2, 1 }));
  }
}
