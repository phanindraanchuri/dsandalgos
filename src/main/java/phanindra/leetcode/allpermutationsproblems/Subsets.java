package phanindra.leetcode.allpermutationsproblems;

import java.util.ArrayList;
import java.util.List;

public class Subsets {
  public List<List<Integer>> subsets(int[] nums) {
    List<List<Integer>> allSubsets = new ArrayList<List<Integer>>();
    subsetsRecursive(nums, 0, new ArrayList<Integer>(), allSubsets);
    return allSubsets;
  }

  void subsetsRecursive(int[] nums, int numIndex, List<Integer> currentSubset, List<List<Integer>> allSubsets) {
    allSubsets.add(new ArrayList<Integer>(currentSubset));

    for (int i = numIndex; i < nums.length; i++) {
      currentSubset.add(nums[i]);
      subsetsRecursive(nums, i + 1, currentSubset, allSubsets);
      currentSubset.remove(currentSubset.size() - 1);
    }
  }

  public static void main(String[] args) {
    Subsets s = new Subsets();
    System.out.println(s.subsets(new int[] { 1, 2, 3 }));
  }
}
