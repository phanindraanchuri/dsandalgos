package phanindra.leetcode.allpermutationsproblems;

import java.util.List;
import java.util.ArrayList;

public class Permutations {
  public List<List<Integer>> permute(int[] nums) {
    List<List<Integer>> allPermutations = new ArrayList<List<Integer>>();
    permuteRecursive(nums, 0, new ArrayList<>(), allPermutations);
    return allPermutations;
  }

  private void permuteRecursive(int[] nums, int numIndex, List<Integer> currentPermutation,
    List<List<Integer>> allPermutations) {
    // If the current permutation has all the elements in the input, it is ready to be added to allPermutations
    if (currentPermutation.size() == nums.length) {
      allPermutations.add(currentPermutation);
      return;
    }
    // Otherwise, for put the number at numIndex in nums in all possible positions of currentPermutation
    // All possible positions in the current permutation depends on the length of the current permutation
    // If the current permutation is of length 2, for instance [2,3], possible positions for the element from num
    // are 0, 1, 2 i.e, 0 through length of current permutation included
    for (int position = 0; position <= currentPermutation.size(); position++) {
      List<Integer> nextPermutation = new ArrayList<>(currentPermutation);
      nextPermutation.add(position, nums[numIndex]);
      permuteRecursive(nums, numIndex + 1, nextPermutation, allPermutations);
    }
  }

  public static void main(String[] args) {
    Permutations p = new Permutations();
    System.out.println(p.permute(new int[] { 1, 2, 3 }));
  }
}
