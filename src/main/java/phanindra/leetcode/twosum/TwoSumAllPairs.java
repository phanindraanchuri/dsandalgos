package phanindra.leetcode.twosum;

import java.util.*;

public class TwoSumAllPairs {
    public List<List<Integer>> twosum(int[] nums, int target) {
        List<List<Integer>> pairs = new ArrayList<>();
        Set<Integer> m = new HashSet<>();
        for(int num : nums) {
            if(m.contains(target - num)) {
                List<Integer> pair = new ArrayList<>();
                pair.add(num);
                pair.add(target - num);
                pairs.add(pair);
            }
            m.add(num);
        }
        return pairs;
    }

    public static void main(String[] args) {
        TwoSumAllPairs tap = new TwoSumAllPairs();
        System.out.println((tap.twosum(new int[] {2, 5, 7, 4}, 9)));
    }
}
