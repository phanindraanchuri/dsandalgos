package phanindra.leetcode.twosum;

import java.util.HashMap;
import java.util.Map;

public class Solution {
  public int[] twoSumMap(int[] nums, int target) {
    int[] result = new int[2];
    Map<Integer, Integer> map = new HashMap<Integer, Integer>();
    for (int i = 0; i < nums.length; i++) {
      // This check is important, consider [0, 4, 3, 0] and target = 0,
      // if a solution is found, we do not need to put it in the map
      if (map.containsKey(target - nums[i])) {
        result[1] = i;
        result[0] = map.get(target - nums[i]);
        return result;
      }
      map.put(nums[i], i);
    }
    return result;
  }

  public int[] twoSumBruteForce(int[] nums, int target) {
    int[] result = new int[] { -1, -1 };
    if (nums.length == 0 || nums.length == 1) {
      return result;
    }
    for (int i = 0; i < nums.length; i++) {
      for (int j = 0; j < nums.length; j++) {
        if (j != i) {
          if (nums[i] + nums[j] == target) {
            result[0] = i;
            result[1] = j;
            return result;
          }
        }
      }
    }
    return result;
  }
}
