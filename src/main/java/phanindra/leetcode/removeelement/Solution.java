package phanindra.leetcode.removeelement;

/**
 *
 * Given an array and a value, remove all instances of that value in place and return the new length.

 Do not allocate extra space for another array, you must do this in place with constant memory.

 The order of elements can be changed. It doesn't matter what you leave beyond the new length.

 Example:
 Given input array nums = [3,2,2,3], val = 3

 Your function should return length = 2, with the first two elements of nums being 2.
 * Like Dutch National flag problem.
 *
 * Invariant is (0, low-1) are non val's, low to high are unknown, high+1 to len-1 are val's
 *
 */
public class Solution {
    public int removeElement(int[] nums, int val) {
        int low = 0;
        int high = nums.length - 1;
        while(low <= high) {
            if(nums[low] != val) {
                low++;
            } else {
                int temp = nums[low];
                nums[low] = nums[high];
                nums[high] = temp;
                high--;
            }
        }
        return low;
    }
}
