package phanindra.leetcode.createmaxnumber;

import java.util.Arrays;
import java.util.PriorityQueue;

public class NonSolutionUsingHeaps {

	public int[] maxNumber(int[] nums1, int[] nums2, int k) {
		int[] result = new int[k];
		int resultIndex = 0;
		PriorityQueue<Triple> pq1 = new PriorityQueue<>();
		PriorityQueue<Triple> pq2 = new PriorityQueue<>();
		for (int i1 = 0; i1 < nums1.length; i1++) {
			pq1.add(new Triple(nums1[i1], i1, 1));
		}
		for (int i2 = 0; i2 < nums2.length; i2++) {
			pq2.add(new Triple(nums2[i2], i2, 2));
		}
		int mostRecentFirstIndex = -1;
		int mostRecentSecondIndex = -1;
		while (k > 0 && !pq1.isEmpty() && !pq2.isEmpty()) {
			Triple first = pq1.peek();
			Triple second = pq2.peek();
			if (first.compareTo(second) < 0
					&& first.index > mostRecentFirstIndex) {
				mostRecentFirstIndex = first.index;
				result[resultIndex] = first.num;
				resultIndex++;
				pq1.remove();
				k--;
			} else if (second.compareTo(first) <= 0
					&& second.index > mostRecentSecondIndex) {
				mostRecentSecondIndex = second.index;
				result[resultIndex] = second.num;
				resultIndex++;
				pq2.remove();
				k--;
			} else if (first.compareTo(second) < 0) {
				pq1.remove();
			} else if (second.compareTo(first) <= 0) {
				pq2.remove();
			}
		}

		if (pq1.isEmpty()) {
			for (int l = k; l > 0; l--) {
				result[resultIndex] = pq2.remove().num;
				resultIndex++;
			}
		}

		if (pq2.isEmpty()) {
			for (int l = k; l > 0; l--) {
				result[resultIndex] = pq1.remove().num;
				resultIndex++;
			}
		}
		return result;
	}

	class Triple implements Comparable<Triple> {
		int num;
		int index;
		int origin;

		public Triple(int num, int index, int origin) {
			this.num = num;
			this.index = index;
			this.origin = origin;
		}

		@Override
		public int compareTo(Triple o) {
			return Integer.valueOf(o.num).compareTo(num);
		}

		@Override
		public String toString() {
			return "[" + num + "" + index + "" + origin + "]";
		}
	}

	public static void main(String[] args) {
		NonSolutionUsingHeaps s = new NonSolutionUsingHeaps();
		int[] nums1 = new int[] { 9, 1, 2, 5, 8, 3 };
		int[] nums2 = new int[] { 3, 4, 6, 5 };
		System.out.println(Arrays.toString(s.maxNumber(nums1, nums2, 5)));
	}
}
