package phanindra.leetcode.reversewords;

public class Solution {
	public String reverseWords(String s) {
		s = s.replaceAll("\\s+", " ").trim();
		char[] reversedString = reverseCharArray(s.toCharArray(), 0,
				s.length() - 1);
		int i = 0;
		int j = 0;
		while (j < reversedString.length) {
			while (j < reversedString.length && reversedString[j] != ' ') {
				j++;
			}
			reverseCharArray(reversedString, i, j - 1);
			i = j + 1;
			j = j + 1;
		}
		return new String(reversedString);
	}

	char[] reverseCharArray(char[] sa, int start, int end) {
		int i = start;
		int j = end;
		while (i < j) {
			char temp = sa[i];
			sa[i] = sa[j];
			sa[j] = temp;
			i++;
			j--;
		}
		return sa;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.reverseWords("  the  sky is blue").length());
	}
}
