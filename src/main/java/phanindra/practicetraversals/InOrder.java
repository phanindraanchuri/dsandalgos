package phanindra.practicetraversals;

import java.util.Stack;

public class InOrder {
  void inorder(TreeNode root) {
    if (root == null) {
      return;
    }
    Stack<TreeNode> stack = new Stack<>();
    TreeNode current = root;
    boolean done = false;
    while (!done) {
      if (current != null) {
        stack.push(current);
        current = current.left;
      }
      else {
        if (stack.isEmpty()) {
          done = true;
        }
        else {
          current = stack.pop();
          System.out.println(current.data);
          current = current.right;
        }
      }
    }
  }

  public static void main(String[] args) {
    InOrder inorder = new InOrder();
    TreeNode root = new TreeNode();
    root.data = 10;
    TreeNode l1 = new TreeNode();
    l1.data = 0;
    TreeNode l2 = new TreeNode();
    l2.data = -10;
    TreeNode l3 = new TreeNode();
    l3.data = 5;
    TreeNode l4 = new TreeNode();
    l4.data = 6;
    TreeNode l5 = new TreeNode();
    l5.data = 11;
    root.left = l1;
    root.right = l2;
    l1.left = l3;
    l1.right = l4;
    l2.right = l5;
    inorder.inorder(root);
  }
}
