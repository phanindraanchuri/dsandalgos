package phanindra.dsandalgos.peakfinder;

public class PeakFinder {
	public static int findAPeakIndex(int[] nums) {
		if (nums.length == 0) {
			return -1;
		}
		if (nums.length == 1) {
			return 0;
		}
		if (nums.length == 2) {
			if (nums[0] >= nums[1]) {
				return 0;
			} else {
				return 1;
			}
		}

		int result = 0;
		int[] numsPadded = new int[nums.length + 2];
		System.arraycopy(nums, 0, numsPadded, 1, nums.length);
		numsPadded[0] = Integer.MIN_VALUE;
		numsPadded[numsPadded.length - 1] = Integer.MIN_VALUE;

		for (int index = 1; index <= numsPadded.length - 2; index++) {
			if (numsPadded[index] >= numsPadded[index - 1]
					&& numsPadded[index] >= numsPadded[index + 1]) {
				result = index - 1;
				break;
			}
		}
		return result;
	}

	public static int findPeakIndexDivideAndConquer(int[] nums) {
		return findPeakIndexDivideAndConquerHelper(nums, 0, nums.length - 1);
	}

	public static int findPeakIndexDivideAndConquerHelper(int[] nums, int low,
			int high) {
		if (high - low <= 1) {
			if (nums[high] >= nums[low]) {
				return high;
			} else {
				return low;
			}
		}
		int mid = low + (high - low) / 2;
		if (nums[mid - 1] <= nums[mid] && nums[mid] >= nums[mid + 1]) {
			return mid;
		} else if (nums[mid - 1] >= nums[mid]) {
			return findPeakIndexDivideAndConquerHelper(nums, low, mid - 1);
		} else if (nums[mid + 1] >= nums[mid]) {
			return findPeakIndexDivideAndConquerHelper(nums, mid + 1, high);
		}
		return -1;
	}
}
