package phanindra.dsandalgos.arrays;

public class Arrays {

	public static int deDup(int[] A) {
		if (A.length == 0 || A.length == 1) {
			return A.length;
		}
		int i = 0;
		int j = 1;
		do {
			if (A[j] == A[i]) {
				A[j] = Integer.MIN_VALUE;
				j++;
			} else {
				i = j;
				j = i + 1;
			}
		} while (j != A.length);
		int count = 0;
		for (int k = 0; k < A.length; k++) {
			if (Integer.MIN_VALUE != A[k]) {
				count++;
			}
		}

		int p1 = 0;
		int p2 = p1 + 1;

		while (true) {
			while (p1 != A.length && Integer.MIN_VALUE != A[p1]) {
				p1++;
			}

			while (p2 != A.length && Integer.MIN_VALUE == A[p2]) {
				p2++;
			}

			if (p1 == A.length || p2 == A.length) {
				break;
			}

			if (A[p1] == Integer.MIN_VALUE && A[p2] == Integer.MIN_VALUE) {
				break;
			}
			int temp = A[p1];
			A[p1] = A[p2];
			A[p2] = temp;
		}
		return count;
	}

}
