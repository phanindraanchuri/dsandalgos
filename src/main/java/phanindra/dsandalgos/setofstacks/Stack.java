package phanindra.dsandalgos.setofstacks;

import phanindra.dsandalgos.stackandqueue.LinkedList;

public class Stack {
	private int capacity;
	private int numberOfElements;

	private LinkedList linkedList;

	public Stack() {
		linkedList = new LinkedList();
		capacity=10;
	}

	public Stack(int capacity) {
		linkedList = new LinkedList();
		this.capacity = capacity;
	}

	public boolean isFull() {
		return numberOfElements == capacity;
	}

	public boolean isEmpty() {
		return numberOfElements == 0;
	}

	public boolean lastElement() {
		return numberOfElements == 1;
	}

	public void push(int item) {
		linkedList.insertAtHead(item);
		numberOfElements++;
	}

	public Integer pop() {
		numberOfElements--;
		return linkedList.deleteAtHead();
	}
}
