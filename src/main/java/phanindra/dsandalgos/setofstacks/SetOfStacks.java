package phanindra.dsandalgos.setofstacks;

import java.util.ArrayList;
import java.util.List;

public class SetOfStacks {

	private int perStackCapacity;

	private List<Stack> stacks;

	private Stack currentStack;

	public SetOfStacks() {
		stacks = new ArrayList<Stack>();
		perStackCapacity = 10;
	}

	public SetOfStacks(int capacity) {
		stacks = new ArrayList<Stack>();
		perStackCapacity = capacity;
	}

	public void push(int item) {
		if (stacks.isEmpty()) {
			Stack newStack = new Stack(perStackCapacity);
			currentStack = newStack;
			stacks.add(newStack);
		} else if (currentStack.isFull()) {
			Stack newStack = new Stack(perStackCapacity);
			currentStack = newStack;
			stacks.add(newStack);
		}
		currentStack.push(item);
	}

	public Integer pop() {
		if (stacks.isEmpty()) {
			return null;
		} else if (currentStack.lastElement() && stacks.size() != 1) {
			Integer item = currentStack.pop();
			int numberOfStacks = stacks.size();
			numberOfStacks--;
			currentStack = stacks.get(numberOfStacks - 1);
			return item;
		} else if (currentStack.lastElement() && stacks.size() == 1) {
			Integer item = currentStack.pop();
			currentStack = null;
			stacks = new ArrayList<Stack>();
			return item;
		} else {
			return currentStack.pop();
		}
	}

	public int size() {
		return stacks.size();
	}
}
