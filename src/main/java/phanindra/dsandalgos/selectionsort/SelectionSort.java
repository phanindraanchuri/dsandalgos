package phanindra.dsandalgos.selectionsort;

public class SelectionSort {
	static int[] sort(int[] a) {
		int i = 0;
		int j = 1;
		while (i < a.length) {
			int minIndex = i;
			while (j < a.length) {
				if (a[j] < a[minIndex]) {
					minIndex = j;
				}
				j = j + 1;
			}
			int temp = a[i];
			a[i] = a[minIndex];
			a[minIndex] = temp;
			i = i + 1;
			j = i + 1;
			if (j == a.length) {
				break;
			}
		}
		return a;
	}
	
	public static void main(String[] args) {
		SelectionSort.sort(new int[] { 7, 3, 27, 11, 21, 37, 4 });
	}
}
