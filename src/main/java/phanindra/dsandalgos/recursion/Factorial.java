package phanindra.dsandalgos.recursion;

public class Factorial {
	public static int factorial(int n) {
		if (n < 0) {
			throw new RuntimeException("Give me a positive integer");
		} else if (n <= 2) {
			return n;
		} else {
			return n * Factorial.factorial(n - 1);
		}
	}
}
