package phanindra.dsandalgos.recursion;

public class SquareRoot {

	public static final double DELTA = 0.01;

	public static double sqrtRecursive(double n) {
		return sqrtR(n, 1);
	}

	private static double sqrtR(double n, double guess) {
		if (Math.abs(guess - n / guess) < DELTA) {
			return guess;
		} else {
			guess = (guess + n / guess) / 2;
			return sqrtR(n, guess);
		}
	}
}
