package phanindra.dsandalgos.map;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.apache.commons.math3.primes.Primes;

public class ChainedHashMap<K,V> implements Map<K, V> {

	private UnOrderedArrayListMap<K, V>[] bucketArray;

	private int capacity;

	@SuppressWarnings("unchecked")
	public ChainedHashMap() {
		this.capacity = 101;
		bucketArray = new UnOrderedArrayListMap[101];
	}

	@SuppressWarnings("unchecked")
	public ChainedHashMap(int capacity) {
		this.capacity = capacity;
		bucketArray = new UnOrderedArrayListMap[capacity];
	}

	@Override
	public void put(K key, V value) {
		int index = hash(key);
		if(bucketArray[index] == null) {
			bucketArray[index] = new UnOrderedArrayListMap<>();
		}
		bucketArray[index].put(key, value);
	}

	@Override
	public V get(K key) {
		int index = hash(key);
		if(bucketArray[index] == null) {
			bucketArray[index] = new UnOrderedArrayListMap<>();
		}
		return bucketArray[index].get(key);
	}

	@Override
	public void remove(K key) {
		int index = hash(key);
		if(bucketArray[index] == null) {
			bucketArray[index] = new UnOrderedArrayListMap<>();
		}
		bucketArray[index].remove(key);
	}

	@Override
	public Set<K> keySet() {
		Set<K> keySet = new HashSet<>();
		for(UnOrderedArrayListMap<K, V> bucket: bucketArray) {
			if(bucket != null) {
				keySet.addAll(bucket.keySet());
			}
		}
		return keySet;
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		Set<Entry<K,V>> entrySet = new HashSet<>();
		for(UnOrderedArrayListMap<K, V> bucket: bucketArray) {
			if(bucket != null) {
				entrySet.addAll(bucket.entrySet());
			}
		}
		return entrySet;
	}

	@Override
	public int size() {
		return entrySet().size();
	}

	private int hash(K key) {
		int prime = Primes.nextPrime(capacity);
		Random r = new Random();
		int a = r.nextInt(prime);
		int b = r.nextInt(prime);
		int i = key.hashCode();
		int hash = ((a * i + b) % prime) % capacity;
		//TODO Replace with log4j loggers
		System.out.println(String.format("Hash : a is %d b is %d hash code is %d prime is %d hash is %d ", a, b, i, prime, hash));
		return hash;
	}
}
