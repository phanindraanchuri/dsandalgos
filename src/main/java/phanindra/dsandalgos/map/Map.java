package phanindra.dsandalgos.map;

import java.util.Set;

public interface Map<K,V> {
	void put(K key, V value);
	V get(K key);
	void remove(K key);
	Set<K> keySet();
	Set<Entry<K,V>> entrySet();
	int size();
}
