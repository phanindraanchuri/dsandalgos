package phanindra.dsandalgos.map;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UnOrderedArrayListMap<K, V> implements Map<K, V> {

	private List<Entry<K, V>> list;

	public UnOrderedArrayListMap() {
		list = new ArrayList<>();
	}

	@Override
	public void put(K key, V value) {
		list.add(new Entry<K, V>(key, value));
	}

	@Override
	public V get(K key) {
		int index = findKeyIndex(key);
		if (Integer.MIN_VALUE == index) {
			return null;
		}
		return list.get(index).getValue();
	}

	@Override
	public void remove(K key) {
		int index = findKeyIndex(key);
		if (Integer.MIN_VALUE == index) {
			return;
		}
		list.remove(index);
	}

	private int findKeyIndex(K key) {
		int returnIndex = Integer.MIN_VALUE;
		for (int index = 0; index < list.size(); index++) {
			if (key.equals(list.get(index).getKey())) {
				returnIndex = index;
				break;
			}
		}
		return returnIndex;
	}

	@Override
	public Set<K> keySet() {
		Set<K> keySet = new HashSet<>();
		for(int listIndex = 0; listIndex < list.size(); listIndex++) {
			keySet.add(list.get(listIndex).getKey());
		}
		return keySet;
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		Set<Entry<K,V>> entrySet = new HashSet<>();
		for(int listIndex = 0; listIndex < list.size(); listIndex++) {
			entrySet.add(list.get(listIndex));
		}
		return entrySet;
	}

	@Override
	public int size() {
		return list.size();
	}
}
