package phanindra.dsandalgos.removespecified;

import java.util.HashSet;
import java.util.Set;

public class Solution {

	static String removeSpecified(String input, String remove) {
		if (remove == null || remove.isEmpty()) {
			return input;
		}

		char[] removeChars = remove.toCharArray();
		Set<Character> removeSet = new HashSet<>();
		for (char removeChar : removeChars) {
			removeSet.add(removeChar);
		}

		char[] result = new char[input.length()];
		int resultIndex = 0;
		char[] inputArr = input.toCharArray();
		for (int inputIndex = 0; inputIndex < input.length(); inputIndex++) {
			if (!removeSet.contains(inputArr[inputIndex])) {
				result[resultIndex] = inputArr[inputIndex];
				resultIndex++;
			}
		}
		return String.valueOf(result);
	}

	static String removeSpecified2(String input, String remove) {
		if (remove == null || remove.isEmpty()) {
			return input;
		}

		char[] removeChars = remove.toCharArray();
		
		//Using an array for lookup instead of a hash map since the character set is small, ASCII 
		boolean[] removeFlags = new boolean[128]; // ASCII
		for (int i = 0; i < removeChars.length; i++) {
			removeFlags[removeChars[i]] = true;
		}
		char[] result = new char[input.length()];
		int resultIndex = 0;
		char[] inputArr = input.toCharArray();
		for (int inputIndex = 0; inputIndex < input.length(); inputIndex++) {
			int removeIndex = inputArr[inputIndex];
			if (!removeFlags[removeIndex]) {
				result[resultIndex] = inputArr[inputIndex];
				resultIndex++;
			}
		}
		return String.valueOf(result);
	}

	public static void main(String[] args) {
		String result = Solution.removeSpecified2(
				"Test input for removal of specified characters", "aeiou");
		System.out.println(result);
	}
}
