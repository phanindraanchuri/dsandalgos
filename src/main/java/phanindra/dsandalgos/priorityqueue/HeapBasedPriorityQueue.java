package phanindra.dsandalgos.priorityqueue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class HeapBasedPriorityQueue<K, V> implements PriorityQueue<K, V> {

	private List<Entry<K, V>> list;

	private Comparator<K> comparator;

	public HeapBasedPriorityQueue() {
		list = new ArrayList<Entry<K, V>>();
		comparator = new KeyComparator<K>();
	}

	@Override
	public void insert(K key, V value) {
		Entry<K, V> entry = new PriorityQueueEntry<K, V>(key, value);
		list.add(entry);
		upheap(list.size() - 1);
	}

	private void upheap(int j) {
		while (j > 0) {
			System.out.println("Upheap " + j);
			int p = parent(j);
			if (comparator.compare(list.get(j).getKey(), list.get(p).getKey()) >= 0) {
				break;
			}
			swap(j, p);
			j = p;
		}

	}

	private int parent(int j) {
		return (j - 1) / 2;
	}

	@Override
	public Entry<K, V> min() {
		return list.isEmpty() ? null : list.get(0);
	}

	@Override
	public Entry<K, V> removeMin() {
		// Get the min element located at root
		Entry<K, V> temp = min();
		// Swap the last element with the root element
		swap(0, list.size() - 1);
		// Remove the min element
		list.remove(list.size() - 1);
		// Restore heap property
		downheap(0);
		return temp;
	}

	private void downheap(int index) {
		while(hasLeft(index)) {
			int leftIndex = 2 * index + 1;
			int rightIndex = 2 * index + 2;

			int minIndex = Integer.MIN_VALUE;
			K leftElement = list.get(leftIndex).getKey();
			K rightElement = list.get(rightIndex).getKey();
			K currentElement = list.get(index).getKey();

			if (comparator.compare(currentElement, leftElement) < 0
					&& comparator.compare(currentElement, rightElement) < 0) {
				break;
			} else if(comparator.compare(leftElement, rightElement) < 0) {
				minIndex = leftIndex;
			} else {
				minIndex = rightIndex;
			}
			swap(index, minIndex);
			index = minIndex;
		}
	}

	private boolean hasLeft(int index) {
		return (2*index+1) < list.size();
	}

	private void swap(int i, int j) {
		Entry<K, V> temp = list.get(i);
		list.set(i, list.get(j));
		list.set(j, temp);
	}
}
