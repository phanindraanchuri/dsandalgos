package phanindra.dsandalgos.priorityqueue;

public class PriorityQueueEntry<K,V> implements Entry<K, V> {

	private K key;

	private V value;

	public PriorityQueueEntry(K key, V value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public K getKey() {
		return key;
	}

	@Override
	public V getValue() {
		return value;
	}

}
