package phanindra.dsandalgos.priorityqueue;

public interface Entry<K,V> {
	K getKey();
	V getValue();
}
