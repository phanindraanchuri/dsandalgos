package phanindra.dsandalgos.priorityqueue;

import java.util.Comparator;

public class KeyComparator<T> implements Comparator<T> {

	@Override
	public int compare(T o1, T o2) {
		Comparable<T> first = (Comparable<T>) o1;
		return first.compareTo(o2);
	}

}
