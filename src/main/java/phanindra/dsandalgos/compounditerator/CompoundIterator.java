package phanindra.dsandalgos.compounditerator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CompoundIterator implements Iterator {

	private final Iterator[] iterators;

	public CompoundIterator(Iterator[] iterators) {
		if (iterators == null || iterators.length == 0) {
			throw new IllegalArgumentException(
					"No iterators passed to constructor");
		}
		this.iterators = iterators;
	}

	public boolean hasNext() {
		for (Iterator iterator : iterators) {
			// Check if any of the iterators have a next element
			if (iterator != null && iterator.hasNext()) {
				return true;
			}
		}
		return false;
	}

	public Object next() {
		// Checking if there is atleast one iterator that has a next element
		if (!hasNext()) {
			throw new NoSuchElementException("Iterators have no more elements");
		}
		for (Iterator iterator : iterators) {
			if (iterator != null && iterator.hasNext()) {
				return iterator.next();
			}
		}
		return null;
	}

	public void remove() throws UnsupportedOperationException {
		throw new UnsupportedOperationException(getClass().getName()
				+ " does not support the remove() operation.");
	}
}
