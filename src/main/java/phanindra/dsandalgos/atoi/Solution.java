package phanindra.dsandalgos.atoi;

public class Solution {
	static int atoi(String input) {
		char[] inputArr = input.toCharArray();
		boolean signExists = inputArr[0] == '-';
		int startIndex = 0;
		if (signExists) {
			startIndex = 1;
		}
		for (int i = startIndex; i < inputArr.length; i++) {
			if (!Character.isDigit(inputArr[i])) {
				throw new RuntimeException(
						"Encountered a non digit character at index " + i
								+ ", cannot convert '" + input
								+ "' to an integer");
			}
		}

		int result = 0;
		for (int i = startIndex; i < inputArr.length; i++) {
			result = result * 10;
			result += inputArr[i] - '0';
		}
		return signExists ? -result : result;
	}

	public static void main(String[] args) {
		System.out.println(Solution.atoi("-123"));
	}
}
