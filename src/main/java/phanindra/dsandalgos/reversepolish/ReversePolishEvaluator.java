package phanindra.dsandalgos.reversepolish;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

public class ReversePolishEvaluator {

	private static Set<String> operators = new HashSet<String>();

	static {
		operators.add("+");
		operators.add("-");
		operators.add("*");
		operators.add("/");
	}

	public int evalRPN(String[] tokens) {
		Deque<String> stack = new ArrayDeque<String>();
		for (String token : tokens) {
			if (!operators.contains(token)) {
				stack.push(token);
			} else {
				String operator = token;
				String operand2 = stack.pop();
				String operand1 = stack.pop();
				stack.push(String.valueOf(performOperation(operator, operand1,
						operand2)));
			}

		}
		return Integer.parseInt(stack.pop());
	}

	private static int performOperation(String operator, String operand1,
			String operand2) {
		int operandOne = Integer.parseInt(operand1);
		int operandTwo = Integer.parseInt(operand2);

		switch (operator) {
		case "+":
			return operandOne + operandTwo;
		case "-":
			return operandOne - operandTwo;
		case "*":
			return operandOne * operandTwo;
		case "/":
			if(operandTwo == 0) {
				throw new IllegalArgumentException();
			}
			return operandOne / operandTwo;
		default:
			return -1;
		}
	}
	
	public static void main(String[] args) {
		ReversePolishEvaluator evaluator = new ReversePolishEvaluator();
		System.out.println(evaluator.evalRPN(new String[] {"4", "3", "-"}));
	}
}
