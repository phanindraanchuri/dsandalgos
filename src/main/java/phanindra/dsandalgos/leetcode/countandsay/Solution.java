package phanindra.dsandalgos.leetcode.countandsay;

import java.util.List;
import java.util.ArrayList;

public class Solution {

	public String countAndSay(int n) {
		List<String> sequence = new ArrayList<String>();
		sequence.add("1");
		for(int i= 1; i< n; i++) {
			sequence.add(produceNext(sequence.get(i-1)));
		}
		return sequence.get(n-1);
	}

	private String produceNext(String current) {
		String next = "";
		if (current.isEmpty()) {
			return next;
		} else if (current.length() == 1) {
			return "1" + current;
		} else {
			int i = 0;
			int j = 1;
			int count = 1;
			while (i < current.length()) {
				while (j < current.length()
						&& current.charAt(i) == current.charAt(j)) {
					i++;
					j++;
					count++;
				}
				next = next + count + current.charAt(i);
				i++;
				j++;
				count = 1;
			}
		}
		return next;
	}

	public static void main(String[] args) {
		Solution solution = new Solution();
		System.out.println(solution.countAndSay(5));
	}
}
