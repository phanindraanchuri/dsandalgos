package phanindra.dsandalgos.sorting;

public class Quicksort {

	public static void sort(int[] array) {
		quicksort(array, 0, array.length - 1);
	}

	private static void quicksort(int[] array, int p, int r) {
		if (p < r) {
			int q = partition(array, p, r);
			quicksort(array, p, q);
			quicksort(array, q + 1, r);
		}
	}

	private static int partition(int[] array, int p, int r) {
		int pivot = array[p];
		int i = p;
		int j = r;
		while (i < j) {
			// i moves forward until it encounters an element greater than the
			// pivot
			while (array[i] < pivot) {
				i++;
			}
			// j moves forward until it encounters an element less than the
			// pivot
			while (array[j] > pivot) {
				j--;
			}
			// found a value greater than the pivot on the left side of the
			// array and a value less than the pivot on the right side
			swap(array, i, j);
		}
		return j;
	}

	private static void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;

	}
}
