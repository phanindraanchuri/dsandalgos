package phanindra.dsandalgos.binarysearchtree;

import phanindra.dsandalgos.binarytree.BinaryTreeNode;

public class BinarySearchTree {
	private BinaryTreeNode root;

	public BinarySearchTree() {
	}

	public BinarySearchTree(BinaryTreeNode root) {
		this.root = root;
	}

	public boolean search(int data) {
		if (root == null) {
			return false;
		} else {
			BinaryTreeNode searchNode = new BinaryTreeNode(data);
			if (root.compareTo(searchNode) == 0) {
				return true;
			} else if (searchNode.compareTo(root) > 0) {
				BinarySearchTree left = new BinarySearchTree(root.getLeft());
				return left.search(data);
			} else {
				BinarySearchTree right = new BinarySearchTree(root.getRight());
				return right.search(data);
			}
		}
	}

	public BinaryTreeNode insert(int data) {
		//If the tree is empty, create a node and make it the root node
		if(root == null) {
			root = new BinaryTreeNode(data);
		} else {
			BinaryTreeNode node = new BinaryTreeNode(data);
			if(node.compareTo(root) <= 0) {
				BinarySearchTree leftTree = new BinarySearchTree(root.getLeft());
				root.setLeft(leftTree.insert(data));
			} else {
				BinarySearchTree rightTree = new BinarySearchTree(root.getRight());
				root.setRight(rightTree.insert(data));
			}
		}
		return root;
	}

	public int size() {
		if(root == null) {
			return 0;
		} else {
			return new BinarySearchTree(root.getLeft()).size() + 1 + new BinarySearchTree(root.getRight()).size();
		}
	}

	public int minValue() {
		if(root == null) {
			return Integer.MIN_VALUE;
		} else {
			BinaryTreeNode currentNode = root;
			while(currentNode.getLeft() != null) {
				currentNode = currentNode.getLeft();
			}
			return currentNode.getData();
		}
	}

	public void inorder() {
		if( root == null ) {
			return;
		} else {
			new BinarySearchTree(root.getLeft()).inorder();
			System.out.println(root.getData());
			new BinarySearchTree(root.getRight()).inorder();
		}
	}

	public void postorder() {
		if( root == null ) {
			return;
		} else {
			new BinarySearchTree(root.getLeft()).postorder();
			new BinarySearchTree(root.getRight()).postorder();
			System.out.println(root.getData());
		}
	}
}
