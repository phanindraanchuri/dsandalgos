package phanindra.dsandalgos;

import phanindra.dsandalgos.exceptions.IllegalInitialCapacityException;

public class Array {

	/**
	 * Capacity is different from size. Size is the number of elements currently
	 * stored. Capacity is the number of elements that can be stored before the
	 * data structure has to restructure its internal storage.
	 */
	private static final int DEFAULT_INITIAL_CAPACITY = 10;

	private static final int CAPACITY_INCREMENT_FACTOR = 2;

	private int[] array;

	private int currentCapacity;

	public Array() {
		array = new int[DEFAULT_INITIAL_CAPACITY];
	}

	public Array(int initialCapacity) {
		if(initialCapacity < 0) {
			throw new IllegalInitialCapacityException();
		}
		array = new int[initialCapacity];
	}

	public int length() {
		return array.length;
	}

	public int remainingCapacity() {
		return currentCapacity - length();
	}

	public boolean isFull() {
		return remainingCapacity() == 0;
	}

	public void insert(int index, int value) {
		if(isFull()) {
			createNewArrayAndCopyElements(index, value, incrementCapacity());
		} else {
			createNewArrayAndCopyElements(index,value, currentCapacity);
		}
	}

	public int get(int index) {
		return array[index];
	}

	private void createNewArrayAndCopyElements(int index, int value, int capacity) {
		int[] newArray = new int[capacity];
		copyElementsToNewArray(index, value, newArray);
		array = newArray;
	}

	private void copyElementsToNewArray(int index, int value, int[] newArray) {
		for(int i = 0 ; i<index; i++) {
			newArray[i] = array[i];
		}
		newArray[index] = value;
		for(int i = index+1; i<array.length; i++) {
			newArray[i] = array[i-1];
		}
	}

	public int incrementCapacity() {
		int newCapacity = currentCapacity*CAPACITY_INCREMENT_FACTOR;
		currentCapacity = newCapacity;
		return newCapacity;

	}

}
