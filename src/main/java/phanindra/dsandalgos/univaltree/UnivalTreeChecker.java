package phanindra.dsandalgos.univaltree;

import java.util.Stack;

import phanindra.dsandalgos.binarytree.BinaryTree;
import phanindra.dsandalgos.binarytree.BinaryTreeNode;

public class UnivalTreeChecker {
	public static boolean checkRecursive(BinaryTree tree) {
		BinaryTreeNode rootNode = tree.getRoot();
		if (rootNode == null) {
			return true;
		}
		return checkTree(rootNode.getLeft(), rootNode.getData())
				&& checkTree(rootNode.getRight(), rootNode.getData());
	}

	public static boolean checkTree(BinaryTreeNode root, int data) {
		if (root == null) {
			return true;
		}
		if (root.getData() != data) {
			return false;
		}
		return checkTree(root.getLeft(), data)
				&& checkTree(root.getRight(), data);
	}

	public static boolean checkIterative(BinaryTree tree) {
		Stack<BinaryTreeNode> stack = new Stack<>();
		stack.push(tree.getRoot());
		while(!stack.isEmpty()) {
			BinaryTreeNode node = stack.pop();
			if(node.getData() != tree.getRoot().getData()) {
				return false;
			}
			if(node.getLeft() != null) {
				stack.push(node.getLeft());
			}
			if(node.getRight() != null) {
				stack.push(node.getRight());
			}
		}
		return true;
	}
}
