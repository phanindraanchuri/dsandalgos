package phanindra.dsandalgos.mergeintervals;

public class Interval {
	int start;
	int end;

	Interval() {
		start = 0;
		end = 0;
	}

	Interval(int s, int e) {
		start = s;
		end = e;
	}

	@Override
	public String toString() {
		return "Interval [ start:" + start + ", end:" + end + "]";
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Interval && this.start == ((Interval)other).start
				&& this.end == ((Interval)other).end;
	}
}
