package phanindra.dsandalgos.mergeintervals;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Solution {
	public List<Interval> merge(List<Interval> intervals) {
		if (intervals.isEmpty() || intervals.size() == 1) {
			return intervals;
		}

		Collections.sort(intervals, new IntervalStartComparator());

		int index = 0;
		while (index < intervals.size() - 1) {
			Interval mergedInterval = merge(intervals.get(index),
					intervals.get(index + 1));
			if(mergedInterval != null) {
				intervals.remove(index);
				intervals.remove(index);
				intervals.add(index, mergedInterval);
			}
			index++;
			if(index == intervals.size()) {
				break;
			}
		}
		
		if(intervals.size() >= 2) {
			Interval lastButOne = intervals.get(intervals.size()-2);
			Interval lastOne = intervals.get(intervals.size()-1);
			Interval mergedInterval = merge(lastButOne, lastOne);
			if(mergedInterval != null) {
				intervals.remove(intervals.size()-1);
				intervals.remove(intervals.size()-1);
				intervals.add(mergedInterval);
			}
		}
		return intervals;
	}

	private Interval merge(Interval first, Interval second) {
		Interval mergedInterval = null;
		if (first.start == second.start && first.end == second.end) {
			mergedInterval = new Interval(first.start, first.end);
		} else if (second.start < first.end) {
			mergedInterval = new Interval(first.start, Math.max(first.end,
					second.end));
		} else if(second.start == first.end) {
			mergedInterval = new Interval(first.start, second.end);
		}
		return mergedInterval;
	}
}

class IntervalStartComparator implements Comparator<Interval> {
	@Override
	public int compare(Interval a, Interval b) {
		return Integer.compare(a.start, b.start);
	}
}