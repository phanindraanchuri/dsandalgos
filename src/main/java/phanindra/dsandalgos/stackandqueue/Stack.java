package phanindra.dsandalgos.stackandqueue;

public class Stack {
	private LinkedList linkedList;

	public Stack() {
		linkedList = new LinkedList();
	}

	public void push(int item) {
		linkedList.insertAtHead(item);
	}

	public Integer pop() {
		return linkedList.deleteAtHead();
	}
}
