package phanindra.dsandalgos.stackandqueue;

public class LinkedListNode {
	private Integer data;
	private LinkedListNode next;

	public LinkedListNode(int data) {
		this.data = data;
	}

	public LinkedListNode getNext() {
		return next;
	}

	public void setNext(LinkedListNode next) {
		this.next = next;
	}

	public int getData() {
		return data;
	}

	@Override
	public String toString() {
		return data.toString();
	}
}
