package phanindra.dsandalgos.stackandqueue;

public class LinkedList {
	private LinkedListNode head;
	private LinkedListNode tail;

	public boolean isEmpty() {
		return head == null;
	}

	public void insertAtTail(int item) {
		LinkedListNode newItem = new LinkedListNode(item);
		if(isEmpty()) {
			head = newItem;
			tail = head;
		} else {
			tail.setNext(newItem);
			tail = tail.getNext();
		}
	}

	public void insertAtHead(int item) {
		LinkedListNode newItem = new LinkedListNode(item);
		if(isEmpty()) {
			head = newItem;
			tail = newItem;
		} else {
			LinkedListNode oldHead = head;
			head = newItem;
			head.setNext(oldHead);
		}
	}

	public Integer deleteAtHead() {
		if(isEmpty()) {
			return null;
		} else {
			Integer headItem = head.getData();
			head = head.getNext();
			if(isEmpty()) {
				tail = null;
			}
			return headItem;
		}
	}

	public LinkedListNode getHead() {
		return head;
	}
}
