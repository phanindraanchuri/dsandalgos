package phanindra.dsandalgos.stackandqueue;

public class Queue {
	private LinkedList linkedList;

	public Queue() {
		linkedList = new LinkedList();
	}

	public void enqueue(int item) {
		linkedList.insertAtTail(item);
	}

	public Integer dequeue() {
		if(linkedList.isEmpty()) {
			return null;
		} else {
			return linkedList.deleteAtHead();
		}
	}

	public Integer peek() {
		if(linkedList.isEmpty()) {
			return null;
		} else {
			return linkedList.getHead().getData();
		}
	}
}
