package phanindra.dsandalgos.firstnonrepeatedchar;

import java.util.LinkedHashMap;
import java.util.Map;

public class FirstNonRepeatedChar {
	public Character firstNonRepeatedChar(String input) {
		if (input.isEmpty()) {
			return null;
		}
		if (input.length() == 1) {
			return input.charAt(0);
		}

		Map<Character, Integer> charIndicesMap = new LinkedHashMap<>();
		for (int index = 0; index < input.length(); index++) {
			char currentChar = input.charAt(index);
			if (charIndicesMap.containsKey(currentChar)) {
				charIndicesMap.put(currentChar,
						charIndicesMap.get(currentChar) + 1);
			} else {
				charIndicesMap.put(currentChar, 1);
			}
		}
		for (Map.Entry<Character, Integer> entry : charIndicesMap.entrySet()) {
			if (entry.getValue() == 1) {
				return entry.getKey();
			}
		}
		return null;
	}
}
