package phanindra.dsandalgos.firstnonrepeatedchar;

import java.util.HashMap;
import java.util.Map;

public class Solution {
	char firstNonRepeated(String input) {
		Character result = null;
		if (input != null && !input.isEmpty()) {
			Map<Character, Integer> charCountMap = new HashMap<>();
			for (Character c : input.toCharArray()) {
				if (charCountMap.containsKey(c)) {
					charCountMap.put(c, charCountMap.get(c) + 1);
				} else {
					charCountMap.put(c, 1);
				}
			}

			for (Character c : input.toCharArray()) {
				if (charCountMap.get(c) == 1) {
					result = c;
					break;
				}
			}
		}
		return result;
	}

	enum OCCURRENCETYPE {
		REPEATED, UNIQUE, NO_OCCURRENCE
	};

	char firstNonRepeatedUsingArray(String input) {
		OCCURRENCETYPE[] arr = new OCCURRENCETYPE[65536]; // Unicode
		for (int i = 0; i < arr.length; i++) {
			arr[i] = OCCURRENCETYPE.NO_OCCURRENCE;
		}
		Character result = null;
		for (Character c : input.toCharArray()) {
			// System.out.println(c-'0');
			OCCURRENCETYPE occurenceType = arr[c - '0'];
			if (OCCURRENCETYPE.NO_OCCURRENCE.equals(occurenceType)) {
				arr[c - '0'] = OCCURRENCETYPE.UNIQUE;
			} else if (OCCURRENCETYPE.UNIQUE.equals(occurenceType)) {
				arr[c - '0'] = OCCURRENCETYPE.REPEATED;
			}
		}

		for (Character c : input.toCharArray()) {
			if (arr[c - '0'] == OCCURRENCETYPE.UNIQUE) {
				result = c;
				break;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Solution solution = new Solution();
		System.out.println(solution.firstNonRepeatedUsingArray("totolll典"));
	}
}
