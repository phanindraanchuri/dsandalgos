package phanindra.dsandalgos.romantoint;

import java.util.HashMap;
import java.util.Map;

public class RomanToInteger {

	private static Map<Character, Integer> charIntMap = new HashMap<Character, Integer>();

	static {
		charIntMap.put('I', 1);
		charIntMap.put('V', 5);
		charIntMap.put('X', 10);
		charIntMap.put('L', 50);
		charIntMap.put('C', 100);
		charIntMap.put('D', 500);
		charIntMap.put('M', 1000);
	}

	public int romanToInt(String s) {
		int result = 0;
		if (s == null || s.isEmpty()) {
			return result;
		}
		char[] a = s.toCharArray();
		for (int index = 0; index < a.length; index++) {
			int current = charIntMap.get(a[index]);
			if (lastIndex(a, index)) {
				result += current;
			} else {
				int next = charIntMap.get(a[index + 1]);
				if (current >= next) {
					result += current;
				} else {
					result -= current;
				}
			}
		}
		return result;
	}

	private boolean lastIndex(char[] a, int index) {
		return index == a.length - 1;
	}
}
