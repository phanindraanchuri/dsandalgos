package phanindra.dsandalgos.stockbuysell;

public class StockBuySell {
	public Solution findBestTime(int[] stockPrices) {
		int buyIndex = 0;
		int sellIndex = 0;
		int minimumIndex = 0;
		int maximumDifference = 0;

		for(int i=0; i< stockPrices.length; i++) {
			if(stockPrices[i] < stockPrices[minimumIndex]) {
				minimumIndex = i;
			}

			int difference = stockPrices[i] - stockPrices[minimumIndex];

			if(difference > maximumDifference) {
				buyIndex = minimumIndex;
				sellIndex = i;
				maximumDifference = difference;
			}
		}
		return new Solution(buyIndex, sellIndex, maximumDifference);
	}

}
