package phanindra.dsandalgos.stockbuysell;

public class Solution {
	private int buyIndex;
	private int sellIndex;
	private int profit;
	public Solution(int buyIndex, int sellIndex, int profit) {
		this.buyIndex = buyIndex;
		this.sellIndex = sellIndex;
		this.profit = profit;
	}
	@Override
	public String toString() {
		return "Solution [buyIndex=" + buyIndex + ", sellIndex=" + sellIndex
				+ ", profit=" + profit + "]";
	}
}
