package phanindra.dsandalgos.fibonacci;

import java.math.BigInteger;

public class Fibonacci {
	public static BigInteger naive(int n) {
		if (n == 0 || n == 1) {
			return BigInteger.valueOf(n);
		} else {
			return naive(n - 1).add(naive(n - 2));
		}
	}

	public static BigInteger iterative(int n) {
		BigInteger[] memo = new BigInteger[n+1];
		memo[0] = BigInteger.ZERO;
		memo[1] = BigInteger.ONE;

		for(int i = 2; i <= n; i++) {
			memo[i] = memo[i-1].add(memo[i-2]);
		}
		return memo[n];
	}
}
