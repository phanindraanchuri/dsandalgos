package phanindra.dsandalgos.minsortedrotatedarray;

public class Solution {
	public int findMin(int[] nums) {
		if (nums.length == 0) {
			throw new IllegalArgumentException("Input array cannot be empty");
		}
		if (nums.length == 1) {
			return nums[0];
		}
		int i = 0;
		int j = 1;
		boolean pivotFound = false;
		while (j < nums.length) {
			if (nums[j] > nums[i] || nums[j] == nums[i]) {
				i++;
				j++;
			} else {
				pivotFound = true;
				break;
			}
		}
		if (j == nums.length) {
			return nums[0];
		}
		if (pivotFound) {
			return nums[j];
		} else {
			return nums[j - 1];
		}
	}
}
