package phanindra.dsandalgos.minsortedrotatedarray;

public class BinarySearchSolution {
	public int findMin(int[] nums) {
		if (nums.length == 0) {
			throw new IllegalArgumentException("Input array cannot be empty");
		}
		if (nums.length == 1) {
			return nums[0];
		}
		int low = 0;
		int high = nums.length - 1;
		int mid = low + (high - low) / 2;
		while(low != high) {
			if(nums[low] < nums[high]) {
				return nums[low];
			} else {
				low = mid+1;
			}
		}
		return -1;
	}
}
