package phanindra.dsandalgos.twosum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TwoSum {
	public List<TwoSumPair> twosumTwoPointer(List<Integer> input, int target) {
		List<TwoSumPair> pairs = new ArrayList<>();
		Collections.sort(input);
		int low = 0;
		int high = input.size() - 1;
		while (low < high) {
			if (input.get(low) + input.get(high) > target) {
				high--;
			} else if (input.get(low) + input.get(high) < target) {
				low++;
			} else {
				TwoSumPair pair = new TwoSumPair(input.get(low),
						input.get(high));
				pairs.add(pair);
				low++;
			}
		}
		return pairs;
	}

	public List<TwoSumPair> twosumMap(List<Integer> input, int target) {
		List<TwoSumPair> pairs = new ArrayList<>();
		Map<Integer, Integer> numberIndexMap = new HashMap<>();

		for (int index = 0; index < input.size(); index++) {
			Integer currentElement = input.get(index);
			if (!numberIndexMap.containsKey(target - currentElement)) {
				numberIndexMap.put(currentElement, index);
			} else {
				if (numberIndexMap.containsKey(target - currentElement)) {
					pairs.add(new TwoSumPair(currentElement, target
							- currentElement));
				}
			}
		}
		return pairs;
	}
}
