package phanindra.dsandalgos.twosum;

public class TwoSumPair {
	private int first;
	private int second;
	
	public TwoSumPair(int first, int second) {
		this.first = first;
		this.second = second;
	}
	
	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() +"("+first+","+second+")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + first;
		result = prime * result + second;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TwoSumPair other = (TwoSumPair) obj;
		if(first == other.second && second == other.first) {
			return true;
		}
		if (first != other.first)
			return false;
		if (second != other.second)
			return false;
		return true;
	}
}
