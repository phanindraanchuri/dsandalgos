package phanindra.dsandalgos.pancakesort;

import java.util.Arrays;

public class PancakeSort {
	static int[] sort(int[] a, int startIndex, int endIndex) {
		while (startIndex != endIndex) {
			int maxIndex = findMaxIndex(a, startIndex, endIndex);
			reverse(a, startIndex, maxIndex);
			reverse(a, startIndex, endIndex);
			endIndex = endIndex - 1;
		}
		return a;
	}

	static int[] sort(int[] a) {
		return sort(a, 0, a.length - 1);
	}

	static int[] sortRecursive(int[] a) {
		return sortRecursive(a, 0, a.length - 1);
	}

	private static int[] sortRecursive(int[] a, int startIndex, int endIndex) {
		if (startIndex != endIndex) {
			int maxIndex = findMaxIndex(a, startIndex, endIndex);
			reverse(a, startIndex, maxIndex);
			reverse(a, startIndex, endIndex);
			endIndex = endIndex - 1;
			return sortRecursive(a, startIndex, endIndex);
		}
		return a;
	}

	static int findMaxIndex(int[] a, int startIndex, int endIndex) {
		int max = a[startIndex];
		int maxIndex = startIndex;
		for (int i = 1; i <= endIndex; i++) {
			if (a[i] > max) {
				max = a[i];
				maxIndex = i;
			}
		}
		return maxIndex;
	}

	static int[] reverse(int[] a) {
		return reverse(a, 0, a.length - 1);
	}

	static int[] reverse(int[] a, int start, int end) {
		int i = start;
		int j = end;
		while (i < j) {
			int temp = a[i];
			a[i] = a[j];
			a[j] = temp;
			i++;
			j--;
		}
		return a;
	}

	public static void main(String[] args) {
		int[] result = PancakeSort.sortRecursive(new int[] { 7, 3, 27, 11, 21,
				37, 4 });
		System.out.println(Arrays.toString(result));
	}
}
