package phanindra.dsandalgos.anagrams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnagramGrouper {

	private List<String> input;

	public AnagramGrouper(List<String> input) {
		this.input = input;
	}

	public List<List<String>> groups() {
		Map<Map<Character, Integer>, List<String>> m = input.stream()
				.collect(Collectors.groupingBy(i -> countHash(i)));
		// System.out.println(m);
		// for (List<String> l : m.values()) {
		// System.out.println(l);
		// }
		
		List<List<String>> groups = new ArrayList<List<String>>();
		for(List<String> list: m.values()) {
			groups.add(list);
		}
		return groups;
	}

	private Map<Character, Integer> countHash(String i) {
		Map<Character, Integer> m = new HashMap<>();
		char[] chars = i.toCharArray();
		for (char chaar : chars) {
			if (m.containsKey(chaar)) {
				m.put(chaar, m.get(chaar) + 1);
			} else {
				m.put(chaar, 1);
			}
		}
		return m;
	}
}
