package phanindra.dsandalgos.anagrams;

import java.util.HashMap;
import java.util.Map;

public class AnagramChecker {

	// Assumes no spaces etc.,
	public static boolean check(String s1, String s2) {
		if (s1.length() != s2.length()) {
			return false;
		}

		Map<Character, Integer> charCountMap = new HashMap<Character, Integer>();
		for (char c : s1.toCharArray()) {
			if (charCountMap.containsKey(c)) {
				Integer currentCount = charCountMap.get(c);
				charCountMap.put(c, currentCount + 1);
			} else {
				charCountMap.put(c, 1);
			}
		}

		for (char c : s2.toCharArray()) {
			if (charCountMap.containsKey(c)) {
				Integer currentCount = charCountMap.get(c);
				charCountMap.put(c, currentCount - 1);
				if (charCountMap.get(c) < 0) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
}
