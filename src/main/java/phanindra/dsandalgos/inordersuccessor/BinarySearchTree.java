package phanindra.dsandalgos.inordersuccessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class BinarySearchTree {
	private BinaryTreeNode root;

	public BinarySearchTree() {
	}

	public BinarySearchTree(BinaryTreeNode root) {
		this.root = root;
	}

	public BinaryTreeNode getRoot() {
		return root;
	}

	public boolean isEmpty() {
		return root == null;
	}

	public boolean search(int data) {
		return searchRecursive(data, root);
	}

	public boolean searchRecursive(int data, BinaryTreeNode node) {
		BinaryTreeNode searchNode = new BinaryTreeNode(data);
		if (isEmpty()) {
			return false;
		} else if (searchNode.compareTo(node) == 0) {
			return true;
		} else if (searchNode.compareTo(node) > 0
				&& node.getRight() != null) {
			return searchRecursive(data, node.getRight());
		} else if (searchNode.compareTo(node) < 0
				&& node.getLeft() != null) {
			return searchRecursive(data, node.getLeft());
		}
		return false;
	}

	public void insert(int data) {
		if (search(data)) {
			return;
		} else {
			BinaryTreeNode newNode = new BinaryTreeNode(data);
			if (isEmpty()) {
				root = newNode;
			} else {
				BinaryTreeNode currentNode = root;
				BinaryTreeNode parentNode = root;
				while (currentNode != null) {
					if (newNode.compareTo(currentNode) < 0) {
						parentNode = currentNode;
						currentNode = currentNode.getLeft();
					} else if (newNode.compareTo(currentNode) > 0) {
						parentNode = currentNode;
						currentNode = currentNode.getRight();
					}
				}

				currentNode = newNode;
				if (currentNode.compareTo(parentNode) > 0) {
					parentNode.setRight(newNode);
				} else {
					parentNode.setLeft(newNode);
				}
			}
		}
	}

	public List<BinaryTreeNode> inorderIterative() {
		List<BinaryTreeNode> list = new ArrayList<BinaryTreeNode>();
		Stack<BinaryTreeNode> s = new Stack<BinaryTreeNode>();
		BinaryTreeNode currentNode = root;
		boolean done = false;
		while(!done) {
			if(currentNode != null) {
				System.out.println("Pushing "+ currentNode + "onto stack");
				s.push(currentNode);
				currentNode = currentNode.getLeft();
			} else {
				if(s.isEmpty()) {
					done = true;
				} else {
					currentNode = s.peek();
					System.out.println("Looking at "+ currentNode);
					s.pop();
					list.add(currentNode);
					currentNode = currentNode.getRight();
				}
			}
		}
		return list;
	}

}
