package phanindra.dsandalgos;

public class LinkedList {

	private ListNode head;

	public boolean isEmpty() {
		return head == null;
	}

	public int length() {
		if (this.head == null) {
			return 0;
		} else {
			int size = 0;
			ListNode currentNode = this.head;
			while (currentNode != null) {
				currentNode = currentNode.getNext();
				size++;
			}
			return size;
		}
	}

	public void print() {
		if (!isEmpty()) {
			ListNode node = head;
			while (node != null) {
				node.print();
				node = node.getNext();
			}
		}
	}

	public ListNode get(int position) {
		if(isEmpty()) {
			return null;
		}
		ListNode node = head;
		for(int i=0; i< position; i++) {
		    node = node.getNext();
		}
		return node;
	}

	public ListNode find(int data) {
		ListNode searchNode = new ListNode(data);
		ListNode aNode = head;
		if(!isEmpty()) {
			while(aNode.getNext() != null) {
				if(aNode.equals(searchNode)) {
					return aNode;
				}
				aNode = aNode.getNext();
			}
		}
		return null;
	}

	public void addFirst(int data) {
		ListNode node = new ListNode(data, head);
		setHead(node);
	}

	public ListNode addLast(int data) {
		ListNode node = new ListNode(data);
		if (isEmpty()) {
			head = node;
		} else {
			ListNode aNode = head;
			while (aNode.getNext() != null) {
				aNode = aNode.getNext();
			}
			aNode.setNext(node);
		}
		return node;
	}

	public void addAt(int position, int data) {
		ListNode node = new ListNode(data);

		if (position > length()) {
			throw new RuntimeException(String.format(
					"Cannot add %d at position %d", data, position));
		} else if (isEmpty()) {
			head = node;
		} else {
			ListNode aNode = head;
			for (int i = 0; i < position - 1; i++) {
				aNode = aNode.getNext() != null ? aNode.getNext() : aNode;
			}
			ListNode previousNode = aNode;
			ListNode nextNode = aNode.getNext();
			previousNode.setNext(node);
			node.setNext(nextNode);
		}
	}

	public void removeFirst() {
		ListNode newHead = head.getNext();
		head.setNext(null);
		head = newHead;
	}

	public ListNode reverse() {
		ListNode previousNode = null;
		ListNode nextNode = null;
		ListNode currentNode = head;
		while(currentNode != null) {
			// Save the next node before the current node's next reference
			// gets destroyed.
			nextNode = currentNode.getNext();
			// make next reference point backwards
			currentNode.setNext(previousNode);

			// setup previous node and current node for next iteration.
			previousNode = currentNode;
			currentNode = nextNode;
		}
		return previousNode;
	}

	public boolean hasLoop() {
		ListNode tortoise = head;
		ListNode hare = head;

		while(hare.getNext() !=null && hare.getNext().getNext() != null) {
			tortoise = tortoise.getNext();
			hare = hare.getNext().getNext();

			if(tortoise == hare) {
				return true;
			}
		}
		return false;
	}

	public ListNode getHead() {
		return head;
	}

	public void setHead(ListNode head) {
		this.head = head;
	}

}
