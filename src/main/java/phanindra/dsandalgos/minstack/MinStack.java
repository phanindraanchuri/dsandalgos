package phanindra.dsandalgos.minstack;

import java.util.Stack;

public class MinStack<E extends Comparable<E>> {

	private Stack<E> stack;
	/**
	 * minStack keeps track of the minimum so far.
	 */
	private Stack<E> minStack;
	private E currentMinimum;

	public MinStack() {
		this.stack = new Stack<>();
		this.minStack = new Stack<>();
	}

	public void push(E element) {
		stack.push(element);
		if(currentMinimum == null) {
			currentMinimum = element;
		}
		// Checking if the element being pushed is
		// lesser than the current minimum.
		if(element.compareTo(currentMinimum) < 0) {
			currentMinimum = element;
		}
		minStack.push(currentMinimum);
	}

	public E pop() {
		minStack.pop();
		return stack.pop();
	}

	public E minumum() {
		return minStack.peek();
	}
}
