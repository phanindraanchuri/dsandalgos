package phanindra.dsandalgos.singlesell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * Given : Array of stock prices Constraints: Only allowed to buy exactly once
 * and sell exactly once Need to buy before we sell Solve: Maximum profit that
 * can be made
 *
 */
public class SingleSellProblem {
	private int[] stockPrices;

	private List<Integer> stockPricesList;

	public SingleSellProblem(int[] stockPrices) {
		this.stockPrices = stockPrices;
		stockPricesList = new ArrayList<>();
		for (int stockPrice : stockPrices) {
			stockPricesList.add(stockPrice);
		}
	}

	public Solution bruteForce() {
		int maxProfit = 0;
		int buyIndex = 0;
		int sellIndex = 0;

		for (int i = 0; i < stockPrices.length; i++) {
			for (int j = i + 1; j < stockPrices.length; j++) {
				if (stockPrices[j] - stockPrices[i] > maxProfit) {
					buyIndex = i;
					sellIndex = j;
					maxProfit = stockPrices[j] - stockPrices[i];
				}
			}
		}
		Solution solution = new Solution();
		solution.setProfit(maxProfit);
		solution.setBuyIndex(buyIndex);
		solution.setSellIndex(sellIndex);
		return solution;
	}

	public Solution divideAndConquer() {
		int profit = divideAndConquerHelper(stockPricesList);
		Solution solution = new Solution();
		solution.setProfit(profit);
		return solution;
	}

	public int divideAndConquerHelper(List<Integer> list) {
		if (list.size() == 0 || list.size() == 1) {
			return 0;
		} else {
			List<Integer> leftHalf = list.subList(0, list.size() / 2);
			List<Integer> rightHalf = list
					.subList(list.size() / 2, list.size());
			int l = divideAndConquerHelper(leftHalf);
			int r = divideAndConquerHelper(rightHalf);
			int minInLeftHalf = Collections.min(leftHalf);
			int maxInRightHalf = Collections.max(rightHalf);
			return Math.max(maxInRightHalf - minInLeftHalf, Math.max(l, r));
		}
	}

	public Solution dynamicProgramming() {
		Solution solution = new Solution();
		if(stockPrices.length == 1) {
			solution.setProfit(0);
			solution.setBuyIndex(0);
			solution.setSellIndex(0);
		} else {
			int min = stockPrices[0];
			int profit = 0;
			for(int i= 1; i < stockPrices.length; i++) {
				min = Math.min(min, stockPrices[i]);
				profit = Math.max(profit, stockPrices[i] - min);
			}
			solution.setProfit(profit);
		}
		return solution;
	}
}
