package phanindra.dsandalgos.singlesell;

public class Solution {
	private int buyIndex;
	private int sellIndex;
	private int profit;

	public int getBuyIndex() {
		return buyIndex;
	}

	public void setBuyIndex(int buyIndex) {
		this.buyIndex = buyIndex;
	}

	public int getSellIndex() {
		return sellIndex;
	}

	public void setSellIndex(int sellIndex) {
		this.sellIndex = sellIndex;
	}

	public int getProfit() {
		return profit;
	}

	public void setProfit(int profit) {
		this.profit = profit;
	}

	@Override
	public String toString() {
		return "Solution [buyIndex=" + buyIndex + ", sellIndex=" + sellIndex
				+ ", profit=" + profit + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + buyIndex;
		result = prime * result + profit;
		result = prime * result + sellIndex;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Solution other = (Solution) obj;
		if (buyIndex != other.buyIndex)
			return false;
		if (profit != other.profit)
			return false;
		if (sellIndex != other.sellIndex)
			return false;
		return true;
	}
}
