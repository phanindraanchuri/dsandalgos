package phanindra.dsandalgos.balancedparens;

import java.util.AbstractMap;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BalancedParanthesesChecker {

	private static final Set<Character> openParens = new HashSet<>();

	private static final Map<Character, Character> openCloseMap = new HashMap<>();

	static {
		openCloseMap.put('{', '}');
		openCloseMap.put('(', ')');
		openCloseMap.put('[', ']');
		openParens.add('{');
		openParens.add('(');
		openParens.add('[');
	}

	public boolean isBalanced(String input) {
		if (input.isEmpty()) {
			return true;
		}
		if (input.length() % 2 != 0) {
			return false;
		}
		Deque<Character> stack = new ArrayDeque<>();
		for (Character c : input.toCharArray()) {
			if (openParens.contains(c)) {
				stack.push(c);
			} else {
				if (stack.isEmpty()) {
					return false;
				}
				Character popped = stack.pop();
				Map.Entry<Character, Character> entry = new AbstractMap.SimpleEntry<>(
						popped, c);
				if (!openCloseMap.entrySet().contains(entry)) {
					return false;
				}
			}
		}
		return stack.isEmpty();
	}
}
