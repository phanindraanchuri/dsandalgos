package phanindra.dsandalgos.binarytree;

public class BinaryTreeProblems {
	public boolean hasPathSum(BinaryTree tree, int sum) {
		return hasPathSumRecursive(tree.getRoot(), sum);
	}

	public boolean hasPathSumRecursive(BinaryTreeNode node, int sum) {
		if (node == null) {
			return sum == 0;
		} else {
			return hasPathSumRecursive(node.getLeft(), sum - node.getData())
					|| hasPathSumRecursive(node.getRight(),
							sum - node.getData());
		}
	}
}
