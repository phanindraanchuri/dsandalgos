package phanindra.dsandalgos.binarytree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class BinaryTree {
	private BinaryTreeNode root;

	public BinaryTree(BinaryTreeNode root) {
		this.root = root;
	}

	public int size() {
		if (root == null) {
			return 0;
		} else if (root.getLeft() == null && root.getRight() == null) {
			return 1;
		} else {
			return 1 + new BinaryTree(root.getLeft()).size()
					+ new BinaryTree(root.getRight()).size();
		}
	}

	public void printPaths() {
		int[] path = new int[100];
		printPathsRecursive(root, path, 0);
	}

	private void printPathsRecursive(BinaryTreeNode node, int[] path, int index) {
		if (node == null) {
			return;
		}
		path[index++] = node.getData();

		if (node.getLeft() == null && node.getRight() == null) {
			printPath(path, index);
		} else {
			printPathsRecursive(node.getLeft(), path, index);
			printPathsRecursive(node.getRight(), path, index);
		}
	}

	private void printPath(int[] path, int index) {
		for (int i = 0; i < index; i++) {
			System.out.print(path[i] + " ");
		}
		System.out.println();
	}

	public void printPathsIterative() {
		Set<BinaryTreeNode> visited = new HashSet<BinaryTreeNode>();
		Stack<BinaryTreeNode> stack = new Stack<BinaryTreeNode>();
		stack.push(root);

		while (!stack.isEmpty()) {
			BinaryTreeNode top = stack.pop();
			List<BinaryTreeNode> unvisitedChildren = new ArrayList<BinaryTreeNode>();
			if(top.getLeft() != null && !visited.contains(top.getLeft())) {
				unvisitedChildren.add(top.getLeft());
			}
			if(top.getRight()!=null && !visited.contains(top.getRight())) {
				unvisitedChildren.add(top.getRight());
			}

			if(!unvisitedChildren.isEmpty()) {
				stack.push(top);
				stack.push(unvisitedChildren.get(0));
			} else {
				visited.add(top);
				if(top.isLeaf()) {
					print(stack);
					System.out.print(top.getData());
					System.out.println();
				}
			}
		}
	}

	private void print(Stack<BinaryTreeNode> stack) {
		for(BinaryTreeNode node: stack) {
			System.out.print(node.getData());
		}
	}

	public BinaryTreeNode getRoot() {
		return root;
	}
}
