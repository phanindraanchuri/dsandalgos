package phanindra.dsandalgos.binarytree;

public class BinaryTreeNode implements Comparable<BinaryTreeNode> {
	private int data;
	private BinaryTreeNode left;
	private BinaryTreeNode right;

	public BinaryTreeNode(int data) {
		this.data = data;
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public BinaryTreeNode getLeft() {
		return left;
	}

	public void setLeft(BinaryTreeNode left) {
		this.left = left;
	}

	public BinaryTreeNode getRight() {
		return right;
	}

	public void setRight(BinaryTreeNode right) {
		this.right = right;
	}

	public boolean isLeaf() {
		return getLeft() == null && getRight() == null;
	}

	@Override
	public String toString() {
		return "{Data: " + data + "}";
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof BinaryTreeNode
				&& ((BinaryTreeNode) other).getData() == this.getData();
	}

	@Override
	public int compareTo(BinaryTreeNode o) {
		if (this.getData() == o.getData()) {
			return 0;
		} else if (this.getData() > o.getData()) {
			return 1;
		} else {
			return -1;
		}
	}
}
