package phanindra.dsandalgos.majority;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MajorityFinder {
	public static int findMajorityUsingHashMap(int[] nums) {
		Map<Integer, Integer> intCountMap = new HashMap<Integer, Integer>();
		for (int num : nums) {
			if (intCountMap.containsKey(num)) {
				int count = intCountMap.get(num);
				intCountMap.put(num, count + 1);
			} else {
				intCountMap.put(num, 1);
			}
		}

		int key = -1;
		for (Map.Entry<Integer, Integer> entry : intCountMap.entrySet()) {
			if (entry.getValue() > nums.length / 2) {
				key = entry.getKey();
				break;
			}
		}
		return key;
	}
	
	public static int findMajorityUsingDivideAndConquer(int[] nums) {
		if(nums.length == 0) {
			return -1;
		}
		if(nums.length == 1) {
			return nums[0];
		}
		int k = nums.length/2;
		int left = MajorityFinder.findMajorityUsingDivideAndConquer(Arrays.copyOfRange(nums, 0, k));
		int right = MajorityFinder.findMajorityUsingDivideAndConquer(Arrays.copyOfRange(nums, k, nums.length));
		int leftCount = frequency(nums, left);
		int rightCount = frequency(nums, right);
		if(leftCount > k) {
			return left;
		}
		else if(rightCount > k) {
			return right;
		}
		return -1;
	}

	private static int frequency(int[] nums, int element) {
		int count = 0;
		for(int num : nums) {
			if(num == element) {
				count++;
			}
		}
		return count;
	}
}
