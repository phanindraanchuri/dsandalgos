package phanindra.javaconcepts.arraytolist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Conversion {
	public static void main(String[] args) {
		int[] a = { 1, 2, 3 };
		// This returns a List<int[]> not a List<Integer>
		List<int[]> l = Arrays.asList(a);

		// First way, if list does not need to modified
		List<Integer> l2 = Arrays.asList(1, 2, 3);
		// Caveat, the following fails with an UnsupportedOperationException
		// because Arrays.asList returns a fixed-size list that cannot be
		// modified.
		/* l2.add(5); */

		Integer[] b = new Integer[] { 1, 2, 3 };
		List<Integer> l3 = Arrays.asList(b);
		// Caveat, the following fails with an UnsupportedOperationException
		// because Arrays.asList returns a fixed-size list that cannot be
		// modified.
		// l3.add(5);

		// Second way, this works for situations in which we need to modify the
		// list
		List<Integer> l4 = new ArrayList<>(Arrays.asList(b));
		l4.add(5);

		String[] sArr = { "test", "hello" };
		List<String> sList = Arrays.asList(sArr);
		// sList.add("cannot add me");

		// First way of doing it
		List<String> l5 = new ArrayList<>();
		Collections.addAll(l5, sArr);
		// The above is equivalent to
		List<String> l6 = new ArrayList<>(Arrays.asList(sArr));
		System.out.println(l5.equals(l6));
	}
}
