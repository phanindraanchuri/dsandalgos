package phanindra.javaconcepts.package2;

import phanindra.javaconcepts.package1.Parent;

public class ChildOutsidePackage extends Parent {

	public void protectedAccess() {
		// int x=20;
		Parent p = new Parent();
		// p.x=100;
		// System.out.println("Super" + super.x);
		// System.out.println("child" + x);
		y = 20;
		x=100;
	}

	public static void main(String[] args) {
		ChildOutsidePackage child = new ChildOutsidePackage();
		child.protectedAccess();
	}

	public static void sample() {
		Parent p = new Parent();
		// x=100;
		y = 12;
	}

}
