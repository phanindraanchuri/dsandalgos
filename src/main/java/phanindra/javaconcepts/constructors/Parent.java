package phanindra.javaconcepts.constructors;

public class Parent {
  private int a;
  
  private int b;
  
  Parent(int x) {
    this.a = x;
  }
  
  Parent(int x, int y) {
    this.a = x;
    this.b = y;
  }
}
