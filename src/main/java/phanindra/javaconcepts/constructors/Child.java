package phanindra.javaconcepts.constructors;

public class Child extends Parent {
  private int y;
  
  Child(int x) {
    super(3, 4);
    y = x;
  }
}
