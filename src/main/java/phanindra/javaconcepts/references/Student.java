package phanindra.javaconcepts.references;

public class Student {
  public static void main(String[] args) {
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;
    System.out.println(students[0]);
  }
}
