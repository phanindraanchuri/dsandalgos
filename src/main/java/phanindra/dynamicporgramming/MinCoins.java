package phanindra.dynamicporgramming;

public class MinCoins {
  int minCoins(int n, int[] denominations) {
    // minCoins[j] represents the minimum number of coins necessary to form amount j
    int[] minCoins = new int[n + 1];
    // result[j] represents the value of the denomination used to form amount j
    int[] result = new int[n + 1];
    minCoins[0] = 0;
    for (int amount = 1; amount <= n; amount++) {
      // Initially we do not know the minimum number of coins we need to make any amount except for 0
      minCoins[amount] = Integer.MAX_VALUE;
      for (int denominationIndex = 0; denominationIndex < denominations.length; denominationIndex++) {
        if (amount >= denominations[denominationIndex]
          && minCoins[amount] > 1 + minCoins[amount - denominations[denominationIndex]]) {
          // Found a better way to make amount, using denomination at denominationIndex
          minCoins[amount] = 1 + minCoins[amount - denominations[denominationIndex]];
          result[amount] = denominations[denominationIndex];
        }
      }
    }
    if (result[result.length - 1] != 0) {
      int start = result.length - 1;
      while (start != 0) {
        int j = result[start];
        System.out.println(j);
        start = start - j;
      }
    }
    return minCoins[n];
  }

  public static void main(String[] args) {
    MinCoins mc = new MinCoins();
    System.out.println("Minimum number of coins " + mc.minCoins(16, new int[] { 1, 4, 6 }));
  }
}
