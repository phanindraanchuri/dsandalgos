package phanindra.longestseq2d;

public class Solution {
	public static final int[][] dirs = { { 0, 1 }, { 1, 0 }, { 0, -1 },
			{ -1, 0 }, { -1, 1 }, { 1, 1 }, { -1, -1 }, { 1, -1 } };

	public int longestSequence(int[][] matrix) {
		if (matrix.length == 0)
			return 0;
		int m = matrix.length, n = matrix[0].length;
		int[][] cache = new int[m][n];
		int max = 1;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				int len = dfs(matrix, i, j, m, n, cache);
				max = Math.max(max, len);
			}
		}
		return max;
	}

	public int dfs(int[][] matrix, int i, int j, int m, int n, int[][] cache) {
		if (cache[i][j] != 0)
			return cache[i][j];
		int max = 1;
		for (int[] dir : dirs) {
			int x = i + dir[0], y = j + dir[1];
			if (x < 0 || x >= m || y < 0 || y >= n
					|| matrix[x][y] <= matrix[i][j])
				continue;
			int len = 1 + dfs(matrix, x, y, m, n, cache);
			max = Math.max(max, len);
		}
		cache[i][j] = max;
		return max;
	}

	public static void main(String[] args) {
		Solution obj = new Solution();
		int grid[][] = { { 8, 2, 4 }, { 0, 7, 1 }, { 3, 7, 9 } };
		int[][] grid1 = { { 1, 2, 3 }, { 6, 5, 4 }, { 7, 8, 9 } };
		int[][] grid2 = { { 5, 6, 8, 9, 0 }, { 5, 7, 8, 4, 9 },
				{ 3, 2, 1, 0, 6 } };
		int[][] grid3 = { { 1, 1, 1 }, { 1, 1, 1 }, { 0, 0, 0 } };
		System.out.println(obj.longestSequence(grid));
		System.out.println(obj.longestSequence(grid1));
		System.out.println(obj.longestSequence(grid2));
		System.out.println(obj.longestSequence(grid3));
	}
}