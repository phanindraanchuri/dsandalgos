package phanindra.longestseq2d;

public class Suraj {

	private int visited[][];
	private int row, col;
	private int grid[][];
	private int coordinatesToFollow[][] = { { 1, 0 }, { -1, 0 }, { 0, 1 },
			{ 0, -1 }, { -1, -1 }, { 1, -1 }, { -1, 1 }, { 1, 1 } };

	public boolean inRange(int i, int j) {
		if (i < 0 || i >= row || j < 0 || j >= col)
			return false;
		return true;
	}

	public boolean setRowAndCol() {
		row = grid.length;
		if (row == 0)
			return false;
		col = grid[0].length;
		if (col == 0)
			return false;
		return true;
	}

	public int longestSequence(int grid[][]) {
		this.grid = grid;
		int max = 0;
		if (!setRowAndCol())
			return 0;

		visited = new int[row][col];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				int localMax = longestSequenceGet(i, j);
				if (max < localMax)
					max = localMax;
			}
		}
		return max + 1;
	}

	private int longestSequenceGet(int i, int j) {
		visited[i][j] = 1;
		int length = 0;
		for (int k = 0; k < coordinatesToFollow.length; k++) {
			int lengthLocal = 0;
			int x = i + coordinatesToFollow[k][0];
			int y = j + coordinatesToFollow[k][1];

			if (inRange(x, y) && visited[x][y] != 1 && grid[i][j] <= grid[x][y]) {
				lengthLocal = 1 + longestSequenceGet(x, y);

			}
			if (lengthLocal > length)
				length = lengthLocal;
		}

		visited[i][j] = 0;
		return length;
	}

	public static void main(String rags[]) {

		Suraj obj = new Suraj();

		int grid[][] = { { 8, 2, 4 }, { 0, 7, 1 }, { 3, 7, 9 } };
		int[][] grid1 = { { 1, 2, 3 }, { 6, 5, 4 }, { 7, 8, 9 } };
		int[][] grid2 = { { 5, 6, 8, 9, 0 }, { 5, 7, 8, 4, 9 },
				{ 3, 2, 1, 0, 6 } };
		int[][] grid3 = { { 1, 1, 1 }, { 1, 1, 1 }, { 0, 0, 0 } };
		System.out.println(obj.longestSequence(grid));
		System.out.println(obj.longestSequence(grid1));
		System.out.println(obj.longestSequence(grid2));
		System.out.println(obj.longestSequence(grid3));
	}

}
