package phanindra.dp;

public class ZeroOneKnapsack {

	public int solve(int[] weights, int[] values, int W) {
		int numItems = values.length;
		int[][] dp = new int[numItems][W + 1];
		for (int i = 1; i < numItems; i++) {
			for (int w = 1; w <= W; w++) {
				if (weights[i - 1] > w) {
					dp[i][w] = dp[i - 1][w];
				} else {
					dp[i][w] = Math.max(dp[i - 1][w], values[i - 1]
							+ dp[i - 1][w - weights[i]]);
				}
			}
		}
		return dp[numItems][W + 1];
	}

	public static void main(String[] args) {
		int[] weights = { 5, 4, 6, 3 };
		int[] values = { 10, 40, 30, 50 };
		int W = 10;
		ZeroOneKnapsack solution = new ZeroOneKnapsack();
		System.out.println(solution.solve(weights, values, W));
	}
}
