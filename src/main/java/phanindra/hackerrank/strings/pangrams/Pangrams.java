package phanindra.hackerrank.strings.pangrams;

import java.util.Scanner;

/**
 Roy wanted to increase his typing speed for programming contests. So, his friend advised him to type the sentence "The quick brown fox jumps over the lazy dog" repeatedly, because it is a pangram. (Pangrams are sentences constructed by using every letter of the alphabet at least once.)

 After typing the sentence several times, Roy became bored with it. So he started to look for other pangrams.

 Given a sentence , tell Roy if it is a pangram or not.

 Input Format

 Input consists of a string .

 Constraints
 Length of  can be at most   and it may contain spaces, lower case and upper case letters. Lower-case and upper-case instances of a letter are considered the same.

 Output Format

 Output a line containing pangram if  is a pangram, otherwise output not pangram.

 Sample Input

 Input #1

 We promptly judged antique ivory buckles for the next prize
 Input #2

 We promptly judged antique ivory buckles for the prize
 Sample Output

 Output #1

 pangram
 Output #2

 not pangram
 */
public class Pangrams {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        input = input.toLowerCase();
        int[] arr = new int[26];
        for (Character c : input.toCharArray()) {
            int index = c - 'a';
            if (index >= 0) {
                arr[index] = arr[index] + 1;
            }
        }

        boolean isPangram = true;
        for (int i = 0; i < 26; i++) {
            if (arr[i] == 0) {
                isPangram = false;
                System.out.println("not pangram");
                break;
            }
        }
        if (isPangram) {
            System.out.println("pangram");
        }
    }
}
