package recursion;

import java.util.List;
import java.util.ArrayList;

/**
 * Created by phanindra on 4/5/16.
 */
public class Permutations {
    public static void main(String[] args) {
        System.out.println(new Permutations().permutations("abc"));
    }

    public List<String> permutations(String s) {

        List<String> result = new ArrayList<>();
       if(s.length() == 1) {
           result.add(s);
           return result;
       } else {
           char c = s.charAt(0);
           List<String> rest = permutations(s.substring(1));
           for(String r: rest) {
               List<String> l = insertCharAtAllPositions(c, r);
               result.addAll(l);
           }
           return result;
       }
    }

    private List<String> insertCharAtAllPositions(char c, String s) {
        List<String> resultList = new ArrayList<>();
        for (int i = 0; i <= s.length(); i++) {
            String prefix = s.substring(0, i);
            String ch = "" + c;
            String suffix = s.substring(i);
            String result = prefix + ch + suffix;
            resultList.add(result);
        }
        return resultList;
    }
}
