package producerconsumer;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class ProducerConsumer {

    private Queue<Integer> sharedQueue = new LinkedList<>();

    private Object lock = new Object();

    private static final int SIZE = 10;

    private Random random = new Random(100);

    public void produce() {
        while(true) {
            synchronized (lock) {
                while(SIZE == sharedQueue.size()) {
                    try {
                        System.out.println("Queue full, waiting for consumers to consume");
                        lock.wait();
                    } catch(InterruptedException ie) {

                    }
                }
                int i = random.nextInt(100);
                System.out.println("Produced "+ i);
                sharedQueue.add(i);
                lock.notifyAll();
            }
        }
    }

    public void consume() throws InterruptedException {
        while(true) {
            synchronized (lock) {
                while (sharedQueue.isEmpty()) {
                    System.out.println("Queue empty, waiting for consumers to consume");
                    lock.wait();
                }
                Integer i = sharedQueue.poll();
                System.out.println("Consumed/Received "+ i);
                lock.notifyAll();
            }
            Thread.sleep(1000);
        }
    }

    public static void main(String[] args) {
        ProducerConsumer pc = new ProducerConsumer();
        Thread producer = new Thread(() -> {
            pc.produce();
        });
        Thread consumer = new Thread(() -> {
            try {
                pc.consume();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        producer.start();
        consumer.start();
    }
}
