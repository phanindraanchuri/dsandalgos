package binarysearch;

/**
 * Created by phanindra on 7/27/16.
 */
public class NumberGuessingGame {

    /* The guess API is defined in the parent class GuessGame.
   @param num, your guess
   @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
      int guess(int num); */
    public int guessNumber(int n) {
        return guessNumberRecursive(1, n);
    }

    public int guessNumberRecursive(int low, int high) {
        int mid = low + (high - low)/2;
        if(guess(mid) == 0) {
            return mid;
        } else if(guess(mid) == -1) {
            return guessNumberRecursive(low, mid-1);
        } else {
            return guessNumberRecursive(mid+1, high);
        }
    }

//    Iterative version
//    public int guessNumber(int n) {
//        int low = 1, high = n;
//        while(low <= high) {
//            int mid = low + (high - low)/2;
//            int result = guess(mid);
//            if(result == 0) {
//                return mid;
//            } else if(result == -1) {
//                high = mid-1;
//            } else {
//                low = mid+1;
//            }
//        }
//        return -1;
//    }

    private int guess(int num) {
        // return -1 if my number is lower, 1 if my number is higher, otherwise return 0
        // This is just a placeholder for the API that was provided.
        return -1;
    }
}
