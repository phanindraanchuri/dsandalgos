package binarysearch;

public class IntegerIdenticalToIndex {
  // Integers in an array are unique and increasingly sorted.
  // Please write a function/method to find an integer from the array
  // who equals to its index.
  // For example, in the array {-3, -1, 1, 3, 5}, the number 3 equals its index 3.
  static int find(int[] nums) {
    if (nums == null || nums.length == 0) {
      return -1;
    }
    int low = 0;
    int high = nums.length - 1;
    while (low <= high) {
      int mid = low + (high - low) / 2;
      if (nums[mid] == mid) {
        return mid;
      }
      if (nums[mid] < mid) {
        low = mid + 1;
      }
      if (nums[mid] > mid) {
        high = mid - 1;
      }
    }
    return -1;
  }

  public static void main(String[] args) {
    System.out.println(find(new int[] { -3, 1, 3, 4, 5 }));
  }
}
