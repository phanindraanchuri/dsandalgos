package binarysearch;

public class PeakFinder {
  static int findPeak(int[] nums) {
    if (nums.length == 0) {
      return -1;
    }
    int low = 0;
    int high = nums.length - 1;
    while (low <= high) {
      int mid = low + ((high - low) >> 1);
      if (nums[mid] > nums[mid - 1] && nums[mid] > nums[mid + 1]) {
        return nums[mid];
      }
      // The answer is in the direction of the failed check
      // Going left => high = mid -1
      // Going right => low = mid+1
      if (nums[mid] <= nums[mid - 1]) {
        high = mid - 1;
      }
      if (nums[mid] <= nums[mid + 1]) {
        low = mid + 1;
      }
    }
    return -1;
  }

  public static void main(String[] args) {
    int[] nums = new int[] { 1, 2, 3, 4, 5, 10, 9, 8, 7, 6 };
    System.out.println(findPeak(nums));
  }
}
