package twitter;

import java.io.IOException;

public class Main {
  public static void main(String[] args) throws IOException {
    Main.flipBits(new int[] { 1, 0, 0, 1 });
  }

  public static int flipBits(int[] a) {
    int numOnes = 0;
    int numZeros = 0;
    for (int i = 0; i < a.length; i++) {
      for (int j = 0; j <= i; j++) {
        if(a[j] == 0) {
          numZeros++;
        }
        System.out.print(a[j]);
      }
      System.out.println();
    }
    return 0;
  }
}
