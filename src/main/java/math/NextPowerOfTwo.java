package math;

import static java.lang.Math.*;

/**
 * Created by phanindra on 7/4/16.
 */
public class NextPowerOfTwo {
    public static int nextUsingLog(int n) {
        // Since Math.log is base e or natural logarithm
        double logBaseTwo = log(n) / log(2);
        return (int) pow(2, ceil(logBaseTwo));
    }

    public static int nextUsingLeftShift(int n) {
        int next = 1;
        while (next < n) {
            next = next << 1;
        }
        return next;
    }

    public static void main(String[] args) {
        System.out.println(NextPowerOfTwo.nextUsingLog(17));
        System.out.println(NextPowerOfTwo.nextUsingLeftShift(17));
    }
}