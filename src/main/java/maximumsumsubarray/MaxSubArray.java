package maximumsumsubarray;

/**
 * Created by phanindra on 7/31/16.
 */
public class MaxSubArray {
    public int maxSumSubArray(int[] nums) {
        return maxSumSubArrayRec(nums);
    }

    /** Generates all subarrays, subarrays of length 1, subarrays of length 2 and calculates
     the sum of each such subarray and updates a global maximum sum - O(n^3)
     */
    public int maxSumSubArrayBruteForce(int[] nums) {
        int maxSum = Integer.MIN_VALUE;
        int n = nums.length;
        for(int subArraySize = 1; subArraySize <= n; subArraySize++) {
            for(int startIndex = 0; startIndex + subArraySize - 1 < n; startIndex++) {
                int endIndex = startIndex + subArraySize - 1;
                int subArraySum = 0;
                for(int i = startIndex; i <= endIndex; i++) {
                    subArraySum += nums[i];
                }
                if(subArraySum > maxSum) {
                    maxSum = subArraySum;
                }
            }
        }
        return maxSum;
    }

    /** O(n^2) algorithm - considers the start index first, followed by the sub array size so
    * as to avoid calculating subarray sums for subarray starting at the same index from scratch
    * each time */
    public int maxSumSubArrayNSquare(int[] nums) {
        int n = nums.length;
        int result = Integer.MIN_VALUE;

        // Consider subarrays starting at the same index together
        for(int startIndex = 0; startIndex < n; startIndex++) {
            int subArraySum = 0;
            for(int subArraySize = 1; subArraySize <= n ; subArraySize++) {
                if(startIndex+subArraySize > n) {
                    break;
                }
                subArraySum += nums[startIndex + subArraySize - 1];
                result = Math.max(result, subArraySum);
            }
        }
        return result;
    }

    public int maxSumSubArrayRec(int[] nums) {
        return maxSumSubArrayRecursive(nums, 0, nums.length - 1);
    }

    public int maxSumSubArrayRecursive(int[] nums, int low, int high) {
        if(low == high) {
            return nums[low];
        } else {
            int mid = low + (high - low)/2;
            int leftMax = maxSumSubArrayRecursive(nums, low, mid);
            int rightMax = maxSumSubArrayRecursive(nums, mid+1, high);
            int crossMax = maxSumSubArrayCross(nums, low, mid, high);
            return Math.max(Math.max(leftMax, rightMax), crossMax);
        }
    }

    private int maxSumSubArrayCross(int[] nums, int low, int mid, int high) {
        // Find suffix of left half with max sum, find prefix of right half with max sum and add

        //Find max subarray of the form a[i..mid]
        int leftMax = Integer.MIN_VALUE;
        int leftSum = 0;
        for(int i = mid; i >= low; i--) {
            leftSum += nums[i];
            if(leftSum > leftMax) {
                leftMax = leftSum;
            }
        }

        // Find max subarray of the form a[mid+1..j]
        int rightMax = Integer.MIN_VALUE;
        int rightSum = 0;
        for(int j = mid+1; j <= high; j++) {
            rightSum += nums[j];
            if(rightSum > rightMax) {
                rightMax = rightSum;
            }
        }
        return leftMax+rightMax;
    }

    public static void main(String[] args) {
        MaxSubArray msa = new MaxSubArray();
        System.out.println(msa.maxSumSubArray(new int[]{3, -2, 5, -1}));
    }
}
