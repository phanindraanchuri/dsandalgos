package editdistance;

/**
 * Created by phanindra on 3/28/16.
 */
public class EditDistance {

    public int editDistance(String s1, String s2) {
        return editDistanceRecursive(s1, s2, s1.length() - 1, s2.length() - 1);
    }

    private int editDistanceRecursive(String s1, String s2, int m, int n) {
        if (m == 0) {
            return n;
        }
        if (n == 0) {
            return m;
        }
        if (s1.charAt(m) == s2.charAt(n)) {
            return editDistanceRecursive(s1, s2, m - 1, n - 1);
        }
        int insert = editDistanceRecursive(s1, s2, m, n - 1);
        int delete = editDistanceRecursive(s1, s2, m - 1, n);
        int replace = editDistanceRecursive(s1, s2, m - 1, n - 1);
        return 1 + Math.min(Math.min(insert, delete), replace);
    }

    public static void main(String[] args) {
        String s1 = "cat";
        String s2 = "cars";
        EditDistance ed = new EditDistance();
        System.out.println(ed.editDistance(s1, s2));
    }
}
