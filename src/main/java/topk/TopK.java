package topk;

import java.util.PriorityQueue;
import java.util.Arrays;

public class TopK {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(TopK.topk(new int[]{9, 4, 2, 8, 6, 1, 7, 3}, 3)));
    }

    public static int[] topk(int[] a, int k) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        if (k > a.length) {
            return a;
        }

        for (int i = 0; i < k; i++) {
            pq.add(a[i]);
        }

        for (int i = k; i < a.length; i++) {
            int curr = a[i];
            int min = pq.peek();
            if (curr > min) {
                pq.poll();
                pq.add(curr);
            }
        }

        int[] result = new int[k];
        for (int i = 0; i < k; i++) {
            result[i] = pq.poll();
        }
        return result;
    }
}
