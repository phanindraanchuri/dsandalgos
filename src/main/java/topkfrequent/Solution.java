package topkfrequent;

import java.util.*;

public class Solution {
    public List<Integer> topKFrequentHeap(int[] nums, int k) {
        List<Integer> result = new ArrayList<>();
        if (nums.length == 0) {
            return result;
        }

        Map<Integer, Integer> freqMap = new HashMap<>();
        for (int num : nums) {
            if (freqMap.containsKey(num)) {
                Integer currFreq = freqMap.get(num);
                freqMap.put(num, currFreq + 1);
            } else {
                freqMap.put(num, 1);
            }
        }

        PriorityQueue<Map.Entry<Integer, Integer>> pq = new PriorityQueue<>(k, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> first, Map.Entry<Integer, Integer> second) {
                return second.getValue() - first.getValue();
            }
        });

        for (Map.Entry<Integer, Integer> entry : freqMap.entrySet()) {
            pq.offer(entry);
        }

        for (int i = 0; i < k; i++) {
            result.add(pq.poll().getKey());
        }
        return result;
    }
}