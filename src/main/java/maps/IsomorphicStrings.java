package maps;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class IsomorphicStrings {
    public boolean isIsomorphic(String s, String t) {
        if(s.length() != t.length()) {
            return false;
        }
        Map<Character, Character> m = new HashMap<>();
        char[] sArr = s.toCharArray();
        char[] tArr = t.toCharArray();
        for(int i = 0; i < sArr.length; i++) {
            if(m.containsKey(sArr[i])) {
                if (!m.get(sArr[i]).equals(tArr[i])) {
                    return false;
                }
            } else if(m.containsValue(tArr[i])) { // This takes linear time.
                return false;
            }
            else {
                m.put(sArr[i], tArr[i]);
            }
        }
        return true;
    }

    public boolean isIsomorphicExtraSpace(String s, String t) {
        if(s.length() != t.length()) {
            return false;
        }
        Map<Character, Character> m = new HashMap<>();
        Set<Character> mappedChars = new HashSet<>();
        char[] sArr = s.toCharArray();
        char[] tArr = t.toCharArray();
        for(int i = 0; i < sArr.length; i++) {
            if(m.containsKey(sArr[i])) {
                if (!m.get(sArr[i]).equals(tArr[i])) {
                    return false;
                }
            } else if(mappedChars.contains(tArr[i])) { // current char in t already mapped to a
                // different char other than current char in s
                return false;
            }
            else {
                m.put(sArr[i], tArr[i]);
            }
        }
        return true;
    }

    public static void main(String[] args) {
        IsomorphicStrings is = new IsomorphicStrings();
        System.out.println(is.isIsomorphic("egg", "add"));
        System.out.println(is.isIsomorphic("foo", "bar"));
        System.out.println(is.isIsomorphic("paper", "title"));
        System.out.println(is.isIsomorphic("ab", "aa"));

        System.out.println(is.isIsomorphicExtraSpace("egg", "add"));
        System.out.println(is.isIsomorphicExtraSpace("foo", "bar"));
        System.out.println(is.isIsomorphicExtraSpace("paper", "title"));
        System.out.println(is.isIsomorphicExtraSpace("ab", "aa"));
    }
}
