package stringsearching;

/**
 * Created by phanindra on 4/3/16.
 */
public class Naive {

    public static int strStr(String text, String pattern) {
        if(text == null || text.isEmpty() || text.length() < pattern.length()) {
            return -1;
        }
        char[] tArr = text.toCharArray();
        char[] pArr = pattern.toCharArray();
        int i;
        int j;
        for(i = 0; i < tArr.length - pArr.length; i++) {
            for(j = 0; j < pArr.length; j++) {
                if(tArr[i+j] != pArr[j]) {
                    break;
                }
            }
            if(j == pArr.length) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(Naive.strStr("ABRACRACARA", "RACA"));
    }
}
