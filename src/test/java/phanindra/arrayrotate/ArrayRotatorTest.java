package phanindra.arrayrotate;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class ArrayRotatorTest {

	@Test
	public void extraSpaceAlgorithmTest() {
		ArrayRotator arrayRotator = new ArrayRotator(
				new ExtraSpaceRotationAlgorithm());
		int[] input = new int[] { 9, 7, 1, 3, 4, 6 };
		int[] result = new int[] { 4, 6, 9, 7, 1, 3 };
		assertArrayEquals(result,
				arrayRotator.rotate(input, 2, RotationDirection.RIGHT));
	}

	@Test
	public void extraSpaceAlgorithmTest_LeftRotation() {
		ArrayRotator arrayRotator = new ArrayRotator(
				new ExtraSpaceRotationAlgorithm());
		int[] input = new int[] { 9, 7, 1, 3, 4, 6 };
		int[] result = new int[] { 1, 3, 4, 6, 9, 7 };
		assertArrayEquals(result,
				arrayRotator.rotate(input, 2, RotationDirection.LEFT));
	}

	@Test
	public void reversalAlgorithmTest() {
		ArrayRotator arrayRotator = new ArrayRotator(
				new ReversalRotationAlgorithm());
		int[] input = new int[] { 9, 7, 1, 3, 4, 6 };
		int[] result = new int[] { 4, 6, 9, 7, 1, 3 };
		assertArrayEquals(result,
				arrayRotator.rotate(input, 2, RotationDirection.RIGHT));
	}

	@Test
	public void reversalAlgorithmTest_LeftRotation() {
		ArrayRotator arrayRotator = new ArrayRotator(
				new ReversalRotationAlgorithm());
		int[] input = new int[] { 9, 7, 1, 3, 4, 6 };
		int[] result = new int[] { 1, 3, 4, 6, 9, 7 };
		assertArrayEquals(result,
				arrayRotator.rotate(input, 2, RotationDirection.LEFT));
	}
}
