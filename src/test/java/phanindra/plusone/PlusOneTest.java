package phanindra.plusone;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class PlusOneTest {

	@Test
	public void noThroughCarryTest() {
		List<Integer> digits = Arrays.asList(1, 2, 9, 9, 9);
		List<Integer> result = Arrays.asList(1, 3, 0, 0, 0);
		assertEquals(result, PlusOne.plusOne(digits));
	}

	@Test
	public void throughCarryTest() {
		List<Integer> digits = Arrays.asList(9, 9, 9);
		List<Integer> result = Arrays.asList(1, 0, 0, 0);
		assertEquals(result, PlusOne.plusOne(digits));
	}

	@Test
	public void noCarryTest() {
		List<Integer> digits = Arrays.asList(2, 3, 4);
		List<Integer> result = Arrays.asList(2, 3, 5);
		assertEquals(result, PlusOne.plusOne(digits));
	}
}
