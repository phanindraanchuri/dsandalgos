package phanindra.dsandalgos.dividetwointegers;

import static org.junit.Assert.*;

import org.junit.Test;

import phanindra.dsangalgos.dividetwointegers.Solution;

public class SolutionTest {

	private static final Solution SOLUTION = new Solution();

	@Test
	public void test() {
		assertEquals(-1, SOLUTION.divide(1, -1));
	}

	@Test
	public void test2() {
		assertEquals(1, SOLUTION.divide(-1, -1));
	}

	@Test
	public void test3() {
		assertEquals(Integer.MAX_VALUE,
				SOLUTION.divide(-1010369383, -2147483648));
	}

	@Test
	public void test4() {
		assertEquals(1, SOLUTION.divide(1, 1));
	}

	@Test
	public void test5() {
		assertEquals(2, SOLUTION.divide(5, 2));
	}

	@Test
	public void test6() {
		assertEquals(-2147483648, SOLUTION.divide(-2147483648, 1));
	}
}
