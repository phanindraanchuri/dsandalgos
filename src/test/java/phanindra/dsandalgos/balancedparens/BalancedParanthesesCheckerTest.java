package phanindra.dsandalgos.balancedparens;

import static org.junit.Assert.*;

import org.junit.Test;

public class BalancedParanthesesCheckerTest {

	private static final BalancedParanthesesChecker CHECKER = new BalancedParanthesesChecker();

	@Test
	public void emptyString() {
		assertTrue(CHECKER.isBalanced(""));
	}
	
	@Test
	public void notBalanced() {
		assertFalse(CHECKER.isBalanced("([)]"));
	}
	
	@Test
	public void balancedCase1() {
		assertTrue(CHECKER.isBalanced("()[]{}"));
	}
	
	@Test
	public void notBalancedCase2() {
		assertFalse(CHECKER.isBalanced("(]"));
	}
	
	@Test
	public void notBalancedCase3() {
		assertFalse(CHECKER.isBalanced("([)]"));
	}
}
