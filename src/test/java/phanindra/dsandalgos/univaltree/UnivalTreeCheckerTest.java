package phanindra.dsandalgos.univaltree;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.binarytree.BinaryTree;
import phanindra.dsandalgos.binarytree.BinaryTreeNode;
import phanindra.dsandalgos.univaltree.UnivalTreeChecker;

public class UnivalTreeCheckerTest {

	@Test
	public void notAUnivalTree() {
		BinaryTreeNode left = new BinaryTreeNode(12);
		BinaryTreeNode right = new BinaryTreeNode(12);
		BinaryTreeNode root = new BinaryTreeNode(13);
		root.setLeft(left);
		root.setRight(right);
		BinaryTree tree = new BinaryTree(root);
		Assert.assertFalse(UnivalTreeChecker.checkRecursive(tree));
	}

	@Test
	public void univalTreeRecursiveTest() {
		BinaryTree tree = univalTree();
		Assert.assertTrue(UnivalTreeChecker.checkRecursive(tree));
	}

	@Test
	public void univalTreeIterativeTest() {
		BinaryTree tree = univalTree();
		Assert.assertTrue(UnivalTreeChecker.checkIterative(tree));
	}

	private BinaryTree univalTree() {
		BinaryTreeNode root = new BinaryTreeNode(12);
		BinaryTreeNode left = new BinaryTreeNode(12);
		BinaryTreeNode right = new BinaryTreeNode(12);
		BinaryTreeNode leftLeft = new BinaryTreeNode(12);
		BinaryTreeNode leftRight = new BinaryTreeNode(12);
		root.setLeft(left);
		root.setRight(right);
		left.setLeft(leftLeft);
		left.setRight(leftRight);
		BinaryTree tree = new BinaryTree(root);
		return tree;
	}
}
