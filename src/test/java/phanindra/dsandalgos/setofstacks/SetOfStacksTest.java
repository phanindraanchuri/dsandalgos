package phanindra.dsandalgos.setofstacks;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.setofstacks.SetOfStacks;

public class SetOfStacksTest {

	@Test
	public void noStacks() {
		SetOfStacks ss = new SetOfStacks();
		Assert.assertEquals(0, ss.size());
		Assert.assertNull(ss.pop());
	}

	@Test
	public void popLastElementFromLastStack() {
		SetOfStacks ss = new SetOfStacks(2);
		ss.push(10);
		Assert.assertEquals(1, ss.size());
		ss.push(20);
		ss.pop();
		ss.pop();
		Assert.assertEquals(0, ss.size());
	}

	@Test
	public void newStackCreationWhenPushingOverCapacity() {
		SetOfStacks ss = new SetOfStacks(2);
		ss.push(10);
		ss.push(20);
		Assert.assertEquals(1, ss.size());
		ss.push(20);
		Assert.assertEquals(2, ss.size());
	}
}
