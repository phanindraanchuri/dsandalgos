package phanindra.dsandalgos.firstnonrepeatedchar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.math.BigInteger;

import org.junit.Test;

public class FirstNonRepeatedCharTest {

	@Test
	public void testEmptyString() {
		FirstNonRepeatedChar f = new FirstNonRepeatedChar();
		assertNull(f.firstNonRepeatedChar(""));
	}
	
	@Test
	public void testStringLengthOne() {
		FirstNonRepeatedChar f = new FirstNonRepeatedChar();
		assertEquals(new Character('a'), f.firstNonRepeatedChar("a"));
	}
	
	@Test
	public void testNoNonRepeatingChars() {
		FirstNonRepeatedChar f = new FirstNonRepeatedChar();
		assertNull(f.firstNonRepeatedChar("aaa"));
	}
	
	@Test
	public void testNoNonRepeatingChars2() {
		FirstNonRepeatedChar f = new FirstNonRepeatedChar();
		assertNull(f.firstNonRepeatedChar("abba"));
	}
	
	@Test
	public void testNontrivialString() {
		FirstNonRepeatedChar f = new FirstNonRepeatedChar();
		assertEquals(new Character('d'), f.firstNonRepeatedChar("aaadbbc"));
	}
	
	@Test
	public void testBlah() {
		String a = "12.4";
		String[] arr =a.split("\\.");
		System.out.println(arr.length);
		
	}
}