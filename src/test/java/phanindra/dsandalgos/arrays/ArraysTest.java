package phanindra.dsandalgos.arrays;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.arrays.Arrays;

public class ArraysTest {

	@Test
	public void test() {
		Assert.assertEquals(3, Arrays.deDup(new int[] { 1, 1, 1, 2, 3, 3 }));
	}

	@Test
	public void test2() {
		Assert.assertEquals(2, Arrays.deDup(new int[] { 1, 1, 1, 3 }));
	}

	@Test
	public void test3() {
		Assert.assertEquals(2, Arrays.deDup(new int[] { 1, 1, 2 }));
	}

	@Test
	public void test4() {
		Assert.assertEquals(1, Arrays.deDup(new int[] { 1, 1 }));
	}

	@Test
	public void test5() {
		Assert.assertEquals(2, Arrays.deDup(new int[] { 1, 2 }));
	}

	@Test
	public void test6() {
		Assert.assertEquals(2, Arrays.deDup(new int[] { 1, 2, 2 }));
	}
}
