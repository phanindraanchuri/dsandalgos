package phanindra.dsandalgos.fibonacci;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

public class FibonacciTest {

	@Test
	public void naiveTest() {
		long start = System.nanoTime();
		System.out.println(Fibonacci.naive(40));
		long stop = System.nanoTime();
		System.out.println("Time taken "
				+ TimeUnit.NANOSECONDS.toMillis(stop - start) + " milli seconds");
	}

	@Test
	public void iterativeTest() {
		long start = System.nanoTime();
		System.out.println(Fibonacci.iterative(300));
		long stop = System.nanoTime();
		System.out.println("Time taken "
				+ TimeUnit.NANOSECONDS.toMillis(stop - start) + " milli seconds");
	}
}
