package phanindra.dsandalgos.mergeintervals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class SolutionTest {
	
	private Solution solution = new Solution();
	
	@Test
	public void noIntervals() {
		List<Interval> input = new ArrayList<Interval>();
		Assert.assertArrayEquals(input.toArray(), solution.merge(input).toArray());
	}
	
	@Test
	public void singleInterval() {
		List<Interval> input = new ArrayList<Interval>();
		input.add(new Interval(1,2));
		Assert.assertArrayEquals(input.toArray(), solution.merge(input).toArray());
	}
	
	@Test
	public void twoNonOverlappingIntervals() {
		List<Interval> input = new ArrayList<Interval>();
		input.add(new Interval(5,7));
		input.add(new Interval(1,4));
		
		List<Interval> expected = new ArrayList<Interval>();
		expected.add(new Interval(1,4));
		expected.add(new Interval(5,7));
		Assert.assertArrayEquals(expected.toArray(), solution.merge(input).toArray());
	}
	
	@Test
	public void mergeSameIntervals() {
		List<Interval> input = new ArrayList<Interval>();
		input.add(new Interval(1,4));
		input.add(new Interval(1,4));
		
		List<Interval> expected = new ArrayList<Interval>();
		expected.add(new Interval(1,4));
		Assert.assertArrayEquals(expected.toArray(), solution.merge(input).toArray());
	}
	
	@Test
	public void mergeOverlappingIntervals() {
		List<Interval> input = new ArrayList<Interval>();
		input.add(new Interval(1,4));
		input.add(new Interval(2,5));
		
		List<Interval> expected = new ArrayList<Interval>();
		expected.add(new Interval(1,5));
		Assert.assertArrayEquals(expected.toArray(), solution.merge(input).toArray());
	}
	
	@Test
	public void mergeOverlappingIntervals2() {
		List<Interval> input = new ArrayList<Interval>();
		input.add(new Interval(1,4));
		input.add(new Interval(4,5));
		
		List<Interval> expected = new ArrayList<Interval>();
		expected.add(new Interval(1,5));
		Assert.assertArrayEquals(expected.toArray(), solution.merge(input).toArray());
	}
	
	@Test
	public void mergeThreeIntervals() {
		List<Interval> input = new ArrayList<Interval>();
		input.add(new Interval(1,4));
		input.add(new Interval(0,2));
		input.add(new Interval(3,5));
		
		List<Interval> expected = new ArrayList<Interval>();
		expected.add(new Interval(0,5));
		Assert.assertArrayEquals(expected.toArray(), solution.merge(input).toArray());
	}
	
	@Test
	public void testCase1() {
		List<Interval> input = new ArrayList<Interval>();
		input.add(new Interval(2,3));
		input.add(new Interval(4,5));
		input.add(new Interval(6,7));
		input.add(new Interval(8,9));
		input.add(new Interval(1,10));
		
		List<Interval> expected = new ArrayList<Interval>();
		expected.add(new Interval(1,10));
		Assert.assertArrayEquals(expected.toArray(), solution.merge(input).toArray());
	}
}
