package phanindra.dsandalgos.twosum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class TwoSumTest {

	@Test
	public void test() {
		TwoSum t = new TwoSum();
		List<TwoSumPair> expected = new ArrayList<>();
		expected.add(new TwoSumPair(1, 3));
		Assert.assertEquals(expected,
				t.twosumTwoPointer(Arrays.asList(1, 2, 3, 4), 4));
	}
	
	@Test
	public void testMapSolution() {
		TwoSum t = new TwoSum();
		List<TwoSumPair> expected = new ArrayList<>();
		expected.add(new TwoSumPair(1, 3));
		Assert.assertEquals(expected,
				t.twosumMap(Arrays.asList(1, 2, 3, 4), 4));
	}
	
	@Test
	public void testMapSolutionDuplicates() {
		TwoSum t = new TwoSum();
		List<TwoSumPair> expected = new ArrayList<>();
		expected.add(new TwoSumPair(1, 3));
		Assert.assertEquals(expected,
				t.twosumMap(Arrays.asList(1, 1, 2, 3, 4), 4));
	}
}
