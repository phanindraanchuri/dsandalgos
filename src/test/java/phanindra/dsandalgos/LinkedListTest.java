package phanindra.dsandalgos;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import phanindra.dsandalgos.LinkedList;
import phanindra.dsandalgos.ListNode;

public class LinkedListTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testLength() {
		ListNode nodeOne = new ListNode(10);
		ListNode nodeTwo = new ListNode(5);
		ListNode nodeThree = new ListNode(7);

		nodeOne.setNext(nodeTwo);
		nodeTwo.setNext(nodeThree);
		nodeThree.setNext(null);

		LinkedList list = new LinkedList();
		list.setHead(nodeOne);

		Assert.assertEquals(3, list.length());
	}

	@Test
	public void emptyListLength() {
		LinkedList list = new LinkedList();
		Assert.assertEquals(0, list.length());
	}

	@Test
	public void addFirstTest() {
		LinkedList list = new LinkedList();
		list.addFirst(2);
		ListNode head = new ListNode(2);
		Assert.assertEquals(head, list.getHead());
		list.addFirst(3);
		ListNode newHead = new ListNode(3);
		Assert.assertEquals(newHead, list.getHead());
	}

	@Test
	public void addLastTest() {
		LinkedList list = new LinkedList();
		list.addLast(2);
		list.addLast(3);
		Assert.assertEquals(2, list.length());
		Assert.assertEquals(2, list.getHead().getData());
	}

	@Test
	public void addAtTest() {
		LinkedList list = new LinkedList();
		list.addAt(0, 100);
		list.addAt(1, 400);
		list.addAt(2, 900);
		list.addAt(1, 225);
		ListNode expected = new ListNode(225);
		Assert.assertEquals(expected, list.get(1));
	}

	@Test
	public void addAtExceptionTest() {
		thrown.expect(RuntimeException.class);
		LinkedList list = new LinkedList();
		list.addAt(1, 2);
	}

	@Test
	public void findInEmptyList() {
		LinkedList emptyList = new LinkedList();
		Assert.assertNull(emptyList.find(20));
	}

	@Test
	public void findInNonEmptyList() {
		LinkedList list = new LinkedList();
		list.addLast(10);
		list.addLast(20);
		list.addLast(30);
		ListNode expected = new ListNode(20);
		Assert.assertEquals(expected, list.find(20));
	}

	@Test
	public void findNonExistentElement() {
		LinkedList list = new LinkedList();
		list.addLast(10);
		list.addLast(20);
		list.addLast(30);
		Assert.assertNull(list.find(40));
	}

	@Test
	public void removeFirst() {
		LinkedList list = new LinkedList();
		list.addLast(10);
		list.addLast(20);
		list.addLast(30);
		list.removeFirst();
		Assert.assertEquals(new ListNode(20), list.get(0));
		Assert.assertNull(list.find(10));
		list.removeFirst();
		Assert.assertEquals(new ListNode(30), list.get(0));
		Assert.assertNull(list.find(10));
		Assert.assertNull(list.find(20));
		list.removeFirst();
		Assert.assertTrue(list.isEmpty());
	}

	@Test
	public void reverseEmptyList() {
		LinkedList list = new LinkedList();
		Assert.assertNull(list.reverse());
	}

	@Test
	public void reverseOneElementList() {
		LinkedList list = new LinkedList();
		list.addLast(2);
		Assert.assertEquals(new ListNode(2), list.reverse());
	}

	@Test
	public void reverseTwoElementList() {
		LinkedList list = new LinkedList();
		list.addLast(2);
		list.addLast(5);
		list.addLast(7);
		ListNode head = list.reverse();
		Assert.assertEquals(new ListNode(7), head);
	}

	@Test
	public void reverseNonTrivialList() {
		LinkedList list = new LinkedList();
		list.addLast(2);
		list.addLast(5);
		list.addLast(7);
		list.addLast(10);
		ListNode head = list.reverse();
		Assert.assertEquals(new ListNode(10), head);
	}

	@Test
	public void listWithLoop() {
		LinkedList list = new LinkedList();
		list.addLast(1);
		list.addLast(2);
		ListNode thirdNode = list.addLast(3);
		list.addLast(4);
		list.addLast(5);
		ListNode lastNode = list.addLast(6);
		lastNode.setNext(thirdNode);
		Assert.assertTrue(list.hasLoop());
	}

	@Test
	public void listWithNoLoop() {
		LinkedList list = new LinkedList();
		list.addLast(1);
		list.addLast(2);
		list.addLast(3);
		list.addLast(4);
		list.addLast(5);
		list.addLast(6);
		Assert.assertFalse(list.hasLoop());
	}
}
