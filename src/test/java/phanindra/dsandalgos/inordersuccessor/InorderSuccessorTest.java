package phanindra.dsandalgos.inordersuccessor;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.inordersuccessor.BinarySearchTree;
import phanindra.dsandalgos.inordersuccessor.BinaryTreeNode;

public class InorderSuccessorTest {

	@Test
	public void emptyTreeSearch() {
		BinarySearchTree bst = new BinarySearchTree();
		Assert.assertFalse(bst.search(20));
	}

	@Test
	public void searchElementNotFoundTest() {
		BinarySearchTree bst = new BinarySearchTree();
		bst.insert(20);
		bst.insert(8);
		bst.insert(22);
		bst.insert(4);
		bst.insert(12);
		bst.insert(10);
		bst.insert(14);
		Assert.assertFalse(bst.search(25));
	}

	@Test
	public void searchElementFoundTest() {
		BinarySearchTree bst = new BinarySearchTree();
		bst.insert(20);
		bst.insert(8);
		bst.insert(22);
		bst.insert(4);
		bst.insert(12);
		bst.insert(10);
		bst.insert(14);
		Assert.assertTrue(bst.search(4));
	}

	@Test(timeout = 10)
	public void insertDuplicateTerminationTest() {
		BinarySearchTree bst = new BinarySearchTree();
		bst.insert(20);
		bst.insert(20);
		Assert.assertTrue(true);
	}

	@Test
	public void inorderTest() {
		BinarySearchTree bst = new BinarySearchTree();
		bst.insert(20);
		bst.insert(8);
		bst.insert(22);
		bst.insert(4);
		bst.insert(12);
		bst.insert(10);
		bst.insert(14);

		List<BinaryTreeNode> expected = new ArrayList<BinaryTreeNode>();
		expected.add(new BinaryTreeNode(4));
		expected.add(new BinaryTreeNode(8));
		expected.add(new BinaryTreeNode(10));
		expected.add(new BinaryTreeNode(12));
		expected.add(new BinaryTreeNode(14));
		expected.add(new BinaryTreeNode(20));
		expected.add(new BinaryTreeNode(22));

		List<BinaryTreeNode> list = bst.inorderIterative();
		Assert.assertThat(list, CoreMatchers.is(expected));
	}
}
