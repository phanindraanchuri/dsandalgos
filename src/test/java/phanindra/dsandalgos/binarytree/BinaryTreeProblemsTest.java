package phanindra.dsandalgos.binarytree;

import static org.junit.Assert.*;

import org.junit.Test;

import phanindra.dsandalgos.binarytree.BinaryTree;
import phanindra.dsandalgos.binarytree.BinaryTreeNode;
import phanindra.dsandalgos.binarytree.BinaryTreeProblems;

public class BinaryTreeProblemsTest {

	@Test
	public void test() {
		BinaryTreeNode rootNode = new BinaryTreeNode(10);
		BinaryTreeNode leftNode = new BinaryTreeNode(5);
		BinaryTreeNode rightNode = new BinaryTreeNode(20);
		rootNode.setLeft(leftNode);
		rootNode.setRight(rightNode);

		BinaryTreeNode leftLeftNode = new BinaryTreeNode(2);
		BinaryTreeNode leftRightNode = new BinaryTreeNode(7);

		leftNode.setLeft(leftLeftNode);
		leftNode.setRight(leftRightNode);

		BinaryTreeNode rightLeftNode = new BinaryTreeNode(15);
		BinaryTreeNode rightRightNode = new BinaryTreeNode(27);

		rightNode.setLeft(rightLeftNode);
		rightNode.setRight(rightRightNode);

		BinaryTree tree = new BinaryTree(rootNode);

		BinaryTreeProblems problems = new BinaryTreeProblems();
		assertTrue(problems.hasPathSum(tree, 17));
	}

}
