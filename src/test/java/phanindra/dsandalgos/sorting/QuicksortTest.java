package phanindra.dsandalgos.sorting;

import static org.junit.Assert.*;

import org.junit.Test;

public class QuicksortTest {

	@Test
	public void test() {
		int[] array = new int[] { 5, 6, 2, 7, 4, 1, 3 };
		Quicksort.sort(array);
		assertArrayEquals(new int[] { 1, 2, 3, 4, 5, 6, 7 }, array);
	}

	@Test
	public void testReverseSortedArray() {
		int[] array = new int[] { 7, 6, 5, 4, 3, 2, 1 };
		Quicksort.sort(array);
		assertArrayEquals(new int[] { 1, 2, 3, 4, 5, 6, 7 }, array);
	}
}
