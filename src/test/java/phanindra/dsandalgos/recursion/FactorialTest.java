package phanindra.dsandalgos.recursion;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import phanindra.dsandalgos.recursion.Factorial;

public class FactorialTest {

	@Rule
	public ExpectedException e = ExpectedException.none();

	@Test
	public void testFactorial() {
		int result = Factorial.factorial(5);
		int expectedResult = 120;
		Assert.assertEquals(expectedResult, result);
	}

	@Test
	public void testFactorialForValuesLessthanTwo() {
		int n = 1;
		int result = Factorial.factorial(n);
		int expectedResult = n;
		Assert.assertEquals(expectedResult, result);
	}

	@Test
	public void testFactorialForNegativeNumbers() {
		e.expect(RuntimeException.class);
		e.expectMessage("Give me a positive integer");
		int n = -10;
		Factorial.factorial(n);
	}
}
