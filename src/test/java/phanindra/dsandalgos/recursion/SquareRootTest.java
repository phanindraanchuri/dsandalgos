package phanindra.dsandalgos.recursion;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.recursion.SquareRoot;

public class SquareRootTest {

	@Test
	public void test() {
		Assert.assertEquals(256,SquareRoot.sqrtRecursive(65536), SquareRoot.DELTA);
	}

}
