package phanindra.dsandalgos.priorityqueue;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.priorityqueue.HeapBasedPriorityQueue;
import phanindra.dsandalgos.priorityqueue.PriorityQueue;

public class HeapBasedPriorityQueueTest {

	@Test
	public void testInsert() {
		PriorityQueue<Integer, String> pq = new HeapBasedPriorityQueue<>();
		pq.insert(9, "nine");
		pq.insert(7, "seven");
		pq.insert(8, "eight");
		pq.insert(11, "eleven");
		pq.insert(10, "ten");
		pq.insert(3, "three");
		System.out.println(pq);
	}

	@Test
	public void testRemoveMin() {
		PriorityQueue<Integer, String> pq = new HeapBasedPriorityQueue<>();
		pq.insert(9, "nine");
		pq.insert(7, "seven");
		pq.insert(8, "eight");
		pq.insert(11, "eleven");
		pq.insert(10, "ten");
		pq.insert(3, "three");
		Assert.assertEquals(3, pq.removeMin().getKey().intValue());
	}
}
