package phanindra.dsandalgos.singlesell;

import static org.junit.Assert.*;

import org.junit.Test;

public class SingleSellProblemTest {

	@Test
	public void testBruteForce() {
		int[] stockPrices = new int[] { 2, 7, 1, 8, 2, 8, 4, 5, 9, 0, 4, 5 };
		SingleSellProblem problem = new SingleSellProblem(stockPrices);
		Solution expected = new Solution();
		expected.setBuyIndex(2);
		expected.setSellIndex(8);
		expected.setProfit(8);
		assertEquals(expected, problem.bruteForce());
	}

	@Test
	public void testDivideAndConquer() {
		int[] stockPrices = new int[] { 2, 7, 1, 8, 2, 8, 4, 5, 9, 0, 4, 5 };
		SingleSellProblem problem = new SingleSellProblem(stockPrices);
		Solution expected = new Solution();
		expected.setProfit(8);
		assertEquals(expected, problem.divideAndConquer());
	}

	@Test
	public void testDynamicProgrammingSolution() {
		int[] stockPrices = new int[] { 2, 7, 1, 8, 2, 8, 4, 5, 9, 0, 4, 5 };
		SingleSellProblem problem = new SingleSellProblem(stockPrices);
		Solution expected = new Solution();
		expected.setProfit(8);
		assertEquals(expected, problem.dynamicProgramming());
	}
}
