package phanindra.dsandalgos.strings;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.strings.PalindromeChecker;

public class PalindromeCheckerTest {

	@Test
	public void emptyString() {
		Assert.assertTrue(PalindromeChecker.isPalindrome(""));
	}

	@Test
	public void stringWithOneChar() {
		Assert.assertTrue(PalindromeChecker.isPalindrome("a"));
	}

	@Test
	public void evenNumberOfChars() {
		Assert.assertTrue(PalindromeChecker.isPalindrome("abba"));
	}

	@Test
	public void oddNumberOfChars() {
		Assert.assertTrue(PalindromeChecker.isPalindrome("cabac"));
	}

	@Test
	public void oddNumberOfCharsNegativeTest() {
		Assert.assertFalse(PalindromeChecker.isPalindrome("abb"));
	}

	@Test
	public void evenNumberOfCharsNegativeTest() {
		Assert.assertFalse(PalindromeChecker.isPalindrome("ab"));
	}

	@Test
	public void longString() {
		Assert.assertTrue(PalindromeChecker
				.isPalindrome("A man, a plan, a canal, Panama"));
	}
}
