package phanindra.dsandalgos.stockbuysell;

import org.junit.Test;

import phanindra.dsandalgos.stockbuysell.Solution;
import phanindra.dsandalgos.stockbuysell.StockBuySell;

public class StockBuySellTest {

	@Test
	public void test() {
		StockBuySell s = new StockBuySell();
		Solution sol = s.findBestTime(new int[] { 5, 3, 10, 9, 15, 7, 1 });
		System.out.println(sol);
	}

}
