package phanindra.dsandalgos.minsortedrotatedarray;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SolutionTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();

	private static final Solution SOLUTION = new Solution();

	@Test
	public void emptyArray() {
		exception.expect(IllegalArgumentException.class);
		int[] emptyArray = {};
		SOLUTION.findMin(emptyArray);
	}

	@Test
	public void noRotation() {
		int[] input = { 1, 2, 3 };
		Assert.assertEquals(input[0], SOLUTION.findMin(input));
	}

	@Test
	public void rotatedArrayOne() {
		int[] input = { 2, 1 };
		Assert.assertEquals(1, SOLUTION.findMin(input));
	}

	@Test
	public void rotatedArrayTwo() {
		int[] input = { 4, 5, 6, 7, 0, 1, 2 };
		Assert.assertEquals(0, SOLUTION.findMin(input));
	}
	
	@Test
	public void rotatedArrayWithDuplicates() {
		int[] input = { 4, 5, 6, 6, 7, 0, 1, 2 };
		Assert.assertEquals(0, SOLUTION.findMin(input));
	}
}
