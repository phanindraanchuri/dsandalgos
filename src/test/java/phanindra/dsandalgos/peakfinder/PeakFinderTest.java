package phanindra.dsandalgos.peakfinder;

import static org.junit.Assert.*;

import org.junit.Test;

public class PeakFinderTest {

	@Test
	public void testNaiveAlgo1() {
		int[] nums = new int[]{1,2,3};
		assertEquals(2, PeakFinder.findAPeakIndex(nums));
	}
	
	@Test
	public void testDivideAndConquerAlgo() {
		int[] nums = new int[]{1,2,3};
		assertEquals(2, PeakFinder.findAPeakIndex(nums));
	}
}
