package phanindra.dsandalgos.anagrams;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class AnagramGrouperTest {

	@Test
	public void testGroups() {
		List<String> list = Arrays.asList("star", "rats", "tar", "rat", "art",
				"cheese");
		AnagramGrouper grouper = new AnagramGrouper(list);
		Assert.assertEquals(3, grouper.groups().size());
	}

}
