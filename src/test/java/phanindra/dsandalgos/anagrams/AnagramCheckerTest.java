package phanindra.dsandalgos.anagrams;

import static org.junit.Assert.*;

import org.junit.Test;

public class AnagramCheckerTest {

	@Test
	public void test() {
		assertTrue(AnagramChecker.check("tacit", "attic"));
	}
	
	@Test
	public void test2() {
		assertFalse(AnagramChecker.check("tacite", "attics"));
	}
}
