package phanindra.dsandalgos.map;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.map.Map;
import phanindra.dsandalgos.map.UnOrderedArrayListMap;

public class UnOrderedArrayListMapTest {

	@Test
	public void removeTest() {
		Map<String, String> employeeDepartmentMap = new UnOrderedArrayListMap<>();
		employeeDepartmentMap.put("John", "Accounting");
		employeeDepartmentMap.put("Smith", "Operations");
		employeeDepartmentMap.put("Rachel", "HR");
		employeeDepartmentMap.put("Ralph", "Technology");
		employeeDepartmentMap.put("Randall", "Operations");
		employeeDepartmentMap.remove("Smith");
		Assert.assertNull(employeeDepartmentMap.get("Smith"));
		Set<String> remainingKeys = new HashSet<String>();
		remainingKeys.add("John");
		remainingKeys.add("Rachel");
		remainingKeys.add("Ralph");
		remainingKeys.add("Randall");
		Assert.assertArrayEquals(remainingKeys.toArray(), employeeDepartmentMap.keySet().toArray());
	}

}
