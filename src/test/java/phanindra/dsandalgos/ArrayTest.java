package phanindra.dsandalgos;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import phanindra.dsandalgos.Array;
import phanindra.dsandalgos.exceptions.IllegalInitialCapacityException;

public class ArrayTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void arrayCreationWithIllegalInitialCapacity() {
		thrown.expect(IllegalInitialCapacityException.class);
		new Array(-10);
	}

}
