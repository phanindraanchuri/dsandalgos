package phanindra.dsandalgos.stackandqueue;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.stackandqueue.Queue;

public class QueueTest {

	@Test
	public void dequeueTest() {
		Queue q = new Queue();
		q.enqueue(12);
		q.enqueue(10);
		q.dequeue();
		Assert.assertEquals(10, q.peek().intValue());
	}

	@Test
	public void emptyQueueTests() {
		Queue q = new Queue();
		Assert.assertNull(q.dequeue());
	}
}
