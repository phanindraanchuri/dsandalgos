package phanindra.dsandalgos.stackandqueue;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.stackandqueue.Stack;

public class StackTest {

	@Test
	public void emptyStackTest() {
		Stack stack = new Stack();
		Assert.assertNull(stack.pop());
	}

	@Test
	public void test() {
		Stack stack = new Stack();
		stack.push(2);
		stack.push(1);
		stack.push(5);
		Assert.assertEquals(5, stack.pop().intValue());
	}
}
