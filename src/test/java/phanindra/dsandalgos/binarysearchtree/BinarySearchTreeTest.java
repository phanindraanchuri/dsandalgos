package phanindra.dsandalgos.binarysearchtree;

import org.junit.Assert;
import org.junit.Test;

import phanindra.dsandalgos.binarysearchtree.BinarySearchTree;
import phanindra.dsandalgos.binarytree.BinaryTreeNode;

public class BinarySearchTreeTest {

	@Test
	public void searchEmptyTree() {
		BinarySearchTree tree = new BinarySearchTree();
		Assert.assertFalse(tree.search(10));
	}

	@Test
	public void searchSingleNodeTree() {
		BinarySearchTree tree = new BinarySearchTree(new BinaryTreeNode(10));
		Assert.assertTrue(tree.search(10));
	}

	@Test
	public void searchTreeForExistingElement() {
		BinaryTreeNode left = new BinaryTreeNode(5);
		BinaryTreeNode right = new BinaryTreeNode(15);
		BinaryTreeNode root = new BinaryTreeNode(10);
		root.setLeft(left);
		root.setRight(right);
		BinarySearchTree tree = new BinarySearchTree(new BinaryTreeNode(10));
		Assert.assertTrue(tree.search(10));
	}

	@Test
	public void searchTreeForNonExistingElement() {
		BinaryTreeNode left = new BinaryTreeNode(5);
		BinaryTreeNode right = new BinaryTreeNode(15);
		BinaryTreeNode root = new BinaryTreeNode(10);
		root.setLeft(left);
		root.setRight(right);
		BinarySearchTree tree = new BinarySearchTree(new BinaryTreeNode(10));
		Assert.assertFalse(tree.search(12));
	}

	@Test
	public void insertTest() {
		BinarySearchTree tree = new BinarySearchTree();
		tree.insert(10);
		tree.insert(5);
		tree.insert(15);
		tree.insert(7);
		System.out.println(tree);
	}

	@Test
	public void sizeOfEmptyTree() {
		BinarySearchTree tree = new BinarySearchTree();
		Assert.assertEquals(0, tree.size());
	}

	@Test
	public void sizeTest() {
		BinarySearchTree tree = new BinarySearchTree();
		tree.insert(10);
		tree.insert(5);
		tree.insert(15);
		Assert.assertEquals(3, tree.size());
	}

	@Test
	public void minValueTest() {
		BinarySearchTree tree = new BinarySearchTree();
		tree.insert(10);
		tree.insert(5);
		tree.insert(15);
		tree.insert(3);
		tree.insert(7);
		tree.insert(12);
		tree.insert(17);
		Assert.assertEquals(3, tree.minValue());
	}

	@Test
	public void inorderTest() {
		BinarySearchTree tree = new BinarySearchTree();
		tree.insert(10);
		tree.insert(5);
		tree.insert(15);
		tree.insert(3);
		tree.insert(7);
		tree.insert(12);
		tree.insert(17);
		tree.inorder();
	}


	@Test
	public void postOrderTest() {
		BinarySearchTree tree = new BinarySearchTree();
		tree.insert(2);
		tree.insert(1);
		tree.insert(3);
		tree.insert(4);
		tree.insert(5);
		tree.postorder();
	}
}
