package phanindra.dsandalgos.minstack;

import static org.junit.Assert.*;

import org.junit.Test;

public class MinStackTest {

	@Test
	public void test() {
		MinStack<Integer> minStack = new MinStack<>();
		minStack.push(2);
		minStack.push(6);
		minStack.push(4);
		minStack.push(1);
		minStack.push(5);
		assertEquals(1, minStack.minumum().intValue());
	}

}
