package phanindra.dsandalgos.compounditerator;

import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

import phanindra.dsandalgos.compounditerator.CompoundIterator;

import junit.framework.TestCase;

/**
 * @noinspection ConstantConditions
 */
public class CompoundIteratorTest
    extends TestCase {
    private static final boolean DEBUG_TEST = true;

    private static Random rnd = new Random(System.currentTimeMillis());

    public static final int[][] SIMPLE_TEST_DATA = {
        {0, 1, 2, 3, 4},
        {5, 6, 7},
        {8},
        {9, 10, 11, 12}
    };

    public static final int[][] SPARSE_TEST_DATA = {
        {0, 1, 2, 3, 4},
        {},
        {5, 6, 7},
        {8},
        {},
        {},
        {9},
        {},
        {10, 11},
        {},
        {12}
    };

    public static final int[][] MULTIPLE_EMPTY_TEST_DATA = {
        {}, {}, {}, {}, {}, {}
    };

    public static final int NUM_TEST_LOOPS = DEBUG_TEST ? 50 : 100;
    public static final int MAX_ITERATORS_FOR_RANDOM_TEST = 100;
    public static final int MAX_ELEMENTS_PER_ITERATOR = 5;

    public static final String EOL = System.getProperty("line.separator");

    public void testSimpleData()
        throws Throwable {
        try {
            int[][] testData = SIMPLE_TEST_DATA;
            doSequentialIntegerTest(testData);
        } catch (Throwable e) {
            System.err.println("Throwable caught within test");
            e.printStackTrace();
            throw e;
        }
    }

    public void testSparseData()
        throws Throwable {
        try {
            int[][] testData = SPARSE_TEST_DATA;
            doSequentialIntegerTest(testData);
        } catch (Throwable e) {
            System.err.println("Throwable caught within test");
            e.printStackTrace();
            throw e;
        }
    }

    public void testEmptyData()
        throws Throwable {
        try {
            int[][] testData = MULTIPLE_EMPTY_TEST_DATA;
            doSequentialIntegerTest(testData);
        } catch (Throwable e) {
            System.err.println("Throwable caught within test");
            e.printStackTrace();
            throw e;
        }

    }

    public void testRandomNonSparseData()
        throws Throwable {

        try {
            for (int i = 0; i < NUM_TEST_LOOPS; i++) {
                int[][] testData = constructRandomTestMatrix(MAX_ITERATORS_FOR_RANDOM_TEST,
                    MAX_ELEMENTS_PER_ITERATOR,
                    false, false);
                doSequentialIntegerTest(testData);
            }
        } catch (Throwable e) {
            System.err.println("Throwable caught within test");
            e.printStackTrace();
            throw e;
        }
    }

    public void testRandomSparseData()
        throws Throwable {

        try {
            for (int i = 0; i < NUM_TEST_LOOPS; i++) {
                int[][] testData = constructRandomTestMatrix(MAX_ITERATORS_FOR_RANDOM_TEST,
                    MAX_ELEMENTS_PER_ITERATOR,
                    true, false);
                doSequentialIntegerTest(testData);
            }
        } catch (Throwable e) {
            System.err.println("Throwable caught within test");
            e.printStackTrace();
            throw e;
        }
    }

    public void testRandomSparseDataWithNulls()
        throws Throwable {

        try {
            for (int i = 0; i < NUM_TEST_LOOPS; i++) {
                int[][] testData = constructRandomTestMatrix(MAX_ITERATORS_FOR_RANDOM_TEST,
                    MAX_ELEMENTS_PER_ITERATOR,
                    true, true);
                doSequentialIntegerTest(testData);
            }
        } catch (Throwable e) {
            System.err.println("Throwable caught within test");
            e.printStackTrace();
            throw e;
        }
    }

    private void doSequentialIntegerTest(int[][] testData) {
        // Test the mode of use where you call hasNext()
        doSequentialIntegerTest(testData, true);

        // Test the mode of use where you call next() n times without ever calling hasNext()
        doSequentialIntegerTest(testData, false);
    }

    private void doSequentialIntegerTest(int[][] testData, boolean shouldCallHasNext) {
        Iterator[] iterators = new Iterator[testData.length];
        int numInts = 0;
        for (int i = 0; i < testData.length; i++) {
            if (testData[i] == null) {
                iterators[i] = null;
            } else {
                List intList = intArrayToList(testData[i]);
                iterators[i] = intList.iterator();
                numInts += intList.size();
            }
        }

        System.out.println(intMatrixToString(testData));

        CompoundIterator iter = new CompoundIterator(iterators);
        int count = 0;
        while (shouldCallHasNext ? iter.hasNext() : (count < numInts)) {
            Integer integerFromIterator = (Integer) iter.next();
            assertEquals("Unexpected value returned from CompoundIterator; " +
                "test data was: " + intMatrixToString(testData) + ". ",
                new Integer(count++), integerFromIterator);
        }
        assertEquals("Expected number of elements in compound iterator " +
            "to be the sum of the number of elements " +
            "in the individual iterators", numInts, count);

        try {
            iter.next();
            fail("Expected next() to throw NoSuchElementException after hasNext() is false.");
        } catch (Throwable e) {
            assertTrue("Expected next() to throw NoSuchElementException, not: " + e.getClass().getName(),
                e instanceof NoSuchElementException);
        }
    }

    private List intArrayToList(int[] arr) {
        List intList = new ArrayList();
        for (int i = 0; i < arr.length; i++) {
            Integer element = new Integer(arr[i]);
            intList.add(element);
        }
        return intList;
    }

    private int[][] constructRandomTestMatrix(int maxIterators,
                                              int maxElementsPerIterator,
                                              boolean isSparse,
                                              boolean canContainNullArrays) {
        int numIterators = randomIntBetween(1, maxIterators + 1);
        int[][] target = new int[numIterators][];

        int count = 0;
        for (int i = 0; i < target.length; i++) {
            int minElementsPerIterator = 1;
            if (isSparse) {
                if (canContainNullArrays) {
                    minElementsPerIterator = -1;
                } else {
                    minElementsPerIterator = 0;
                }
            }
            int numElements =
                randomIntBetween(minElementsPerIterator,
                    maxElementsPerIterator + 1);
            int[] subArray = null;
            if (numElements >= 0) {
                subArray = new int[numElements];
                for (int j = 0; j < subArray.length; j++) {
                    subArray[j] = count++;
                }
            }
            if (DEBUG_TEST && (subArray == null)) {
                System.out.println("subArray is null.");
            }
            target[i] = subArray;
        }

        if (DEBUG_TEST) {
            System.out.println(intMatrixToString(target));
        }

        return target;
    }

    private int randomIntBetween(int inclusiveLowerBound, int exclusiveUpperBound) {
        if (inclusiveLowerBound >= exclusiveUpperBound) {
            throw new IllegalArgumentException(
                "Bad range for randomIntBetween();" +
                    " expected inclusiveLowerBound of " + inclusiveLowerBound +
                    " to be stricly less than exclusiveUpperBound of " + exclusiveUpperBound);
        }
        int range = exclusiveUpperBound - inclusiveLowerBound;
        return inclusiveLowerBound + rnd.nextInt(range);
    }

    private String intMatrixToString(int[][] matrix) {
        StringBuffer buf = new StringBuffer();
        buf.append("{").append(EOL);
        for (int i = 0; i < matrix.length; i++) {
            buf.append("    ");
            int[] ints = matrix[i];
            if (ints == null) {
                buf.append("null");
            } else {
                buf.append("{ ");
                for (int j = 0; j < ints.length; j++) {
                    int anInt = ints[j];
                    buf.append(anInt);
                    if (j < ints.length - 1) {
                        buf.append(",");
                    }
                    buf.append(" ");
                }
                buf.append("}");
            }
            if (i < matrix.length - 1) {
                buf.append(",");
            }
            buf.append(EOL);
        }
        buf.append("}");
        return buf.toString();
    }

    public void testInfiniteIterator() throws Throwable {
        try {
            InfiniteIteratorRunnable runnable = new InfiniteIteratorRunnable();
            Thread constructorThread = new Thread(runnable);
            constructorThread.start();
            final long timeout = 10 * 1000L;
            constructorThread.join(timeout);
            if (constructorThread.isAlive()) {
                constructorThread.interrupt();
                fail("Improper handling of infinite Iterator; had to wait more than " + timeout + "ms to construct and make 10 calls to next().");
            } else if (runnable.didCatchThrowable()) {
                throw (new IllegalStateException(runnable.getCaughtThrowable()));
            }
        } catch (Throwable e) {
            System.err.println("Throwable caught within test");
            e.printStackTrace();
            throw e;
        }
    }

    private static class InfiniteRandomIntegerIterator implements Iterator {
        private Random rnd = new Random(System.currentTimeMillis());

        public boolean hasNext() {
            return true;
        }

        public Object next() {
            synchronized (rnd) {
                return rnd.nextInt();
            }
        }

        public void remove()
            throws UnsupportedOperationException {
            throw new UnsupportedOperationException(
                getClass().getName() + " does not support the remove() operation.");
        }
    }

    private static class InfiniteIteratorRunnable implements Runnable {
        private Throwable caughtThrowable = null;

        public void run() {
            try {
                InfiniteRandomIntegerIterator it = new InfiniteRandomIntegerIterator();
                Iterator[] iterators = new Iterator[]{it};
                CompoundIterator ci = new CompoundIterator(iterators);
                for (int i = 0; i < 10 && ci.hasNext(); i++) {
                    ci.next();
                }
            } catch (Throwable t) {
                caughtThrowable = t;
            }
        }

        public boolean didCatchThrowable() {
            return caughtThrowable != null;
        }

        public Throwable getCaughtThrowable() {
            return caughtThrowable;
        }
    }
}

